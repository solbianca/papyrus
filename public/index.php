<?php

define('ROOT', dirname(dirname(__DIR__ . '/..')));

require __DIR__ . '/../vendor/autoload.php';

use Dotenv\Dotenv;
use Papyrus\Application\Kernel\Kernel;

(new Dotenv(ROOT))->load();

$kernel = Kernel::init();
$app = $kernel->getApplication();

// Set up services
require __DIR__ . '/../bootstrap/services.php';
// Register middleware
require __DIR__ . '/../bootstrap/middleware.php';
// Register routes
require __DIR__ . '/../bootstrap/routes.php';

$kernel->handle();