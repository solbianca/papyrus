<?php

$app->get('/', \Papyrus\Http\Actions\MainPage::class);
$app->get('/test', \Papyrus\Http\Actions\TestPage::class);

$app->get('/articles/{id:[0-9]+}', \Papyrus\Http\Actions\Simple\Articles\ShowArticleAction::class);
$app->get('/articles/list', \Papyrus\Http\Actions\Simple\Articles\ListArticleAction::class);
$app->get('/articles/history/{pid}', \Papyrus\Http\Actions\Simple\Articles\HistoryArticleAction::class);
$app->map(['get'], '/articles/edit/{pid}', \Papyrus\Http\Actions\Simple\Articles\FormEditArticleAction::class);
$app->map(['post'], '/articles/edit/{pid}', \Papyrus\Http\Actions\Simple\Articles\EditArticleAction::class);
$app->map(['get'], '/articles/create', \Papyrus\Http\Actions\Simple\Articles\FormCreateArticleAction::class);
$app->map(['post'], '/articles/create', \Papyrus\Http\Actions\Simple\Articles\CreateArticleAction::class);
$app->map(['post'], '/articles/compare', \Papyrus\Http\Actions\Simple\Articles\CompareArticlesAction::class);

$app->get('/api/v1/articles/list', \Papyrus\Http\Actions\ApiV1\Articles\ListArticlesAction::class);

// Админка

$app->get('/admin/', \Papyrus\Http\Actions\DashboardAction::class);
$app->get('/admin/pages/show/{id:[0-9]+}', \Papyrus\Http\Actions\Pages\ShowPageAction::class);
$app->get('/admin/pages/edit/{id:[0-9]+}', \Papyrus\Http\Actions\Pages\EditPageAction::class);
$app->post('/admin/pages/edit/{id:[0-9]+}', \Papyrus\Http\Actions\Pages\EditPageAction::class);
$app->get('/admin/pages/{pageId:[0-9]+}/articles/history/{articlePid}',
    \Papyrus\Http\Actions\Articles\HistoryArticleAction::class);
$app->get('/admin/pages/{pageId:[0-9]+}/articles/show/{articlePid}',
    \Papyrus\Http\Actions\Articles\ShowArticleAction::class);
$app->get('/admin/pages/{pageId:[0-9]+}/articles/create',
    \Papyrus\Http\Actions\Articles\CreateArticleAction::class);
$app->get('/admin/pages/{pageId:[0-9]+}/articles/edit/{articlePid}',
    \Papyrus\Http\Actions\Articles\EditArticleAction::class);
$app->post('/admin/pages/{pageId:[0-9]+}/articles/edit/{articlePid}',
    \Papyrus\Http\Actions\Articles\EditArticleAction::class);
$app->post('/admin/pages/{pageId:[0-9]+}/articles/compare',
    \Papyrus\Http\Actions\Articles\CompareArticlesAction::class);