<?php

function root(): string
{
    return ROOT;
}

function resources(): string
{
    return ROOT . '/resources';
}

function web(): string
{
    return ROOT . '/public';
}

function storage(): string
{
    return ROOT . '/storage';
}

function tests(): string
{
    return ROOT . '/tests';
}