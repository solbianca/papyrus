<?php

use Papyrus\Application\Kernel\Kernel;
use Papyrus\Application\Kernel\Application;

function app(): Application
{
    return Kernel::application();
}

function container(): \DI\Container
{
    return Kernel::application()->getContainer();
}

/**
 * @param string $key
 * @param null $default
 * @return mixed|null
 * @throws \DI\DependencyException
 * @throws \DI\NotFoundException
 * @throws \Papyrus\Application\Config\ConfigException
 */
function config(string $key, $default = null)
{
    $config = container()->get(\Papyrus\Application\Config\Config::class);
    return $config->get($key, $default);
}

function atlas(): \Atlas\Orm\Atlas
{
    return container()->get('atlas');
}

function request(): \Psr\Http\Message\ServerRequestInterface
{
    return container()->get('request');
}

function response(): \Psr\Http\Message\ResponseInterface
{
    return container()->get('response');
}

function redirect(string $uri, int $status = 302)
{
    return response()->withStatus(302)->withHeader('Location', $uri);
}

function writer(): \Papyrus\Application\Writer\WriterContract
{
    return container()->get(\Papyrus\Application\Writer\WriterContract::class);
}

function repositoryFactory()
{
    return container()->get(\Papyrus\Application\Repositories\RepositoryFactory::class);
}