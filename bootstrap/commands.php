<?php

return [
    \Papyrus\Commands\Phinx\MigrateCommand::class,
    \Papyrus\Commands\Phinx\RollbackCommand::class,
    \Papyrus\Commands\Phinx\RerunCommand::class,
    \Papyrus\Commands\Atlas\GenerateCommand::class,
    \Papyrus\Commands\Seed\CreateCommand::class,
    \Papyrus\Commands\Seed\RunCommand::class,
    \Papyrus\Commands\Database\DumpCommand::class,
    \Papyrus\Commands\Tests\DbInitCommand::class,
];