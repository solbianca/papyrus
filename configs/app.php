<?php

return [
    'name' => 'Papyrus Application',
    'api_version' => '1',
    'timezone' => 'Europe/Moscow',

    'view' => [
        'template_path' => __DIR__ . '/../resources/templates/',
        'widgets' => [
            'namespace' => 'Papyrus\Http\Widgets',
            'postfix' => 'Widget',
        ],
    ],

    'cache' => [
        'path' => __DIR__ . '/../storage/cache',
    ],

    'security' => [
        'rsa_private_key' => __DIR__ . '/../private.key',
        'rsa_public_key' => __DIR__ . '/../public.key',
    ],
    
    'session' => [
        
    ]
];