<?php

return [
    'name' => 'Papyrus',
    'file' => __DIR__ . '/../storage/logs/log.txt',
    'level' => \Monolog\Logger::DEBUG,
];