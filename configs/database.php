<?php

return [
    'papyrus' => [
        'pdo' => [
            'mysql:dbname=' . env('DB_NAME') . ';host=' . env('DB_HOST') . ';port=3306;charset=utf8',
            env('DB_USER'),
            env('DB_PASSWORD'),
        ],
        'namespace' => 'Papyrus\\Persistent',
        'directory' => './app/Persistent',
    ],
];