<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $user = $this->fetchRow("select * from users where username = 'johndoe'");
        if ($user) {
            return;
        }

        $data = [
            'username' => 'johndoe',
            'password' => md5('qwerty'),
            'email' => 'johndoe@example.com',
            'created_at' => date('Y-m-d H:i:s'),
        ];
        $this->insert('users', $data);

        $data = [];
        for ($i = 0; $i < 100; $i++) {
            $data[] = [
                'username' => $faker->userName,
                'password' => md5($faker->password),
                'email' => $faker->email,
                'created_at' => date('Y-m-d H:i:s'),
            ];
        }

        $this->insert('users', $data);
    }
}
