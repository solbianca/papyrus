<?php

use Phinx\Seed\AbstractSeed;

class ArticlesSeeder extends AbstractSeed
{
    private $statuses = ['published', 'hide'];

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $pagesData = [
            'id' => 1,
            'title' => 'Тестовая страница',
            'path' => '/test',
        ];
        $this->insert('pages', $pagesData);

        $faker = Faker\Factory::create();
        $pidGenerator = new \Papyrus\Domain\Articles\PidGenerator();
        $data = [];
        for ($i = 0; $i < 10; $i++) {
            $data[] = [
                'pid' => $pidGenerator->generate(),
                'page_id' => 1,
                'author' => $faker->name,
                'version' => 1,
                'title' => $faker->realText(100, 2),
                'body' => '<p>' . implode('<br>', $faker->paragraphs(5)) . '</p>',
                'status' => $this->statuses[mt_rand(0, count($this->statuses) - 1)],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        $this->insert('articles', $data);
    }
}