<?php


use Phinx\Migration\AbstractMigration;

class Init extends AbstractMigration
{
    public function up()
    {
        $this->table('users')
            ->addColumn('username', 'string', ['limit' => 30])
            ->addColumn('email', 'string', ['limit' => 255])
            ->addColumn('password', 'string', ['limit' => 64])
            ->addColumn('status', 'enum', ['default' => 'active', 'values' => ['active']])
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addIndex(['email'], ['unique' => true])
            ->create();

        $this->table('pages')
            ->addColumn('title', 'string', ['limit' => 256, 'comment' => 'Заголовок для страницы'])
            ->addColumn('path', 'string',
                ['limit' => 2048, 'comment' => 'Url-путь  страницы к которой будут привязаны статьи'])
            ->addColumn('status', 'enum', ['default' => 'active', 'values' => ['active']])
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->create();

        $this->table('articles')
            ->addColumn('pid', 'string', ['limit' => 16, 'comment' => 'Публичный id записи'])
            ->addColumn('page_id', 'integer', ['comment' => 'Id страницы к которой принадлежит статья'])
            ->addColumn('version', 'integer', ['default' => 0, 'comment' => 'Версия статьи'])
            ->addColumn('weight', 'integer', ['default' => 100, 'comment' => 'Вес статьи'])
            ->addColumn('status', 'enum', ['default' => 'hide', 'values' => ['published', 'hide']])
            ->addColumn('title', 'string', ['limit' => 256, 'comment' => 'Заголовок статьи'])
            ->addColumn('body', 'text', ['comment' => 'Основной контент статьи'])
            ->addColumn('author', 'string', ['limit' => 256, 'default' => null, 'comment' => 'Имя автора публикации'])
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('updated_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addIndex(['page_id'])
            ->create();
    }

    public function down()
    {
        $this->table('users')->drop()->save();
        $this->table('pages')->drop()->save();
        $this->table('articles')->drop()->save();
    }
}
