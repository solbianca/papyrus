<?php


namespace Papyrus\Providers;


use DI\Container;
use Papyrus\Application\Kernel\ServiceProviderContract;

class RendererProvider implements ServiceProviderContract
{
    /**
     * {@inheritdoc}
     */
    public function register(Container $container)
    {
        $config = $container->get(\Papyrus\Application\Config\Config::class);

        $container->set(\Slim\Views\PhpRenderer::class, function () use ($config) {
            return new \Slim\Views\PhpRenderer($config->get('app:renderer:template_path'));
        });

        $container->set('renderer', function () use ($container) {
            return $container->get(\Slim\Views\PhpRenderer::class);
        });
    }

}