<?php


namespace Papyrus\Providers;


use Atlas\Orm\Atlas;
use DI\Container;
use Papyrus\Application\Kernel\ServiceProviderContract;
use Papyrus\Application\Writer\Writer;
use Papyrus\Application\Writer\WriterContract;

class WriterProvider implements ServiceProviderContract
{
    public function register(Container $container)
    {
        $container->set(WriterContract::class, function () use ($container) {
            return new Writer($container->get(Atlas::class));
        });

        $container->set(Writer::class, function () use ($container) {
            return new Writer($container->get(Atlas::class));
        });

        $container->set('writer', function () use ($container) {
            return $container->get(WriterContract::class);
        });
    }

}