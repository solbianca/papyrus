<?php


namespace Papyrus\Providers;


use DI\Container;
use Papyrus\Application\Kernel\ServiceProviderContract;
use Papyrus\Application\Repositories\RepositoryFactory;

class RepositoryFactoryServiceProvider implements ServiceProviderContract
{
    public function register(Container $container)
    {
        $container->set(RepositoryFactory::class, function () {
            return new RepositoryFactory();
        });
    }

}