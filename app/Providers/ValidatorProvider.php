<?php


namespace Papyrus\Providers;


use DI\Container;
use Papyrus\Application\Kernel\ServiceProviderContract;
use Papyrus\Application\Validator\Contracts\ValidatorContract;
use Papyrus\Application\Validator\SymfonyValidatorAdapter;
use Symfony\Component\Validator\ValidatorBuilder;

class ValidatorProvider implements ServiceProviderContract
{
    public function register(Container $container)
    {
        $container->set(ValidatorContract::class, function () {
            $validator = (new ValidatorBuilder())->getValidator();
            return new SymfonyValidatorAdapter($validator);
        });

        $container->set('validator', function () use ($container) {
            return $container->get(ValidatorContract::class);
        });
    }

}