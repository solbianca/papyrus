<?php


namespace Papyrus\Providers;


use DI\Container;
use Papyrus\Application\Config\Config;
use Papyrus\Application\Kernel\ServiceProviderContract;
use Papyrus\Application\Twig\WidgetLoader;
use Slim\Views\Twig;
use Twig_Extension_Debug;

class TwigProvider implements ServiceProviderContract
{
    public function register(Container $container)
    {
        $config = $container->get(\Papyrus\Application\Config\Config::class);

        $twig = new \Slim\Views\Twig($config->get('app:view:template_path'), [
            'cache' => false,
            'debug' => true,
        ]);

        $this->registerWidgetFunction($twig, $container, $config);

        $router = $container->get('router');
        $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
        $twig->addExtension(new \Slim\Views\TwigExtension($router, $uri));
        $twig->addExtension(new Twig_Extension_Debug());

        $container->set('twig', function () use ($twig) {
            return $twig;
        });
        $container->set(Twig::class, function () use ($container) {
            return $container->get('twig');
        });
    }

    private function registerWidgetFunction(Twig $twig, Container $container, Config $config)
    {
        $widgetLoader = new WidgetLoader($container);
        $widgetLoader->setPostfix($config->get('app:view:widgets:postfix'));
        $widgetLoader->setNamespace($config->get('app:view:widgets:namespace'));

        $widgetFunction = new \Twig_Function('widget', function (string $classname) use ($widgetLoader) {
            $widget = $widgetLoader->load($classname);
            return $widget->render();
        }, ['is_safe' => ['html' => true]]);

        $twig->getEnvironment()->addFunction($widgetFunction);
    }
}