<?php


namespace Papyrus\Providers;


use DI\Container;
use Papyrus\Application\Kernel\ServiceProviderContract;
use Papyrus\Application\Session\Session;
use Papyrus\Application\Session\SessionBootstrapper;

final class SessionServiceProvider implements ServiceProviderContract
{

    public function register(Container $container)
    {
        $config = $container->get(\Papyrus\Application\Config\Config::class);
        
        $settings = $config->get('app:session', []);

        $this->bootstrapSession($settings);

        $session = new Session();
        $container->set(Session::class, function () use ($session) {
            return $session;
        });

        $container->set('session', function () use ($session) {
            return $session;
        });
    }

    private function bootstrapSession(array $settings)
    {
        (new SessionBootstrapper($settings))->bootstrap();
    }
}