<?php


namespace Papyrus\Providers;


use DI\Container;
use Papyrus\Application\Kernel\ServiceProviderContract;
use Papyrus\Application\Sanitizer\Sanitizer;
use Papyrus\Application\Sanitizer\SanitizerContract;

class SanitizerProvider implements ServiceProviderContract
{
    public function register(Container $container)
    {
        $container->set(SanitizerContract::class, function () {
            return new Sanitizer();
        });

        $container->set('sanitizer', function () use ($container) {
            return $container->get(SanitizerContract::class);
        });
    }

}