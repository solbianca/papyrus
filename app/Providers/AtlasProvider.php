<?php


namespace Papyrus\Providers;


use DI\Container;
use Papyrus\Application\Kernel\ServiceProviderContract;

class AtlasProvider implements ServiceProviderContract
{
    /**
     * {@inheritdoc}
     */
    public function register(Container $container)
    {
        $config = $container->get(\Papyrus\Application\Config\Config::class);

        $container->set(\Atlas\Orm\Atlas::class, function () use ($config) {
            $pdo = $config->get('database:papyrus:pdo');
            return \Atlas\Orm\Atlas::new($pdo[0], $pdo[1], $pdo[2]);
        });

        $container->set('atlas', function () use ($container) {
            return $container->get(\Atlas\Orm\Atlas::class);
        });
    }
}