<?php


namespace Papyrus\Providers;


use DI\Container;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Papyrus\Application\Kernel\ServiceProviderContract;
use Psr\Log\LoggerInterface;

class LoggerProvider implements ServiceProviderContract
{
    /**
     * {@inheritdoc}
     */
    public function register(Container $container)
    {
        $config = $container->get(\Papyrus\Application\Config\Config::class);

        $container->set(\Monolog\Logger::class, function () use ($config) {
            $logger = new Logger($config->get('log:name'));
            $logger->pushProcessor(new UidProcessor());
            $logger->pushHandler(new StreamHandler($config->get('log:file'), $config->get('log:level')));
            return $logger;
        });

        $container->set('logger', function () use ($container) {
            return $container->get(Logger::class);
        });

        $container->set(LoggerInterface::class, function () use ($container) {
            return $container->get(Logger::class);
        });
    }
}