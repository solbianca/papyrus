<?php
/**
 * This file was generated by Atlas. Changes will be overwritten.
 */
declare(strict_types=1);

namespace Papyrus\Persistent\Migration;

use Atlas\Table\Table;

/**
 * @method MigrationRow|null fetchRow($primaryVal)
 * @method MigrationRow[] fetchRows(array $primaryVals)
 * @method MigrationTableSelect select(array $whereEquals = [])
 * @method MigrationRow newRow(array $cols = [])
 * @method MigrationRow newSelectedRow(array $cols)
 */
class MigrationTable extends Table
{
    const DRIVER = 'mysql';

    const NAME = 'migrations';

    const COLUMNS = [
        'version' => [
            'name' => 'version',
            'type' => 'bigint',
            'size' => 19,
            'scale' => 0,
            'notnull' => true,
            'default' => null,
            'autoinc' => false,
            'primary' => true,
            'options' => null,
        ],
        'migration_name' => [
            'name' => 'migration_name',
            'type' => 'varchar',
            'size' => 100,
            'scale' => null,
            'notnull' => false,
            'default' => null,
            'autoinc' => false,
            'primary' => false,
            'options' => null,
        ],
        'start_time' => [
            'name' => 'start_time',
            'type' => 'timestamp',
            'size' => null,
            'scale' => null,
            'notnull' => false,
            'default' => null,
            'autoinc' => false,
            'primary' => false,
            'options' => null,
        ],
        'end_time' => [
            'name' => 'end_time',
            'type' => 'timestamp',
            'size' => null,
            'scale' => null,
            'notnull' => false,
            'default' => null,
            'autoinc' => false,
            'primary' => false,
            'options' => null,
        ],
        'breakpoint' => [
            'name' => 'breakpoint',
            'type' => 'tinyint',
            'size' => 3,
            'scale' => 0,
            'notnull' => true,
            'default' => 0,
            'autoinc' => false,
            'primary' => false,
            'options' => null,
        ],
    ];

    const COLUMN_NAMES = [
        'version',
        'migration_name',
        'start_time',
        'end_time',
        'breakpoint',
    ];

    const COLUMN_DEFAULTS = [
        'version' => null,
        'migration_name' => null,
        'start_time' => null,
        'end_time' => null,
        'breakpoint' => 0,
    ];

    const PRIMARY_KEY = [
        'version',
    ];

    const AUTOINC_COLUMN = null;

    const AUTOINC_SEQUENCE = null;
}
