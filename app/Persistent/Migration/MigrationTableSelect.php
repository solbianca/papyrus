<?php
declare(strict_types=1);

namespace Papyrus\Persistent\Migration;

use Atlas\Table\TableSelect;

/**
 * @method MigrationRow|null fetchRow()
 * @method MigrationRow[] fetchRows()
 */
class MigrationTableSelect extends TableSelect
{
}
