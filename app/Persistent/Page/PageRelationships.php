<?php
declare(strict_types = 1);

namespace Papyrus\Persistent\Page;

use Atlas\Mapper\MapperRelationships;
use Papyrus\Persistent\Article\Article;

class PageRelationships extends MapperRelationships
{
    protected function define()
    {
        $this->oneToMany('articles', Article::class, ['id' => 'page_id']);
    }
}
