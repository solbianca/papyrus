<?php
declare(strict_types=1);

namespace Papyrus\Persistent\Page;

use Atlas\Table\TableSelect;

/**
 * @method PageRow|null fetchRow()
 * @method PageRow[] fetchRows()
 */
class PageTableSelect extends TableSelect
{
}
