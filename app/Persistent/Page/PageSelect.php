<?php
declare(strict_types=1);

namespace Papyrus\Persistent\Page;

use Atlas\Mapper\MapperSelect;

/**
 * @method PageRecord|null fetchRecord()
 * @method PageRecord[] fetchRecords()
 * @method PageRecordSet fetchRecordSet()
 */
class PageSelect extends MapperSelect
{
}
