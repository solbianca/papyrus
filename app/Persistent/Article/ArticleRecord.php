<?php
declare(strict_types = 1);

namespace Papyrus\Persistent\Article;

use Atlas\Mapper\Record;

/**
 * @method ArticleRow getRow()
 */
class ArticleRecord extends Record
{
    use ArticleFields;

    /**
     * @var ArticleRecord
     */
    public $draft;
}
