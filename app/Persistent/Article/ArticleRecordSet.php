<?php
declare(strict_types = 1);

namespace Papyrus\Persistent\Article;

use Atlas\Mapper\RecordSet;

/**
 * @method ArticleRecord offsetGet($offset)
 * @method ArticleRecord appendNew(array $fields = [])
 * @method ArticleRecord|null getOneBy(array $whereEquals)
 * @method ArticleRecordSet getAllBy(array $whereEquals)
 * @method ArticleRecord|null detachOneBy(array $whereEquals)
 * @method ArticleRecordSet detachAllBy(array $whereEquals)
 * @method ArticleRecordSet detachAll()
 * @method ArticleRecordSet detachDeleted()
 */
class ArticleRecordSet extends RecordSet
{
    /**
     * @param mixed $key
     * @return ArticleRecordSet
     */
    public function keyBy($key)
    {
        $iterator = $this->getIterator();
        $this->detachAll();
        foreach ($iterator as $record) {
            if (isset($record->{$key})) {
                $this[$record->{$key}] = $record;
            }
        }
        return $this;
    }
}
