<?php
declare(strict_types=1);

namespace Papyrus\Persistent\Article;

use Atlas\Table\TableSelect;

/**
 * @method ArticleRow|null fetchRow()
 * @method ArticleRow[] fetchRows()
 */
class ArticleTableSelect extends TableSelect
{
}
