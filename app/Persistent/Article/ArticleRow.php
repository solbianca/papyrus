<?php
/**
 * This file was generated by Atlas. Changes will be overwritten.
 */
declare(strict_types=1);

namespace Papyrus\Persistent\Article;

use Atlas\Table\Row;

/**
 * @property mixed $id int(10,0) NOT NULL
 * @property mixed $pid varchar(16) NOT NULL
 * @property mixed $page_id int(10,0) NOT NULL
 * @property mixed $version int(10,0) NOT NULL
 * @property mixed $weight int(10,0) NOT NULL
 * @property mixed $status enum(9) NOT NULL
 * @property mixed $title varchar(256) NOT NULL
 * @property mixed $body text(65535) NOT NULL
 * @property mixed $author varchar(256) NOT NULL
 * @property mixed $created_at timestamp NOT NULL
 * @property mixed $updated_at timestamp NOT NULL
 */
class ArticleRow extends Row
{
    protected $cols = [
        'id' => null,
        'pid' => null,
        'page_id' => null,
        'version' => 0,
        'weight' => 100,
        'status' => 'hide',
        'title' => null,
        'body' => null,
        'author' => null,
        'created_at' => null,
        'updated_at' => null,
    ];
}
