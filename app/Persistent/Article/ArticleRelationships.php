<?php
declare(strict_types = 1);

namespace Papyrus\Persistent\Article;

use Atlas\Mapper\MapperRelationships;
use Papyrus\Persistent\Page\Page;

class ArticleRelationships extends MapperRelationships
{
    protected function define()
    {
        $this->manyToOne('page', Page::class, ['page_id' => 'id',]);
    }
}
