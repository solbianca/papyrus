<?php


namespace Papyrus\Application\Session;


class SessionBootstrapper
{
    /**
     * @var array
     */
    private $defaults = [
        'lifetime' => 600, // 10 минут
        'path' => '/',
        'domain' => null,
        'secure' => false,
        'httponly' => false,
        'name' => 'app_session',
        'autorefresh' => false,
        'handler' => null,
        'ini_settings' => [],
    ];

    /**
     * @var array
     */
    private $settings;

    public function __construct(array $settings)
    {
        $settings = array_merge($this->defaults, $settings);
        $this->settings = $settings;
    }

    public function bootstrap()
    {
        $this->iniSet($this->settings['ini_settings']);
        // Just override this, to ensure package is working
        if (ini_get('session.gc_maxlifetime') < $this->settings['lifetime']) {
            $this->iniSet([
                'session.gc_maxlifetime' => $this->settings['lifetime'] * 2,
            ]);
        }

        if ($this->isSessionActive()) {
            return;
        }

        $this->setCookieParams();

        $name = $this->settings['name'];
        if ($this->settings['autorefresh'] && isset($_COOKIE[$name])) {
            $this->refreshSessionCookie($name);
        }

        session_name($name);

        $this->initSessionHandler();

        session_cache_limiter(false);
        session_start();
    }

    private function isSessionActive(): bool
    {
        return session_status() === PHP_SESSION_ACTIVE;
    }

    private function setCookieParams()
    {
        session_set_cookie_params(
            $this->settings['lifetime'],
            $this->settings['path'],
            $this->settings['domain'],
            $this->settings['secure'],
            $this->settings['httponly']
        );
    }

    private function refreshSessionCookie(string $name)
    {
        setcookie(
            $name,
            $_COOKIE[$name],
            time() + $this->settings['lifetime'],
            $this->settings['path'],
            $this->settings['domain'],
            $this->settings['secure'],
            $this->settings['httponly']
        );
    }

    private function initSessionHandler()
    {
        $handler = $this->settings['handler'];

        if (!$handler) {
            return;
        }

        if (!is_callable($handler) || !is_object($handler)) {
            throw new \RuntimeException("Session handler must be object or callable");
        }

        if (is_callable($handler)) {
            $handler = call_user_func($handler);
        }

        if (!($handler instanceof \SessionHandlerInterface)) {
            throw new \RuntimeException("Session handler must implement `\SessionHandlerInterface`");
        }

        session_set_save_handler($handler, true);
    }

    protected function iniSet($settings)
    {
        foreach ($settings as $key => $val) {
            if (strpos($key, 'session.') === 0) {
                ini_set($key, $val);
            }
        }
    }
}