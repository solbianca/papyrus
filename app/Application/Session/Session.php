<?php


namespace Papyrus\Application\Session;


class Session
{
    /**
     * @param string $key
     * @param mixed $default
     *
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        return $this->exists($key)
            ? $_SESSION[$key]
            : $default;
    }

    /**
     * @param string $key
     * @param mixed $value
     *
     * @return Session
     */
    public function set(string $key, $value): Session
    {
        $_SESSION[$key] = $value;
        return $this;
    }

    /**
     * @param string $key
     *
     * @return Session
     */
    public function delete($key): Session
    {
        if ($this->exists($key)) {
            unset($_SESSION[$key]);
        }
        return $this;
    }

    public function clear(): Session
    {
        $_SESSION = [];
        return $this;
    }

    public function exists(string $key): bool
    {
        return array_key_exists($key, $_SESSION);
    }

    public function getId(): string
    {
        return session_id() ?: '';
    }

    public function regenerateId(): bool
    {
        if (session_id()) {
            return session_regenerate_id(true);
        }

        return false;
    }

    /**
     * Destroy the session.
     */
    public function destroy()
    {
        if (!$this->getId()) {
            return;
        }

        session_unset();
        session_destroy();
        session_write_close();

        if (ini_get('session.use_cookies')) {
            $params = session_get_cookie_params();
            setcookie(
                session_name(),
                '',
                time() - 4200,
                $params['path'],
                $params['domain'],
                $params['secure'],
                $params['httponly']
            );
        }
    }
}