<?php


namespace Papyrus\Application\Console;


use Papyrus\Application\Application;
use Symfony\Component\Console\Application;

class ConsoleKernel
{
    /**
     * @var ConsoleKernel
     */
    private static $instance;

    /**
     * @var Application
     */
    private $application;

    /**
     * @var Application
     */
    private $consoleApplication;

    private function __construct()
    {

    }

    public static function init(): ConsoleKernel
    {
        if (null === static::$instance) {
            static::$instance = new static();
            static::$instance->consoleApplication = new Application();
        }
        return static::$instance;
    }

    public function setCommands(array $commands): ConsoleKernel
    {
        foreach ($commands as $loadedCommand) {
            $command = null;
            if (is_string($loadedCommand)) {
                $command = $this->application->getContainer()->make($loadedCommand);
            } elseif (is_object($loadedCommand)) {
                $command = $loadedCommand;
            } else {
                throw new \RuntimeException("Command must be string or object");
            }
            $this->consoleApplication->add($command);
        }
        return $this;
    }

    public function setApplication(Application $app): ConsoleKernel
    {
        $this->application = $app;
        return $this;
    }


    public function handle()
    {
        $this->consoleApplication->run();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}