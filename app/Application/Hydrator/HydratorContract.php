<?php


namespace Papyrus\Application\Hydrator;


interface HydratorContract
{
    /**
     * Creates an instance of a class filled with data according to map
     *
     * @param string|object $object Object or class name to hydrate
     * @param array $data Array represented as ['propertyName' => 'propertyValue]
     * @return object Object with filling properties
     *
     * @throws HydratorException
     */
    public function hydrate($object, array $data);

    /**
     * Extracts data from an object according to map
     *
     * @param object $object Object you need to extract data
     * @param array $properties Properties for extraction. If not set extract data from all properties
     * @return array
     */
    public function extract($object, array $properties = []);

}