<?php


namespace Papyrus\Application\Repositories;


use Atlas\Mapper\MapperSelect;
use Atlas\Mapper\Record;
use Atlas\Mapper\RecordSet;
use Atlas\Orm\Atlas;

abstract class Repository
{
    /**
     * @var Atlas
     */
    protected $atlas;

    /**
     * @var string
     */
    protected $mapper = '';

    public function __construct(Atlas $atlas)
    {
        $this->atlas = $atlas;
    }

    protected function getMapper(): string
    {
        return $this->mapper;
    }

    public function select(): MapperSelect
    {
        return $this->atlas->select($this->getMapper());
    }

    public function findById(int $id, array $with = []): ?Record
    {
        return $this->atlas->fetchRecord($this->getMapper(), $id, $with);
    }

    public function findBy(... $conditions): RecordSet
    {
        if (!$conditions || (isset($conditions[0]) && $conditions[0] === [])) {
            throw new \RuntimeException("Select query condition cant be empty");
        }

        $select = $this->select();

        if (is_array($conditions[0])) {
            foreach ($conditions as $condition) {
                $select = $this->handleFindByCondition($condition, $select);
            }
        } else {
            $select = $this->handleFindByCondition($conditions, $select);
        }

        return $select->fetchRecordSet();
    }

    protected function handleFindByCondition(array $condition, MapperSelect $mapperSelect)
    {
        $count = count($condition);

        if ($count === 1 && is_string($condition[0])) {
            $mapperSelect->where($condition[0]);
        } elseif ($count === 2 && is_string($condition[0])) {
            $mapperSelect->where($condition[0], $condition[1]);
        } elseif ($this->isFindByConditionsByThreeParametersValid($condition)) {
            if ($condition[0] === 'or') {
                $mapperSelect->orWhere($condition[1], $condition[2]);
            } else {
                $mapperSelect->where($condition[1], $condition[2]);
            }
        } else {
            throw new \RuntimeException("Invalid select query condition");
        }

        return $mapperSelect;
    }

    protected function isFindByConditionsByThreeParametersValid(array $condition): bool
    {
        return count($condition) === 3
            && is_string($condition[0])
            && is_string($condition[1])
            && in_array($condition[0], ['and', 'or']);
    }

    public function isExistById(int $id): bool
    {
        $count = $this->select()->where('id =', $id)->fetchCount('id');
        return (int)$count === 1 ? true : false;
    }
}