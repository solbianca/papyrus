<?php


namespace Papyrus\Application\Repositories;


class RepositoryFactory
{
    public static function create(string $class): Repository
    {
        if (!class_exists($class)) {
            throw new \RuntimeException("Class `{$class}` not exist");
        }

        if (!is_subclass_of($class, Repository::class)) {
            $parent = Repository::class;
            throw new \RuntimeException("Class `{$class}` must extends `{$parent}`");
        }

        return container()->make($class);
    }
}