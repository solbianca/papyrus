<?php


namespace Papyrus\Application\Sanitizer;


use Closure;
use Papyrus\Application\Sanitizer\Rules\RuleContract;

class Sanitizer implements SanitizerContract
{
    /**
     * Data to sanitize
     *
     * @var array
     */
    private $data;

    /**
     * Filters to apply
     *
     * @var array
     */
    private $rules;

    /**
     * Sanitize the given data
     *
     * @param array $data
     * @param array $rules
     * @return array
     */
    public function sanitize(array $data, array $rules): array
    {
        $this->data = $data;
        $this->rules = $rules;

        $sanitized = [];
        foreach ($this->data as $attribute => $value) {
            if (key_exists($attribute, $rules)) {
                $sanitized[$attribute] = $this->sanitizeAttribute($attribute, $value);
            } else {
                $sanitized[$attribute] = $value;
            }
        }
        return $sanitized;
    }

    /**
     *  Sanitize the given attribute
     *
     * @param  string $attribute Attribute name
     * @param  mixed $value Attribute value
     * @return mixed   Sanitized value
     */
    private function sanitizeAttribute($attribute, $value)
    {
        if (isset($this->rules[$attribute])) {
            foreach ($this->rules[$attribute] as $rule) {
                $value = $this->applyRule($rule, $value);
            }
        }
        return $value;
    }

    /**
     * Apply the given filter by its name
     *
     * @param Closure|RuleContract $rule
     * @param $value
     * @return mixed
     * @throws \RuntimeException
     */
    private function applyRule($rule, $value)
    {
        if ($rule instanceof Closure) {
            return call_user_func_array($rule, [$value]);
        } elseif ($rule instanceof RuleContract) {
            return $rule->apply($value);
        } else {
            throw new \RuntimeException("Rule must be closure or implement `\Papyrus\Application\Sanitizer\Rules\RuleInterface` or be `Closure`");
        }
    }
}