<?php


namespace Papyrus\Application\Sanitizer;


interface SanitizerContract
{
    public function sanitize(array $data, array $rules): array;
}