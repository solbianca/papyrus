<?php


namespace Papyrus\Application\Sanitizer\Rules;


final class Capitalize implements RuleContract
{
    /**
     *  Capitalize the given string.
     *
     * @param  string $value
     * @return string
     */
    public function apply($value)
    {
        return is_string($value) ? ucwords(strtolower($value)) : $value;
    }
}