<?php


namespace Papyrus\Application\Sanitizer\Rules;


final class All implements RuleContract
{
    /**
     * @var RuleContract[]
     */
    private $rules = [];

    public function __construct(array $rules)
    {
        $this->rules = $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function apply($value)
    {
        if (!is_array($value)) {
            return $value;
        }

        $sanitisedValue = [];

        foreach ($value as $key => $item) {
            $sanitisedValue[$key] = $this->applyRules($item);
        }

        return $sanitisedValue;
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function applyRules($value)
    {
        $sanitisedItem = $value;
        foreach ($this->rules as $rule) {
            $sanitisedItem = $rule->apply($value);
        }
        return $sanitisedItem;
    }
}