<?php


namespace Papyrus\Application\Sanitizer\Rules;


final class Cast implements RuleContract
{
    /**
     * @var string
     */
    private $type;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    /**
     *  Typecast given attribute
     *
     * @param mixed $value
     * @return string
     */
    public function apply($value)
    {
        switch ($this->type) {
            case 'int':
            case 'integer':
                return (int)$value;
            case 'real':
            case 'float':
            case 'double':
                return (float)$value;
            case 'str':
            case 'string':
                return (string)$value;
            case 'bool':
            case 'boolean':
                return (bool)$value;
            default:
                $valueType = gettype($value);
                throw new \InvalidArgumentException("Wrong Sanitizer casting format: {\$type} is {$valueType}.");
        }
    }
}