<?php


namespace Papyrus\Application\Sanitizer\Rules;


final class Collection implements RuleContract
{
    /**
     * @var RuleContract[]
     */
    private $rules;

    public function __construct(array $rules)
    {
        $this->rules = $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function apply($value)
    {
        if (!is_array($value)) {
            return $value;
        }

        $sanitisedValue = [];

        foreach ($value as $key => $item) {
            if (isset($this->rules[$key])) {
                $sanitisedValue[$key] = $this->applyRules($item, $this->rules[$key]);
            } else {
                $sanitisedValue[$key] = $item;
            }
        }
        return $sanitisedValue;
    }

    /**
     * @param mixed $value
     * @param array $rules
     * @return mixed
     */
    public function applyRules($value, array $rules)
    {
        $sanitisedItem = $value;
        foreach ($rules as $rule) {
            $sanitisedItem = $rule->apply($value);
        }
        return $sanitisedItem;
    }
}