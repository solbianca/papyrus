<?php


namespace Papyrus\Application\Sanitizer\Rules;


interface RuleContract
{

    /**
     *  Return the result of applying this filter to the given input
     *
     * @param  mixed $value
     * @return mixed
     */
    public function apply($value);
}