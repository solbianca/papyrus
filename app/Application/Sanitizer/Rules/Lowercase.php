<?php


namespace Papyrus\Application\Sanitizer\Rules;


final class Lowercase implements RuleContract
{
    /**
     *  Lowercase the given string
     *
     * @param  string $value
     * @param  array $options
     * @return string
     */
    public function apply($value, array $options = [])
    {
        return is_string($value) ? strtolower($value) : $value;
    }
}