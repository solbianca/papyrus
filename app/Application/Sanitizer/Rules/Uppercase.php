<?php


namespace Papyrus\Application\Sanitizer\Rules;


final class Uppercase implements RuleContract
{
    /**
     *  Uppercase the given string
     *
     * @param  string $value
     * @param  array $options
     * @return string
     */
    public function apply($value, array $options = [])
    {
        return is_string($value) ? strtoupper($value) : $value;
    }
}