<?php


namespace Papyrus\Application\Validator;


use Papyrus\Application\Validator\Contracts\ValidationErrorsBagContract;
use Papyrus\Application\Validator\Contracts\ValidationResultContract;

class ValidationResult implements ValidationResultContract
{
    /**
     * @var ValidationErrorsBagContract
     */
    private $errorsBag;

    public function __construct(ValidationErrorsBagContract $errorsBag)
    {
        $this->errorsBag = $errorsBag;
    }

    public function passed(): bool
    {
        return $this->getErrorsBag()->isEmpty();
    }

    public function failed(): bool
    {
        return !$this->passed();
    }

    public function getErrorsBag(): ValidationErrorsBagContract
    {
        return $this->errorsBag;
    }


}