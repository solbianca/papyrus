<?php


namespace Papyrus\Application\Validator;

use Papyrus\Application\Validator\Contracts\ErrorMessageContract;
use Papyrus\Application\Validator\Contracts\ValidationErrorsBagContract;

class ValidationErrorsBag implements ValidationErrorsBagContract
{
    /**
     * @var ErrorMessageContract[]
     */
    private $messages;

    /**
     * {@inheritdoc}
     */
    public function all(): array
    {
        return $this->messages;
    }

    /**
     * {@inheritdoc}
     */
    public function add(ErrorMessageContract $message): ValidationErrorsBagContract
    {
        $this->messages[] = $message;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty(): bool
    {
        return empty($this->messages);
    }

    public function count(): int
    {
        if ($this->messages) {
            return count($this->messages);
        }

        return 0;
    }
}