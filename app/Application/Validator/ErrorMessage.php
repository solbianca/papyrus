<?php


namespace Papyrus\Application\Validator;


use Papyrus\Application\Validator\Contracts\ErrorMessageContract;

class ErrorMessage implements ErrorMessageContract
{

    /**
     * @var string|null
     */
    private $message;

    /**
     * @var int|null
     */
    private $code = 422;

    /**
     * @var mixed
     */
    private $invalidValue;

    /**
     * @var string
     */
    private $propertyPath;

    public function __construct(?string $message, ?int $code, $invalidValue, string $propertyPath)
    {
        $this->message = $message;
        $this->code = $code;
        $this->invalidValue = $invalidValue;
        $this->propertyPath = $propertyPath;
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * {@inheritdoc}
     */
    public function getCode(): ?int
    {
        return $this->code;
    }

    /**
     * {@inheritdoc}
     */
    public function getInvalidValue()
    {
        return $this->invalidValue;
    }

    /**
     * {@inheritdoc}
     */
    public function getPropertyPath(): string
    {
        return $this->propertyPath;
    }
}