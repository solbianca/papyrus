<?php


namespace Papyrus\Application\Validator;

use Papyrus\Application\Validator\Contracts\ErrorMessageContract;
use Papyrus\Application\Validator\Contracts\ValidationErrorsBagContract;
use Papyrus\Application\Validator\Contracts\ValidationResultContract;
use Papyrus\Application\Validator\Contracts\ValidatorContract;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SymfonyValidatorAdapter implements ValidatorContract
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->errors = new ValidationErrorsBag();
    }

    public function validate(array $attributes, array $rules): ValidationResultContract
    {
        $assertsCollection = new Collection($rules);
        $constraintViolationList = $this->validator->validate($attributes, $assertsCollection);

        $errorsBag = new ValidationErrorsBag();
        foreach ($constraintViolationList as $violation) {
            /* @var \Symfony\Component\Validator\ConstraintViolationInterface $violation */
            $errorMessage = $this->createErrorMessage($violation);
            $errorsBag->add($errorMessage);
        }

        return $this->createResult($errorsBag);
    }

    /**
     * @param array $attributes
     * @param array $rules
     * @return ValidatorContract
     * @throws ValidationException
     */
    public function validateOrThrow(array $attributes, array $rules): ValidationResultContract
    {
        $result = $this->validate($attributes, $rules);
        if ($result->failed()) {
            $exception = new ValidationException();
            $exception->setValidationErrors($result->getErrorsBag());
            throw $exception;
        }
        return $result;
    }

    private function createResult(ValidationErrorsBagContract $errorsBag): ValidationResultContract
    {
        return new ValidationResult($errorsBag);
    }

    private function createErrorMessage(ConstraintViolationInterface $violation): ErrorMessageContract
    {
        $propertyPath = trim($violation->getPropertyPath(), '[]');
        $errorMessage = new ErrorMessage(
            $violation->getMessage(),
            (int)$violation->getCode(),
            $violation->getInvalidValue(),
            $propertyPath
        );
        return $errorMessage;
    }
}