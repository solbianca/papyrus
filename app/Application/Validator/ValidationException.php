<?php


namespace Papyrus\Application\Validator;


use Papyrus\Application\Validator\Contracts\ValidationErrorsBagContract;
use Papyrus\Application\Validator\Contracts\ValidationExceptionContract;

class ValidationException extends \Exception implements ValidationExceptionContract
{
    /**
     * @var ValidationErrorsBagContract
     */
    private $validationErrors;

    public function getValidationErrors(): ?ValidationErrorsBagContract
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(ValidationErrorsBagContract $validationErrors): void
    {
        $this->validationErrors = $validationErrors;
    }
}