<?php


namespace Papyrus\Application\Validator\Contracts;


use Papyrus\Application\Validator\ValidationException;

interface ValidatorContract
{
    public function validate(array $attributes, array $rules): ValidationResultContract;

    /**
     * @throws ValidationException
     */
    public function validateOrThrow(array $attributes, array $rules): ValidationResultContract;
}