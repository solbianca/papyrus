<?php


namespace Papyrus\Application\Validator\Contracts;


interface ErrorMessageContract
{
    /**
     * Returns the violation message
     *
     * @return string|null
     */
    public function getMessage(): ?string;

    /**
     * Returns a machine-digestible error code for the violation
     *
     * @return int|null
     */
    public function getCode(): ?int;

    /**
     * Returns the value that caused the violation
     *
     * @return mixed
     */
    public function getInvalidValue();

    /**
     * Returns the property path from the root element to the violation.
     *
     * @return string
     */
    public function getPropertyPath(): string;
}