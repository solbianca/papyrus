<?php


namespace Papyrus\Application\Validator\Contracts;


interface ValidationErrorsBagContract
{
    /**
     * @return ErrorMessageContract[]
     */
    public function all(): array;

    public function add(ErrorMessageContract $message): ValidationErrorsBagContract;

    /**
     * Checks if the bag is empty.
     *
     * @return boolean
     */
    public function isEmpty(): bool;

    /**
     * @return int
     */
    public function count(): int;
}