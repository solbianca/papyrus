<?php


namespace Papyrus\Application\Validator\Contracts;


interface ValidationExceptionContract extends \Throwable
{
    public function getValidationErrors(): ?ValidationErrorsBagContract;

    public function setValidationErrors(ValidationErrorsBagContract $validationErrors): void;
}