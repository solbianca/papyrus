<?php


namespace Papyrus\Application\Validator\Contracts;


interface ValidationResultContract
{
    public function passed(): bool;

    public function failed(): bool;

    public function getErrorsBag(): ValidationErrorsBagContract;
}