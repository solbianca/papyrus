<?php


namespace Papyrus\Application\Config;


interface ConfigInterface
{
    /**
     * Return value from config by key. Example $config->get('key1:subkey2:subsubkey3');
     *
     * @param string $key
     * @param mixed|null $defaultValue
     * @return mixed|null
     *
     * @throws ConfigException
     */
    public function get(string $key, $defaultValue = null);
}