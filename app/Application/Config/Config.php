<?php


namespace Papyrus\Application\Config;


class Config implements ConfigInterface
{
    /**
     * @var string
     */
    private $directory;

    /**
     * @var array
     */
    private $config = [];

    /**
     * @var string
     */
    private $delimiter;

    /**
     * Config constructor.
     * @param string $directory
     * @param string $delimiter
     */
    public function __construct(string $directory, string $delimiter = ':')
    {
        $this->directory = $directory;
        $this->delimiter = $delimiter;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key, $defaultValue = null)
    {
        $keys = explode($this->delimiter, $key);

        if (!$this->isKeyValid($keys)) {
            throw new ConfigException("Invalid key `{$key}`");
        }

        $file = $keys[0];
        unset($keys[0]);

        if (!$this->isConfigFileExist($this->path($file))) {
            throw new ConfigException("Config file `{$file}` not exist ");
        }

        return $this->getConfig($file, $keys, $defaultValue);
    }

    /**
     * @param mixed $keys
     * @return bool
     */
    private function isKeyValid($keys): bool
    {
        if (!is_array($keys)) {
            false;
        }

        if (empty($keys[0])) {
            return false;
        }

        return true;
    }

    private function isConfigFileExist($file): bool
    {
        return file_exists($file);
    }

    private function path(string $file): string
    {
        return $this->directory . '/' . $file . '.php';
    }

    private function getConfig($file, $keys, $defaultValue)
    {
        if (!array_key_exists($file, $this->config)) {
            $this->config[$file] = $this->loadFile($this->path($file));
        }

        if (count($keys) === 0) {
            return $this->config[$file];
        }

        $result = $defaultValue;
        $data = $this->config[$file];
        foreach ($keys as $k) {
            if (key_exists($k, $data)) {
                $data = $data[$k];
                $result = $data;
            } else {
                return $defaultValue;
            }
        }

        return $result;
    }

    private function loadFile(string $file): array
    {
        return include $file;
    }
}