<?php


namespace Papyrus\Application\Writer;


use Atlas\Mapper\Record;

interface WriterContract
{
    /**
     * @param string $mapper
     * @param array $fields
     * @return Record
     */
    public function newRecord(string $mapper, array $fields);

    /**
     * @param Record $record
     * @return Record
     */
    public function insertRecord(Record $record);

    /**
     * @param string $mapper
     * @param array $fields
     * @return Record
     */
    public function create(string $mapper, array $fields);

    /**
     * @param Record $record
     * @return void
     */
    public function update(Record $record);
}