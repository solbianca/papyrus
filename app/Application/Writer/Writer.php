<?php


namespace Papyrus\Application\Writer;


use Atlas\Mapper\Record;
use Atlas\Orm\Atlas;

class Writer implements WriterContract
{
    /**
     * @var Atlas
     */
    private $atlas;

    public function __construct(Atlas $atlas)
    {
        $this->atlas = $atlas;
    }

    /**
     * {@inheritdoc}
     */
    public function newRecord(string $mapper, array $fields)
    {
        return $this->atlas->newRecord($mapper, $fields);
    }

    /**
     * {@inheritdoc}
     */
    public function insertRecord(Record $record)
    {
        $this->atlas->insert($record);
        return $record;
    }

    /**
     * {@inheritdoc}
     */
    public function create(string $mapper, array $fields)
    {
        $record = $this->newRecord($mapper, $fields);
        return $this->insertRecord($record);
    }

    /**
     * {@inheritdoc}
     */
    public function update(Record $record)
    {
        $this->atlas->update($record);
    }
}