<?php


namespace Papyrus\Application\Http;


use Papyrus\Application\Sanitizer\SanitizerContract;
use Papyrus\Application\Validator\Contracts\ValidationErrorsBagContract;
use Papyrus\Application\Validator\Contracts\ValidationResultContract;
use Papyrus\Application\Validator\Contracts\ValidatorContract;
use Papyrus\Application\Validator\ValidationErrorsBag;
use Papyrus\Application\Validator\ValidationException;
use Papyrus\Application\Validator\ValidationResult;
use Psr\Http\Message\ServerRequestInterface as RequestContract;
use Slim\Http\Request;

abstract class ProcessedRequest implements ProcessedRequestContract
{
    /**
     * @var ValidatorContract
     */
    protected $validator;

    /**
     * @var SanitizerContract
     */
    protected $sanitizer;

    /**
     * @var RequestContract
     */
    protected $request;

    /**
     * @var array
     */
    protected $processed = [];

    /**
     * @var ValidationResultContract
     */
    protected $validationResult;


    public function __construct(RequestContract $request, ValidatorContract $validator, SanitizerContract $sanitizer)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->sanitizer = $sanitizer;
    }


    public function getValidationRules(): array
    {
        return [];
    }

    public function getSanitizerRules(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestParams(): ?array
    {
        return $this->getRequest()->getParams();
    }

    /**
     * @return Request
     */
    public function getRequest(): RequestContract
    {
        return $this->request;
    }

    public function getProcessed(): array
    {
        return $this->processed;
    }

    public function process(): ProcessedRequestContract
    {
        $this->processed = $this->sanitizer->sanitize($this->getRequestParams(), $this->getSanitizerRules());
        if (!$rules = $this->getValidationRules()) {
            $this->validationResult = new ValidationResult(new ValidationErrorsBag());
            return $this;
        }

        $this->validationResult = $this->getValidator()->validate($this->processed, $rules);

        return $this;
    }

    public function processOrThrow(): ProcessedRequestContract
    {
        if ($this->process()->passed()) {
            return $this;
        }

        $exception = new ValidationException();
        $exception->setValidationErrors($this->getErrorsBag());
        throw $exception;
    }

    public function passed(): bool
    {
        if (!$this->validationResult) {
            throw new \RuntimeException("Validation not executed. Run `validate()` first.");
        }
        return $this->validationResult->passed();
    }

    public function failed(): bool
    {
        if (!$this->validationResult) {
            throw new \RuntimeException("Validation not executed. Run `validate()` first.");
        }
        return $this->validationResult->failed();
    }

    public function getErrorsBag(): ValidationErrorsBagContract
    {
        if (!$this->validationResult) {
            throw new \RuntimeException("Validation not executed. Run `validate()` first.");
        }
        return $this->validationResult->getErrorsBag();
    }

    public function getValidator(): ValidatorContract
    {
        return $this->validator;
    }

    public function getSanitizer(): SanitizerContract
    {
        return $this->sanitizer;
    }
}