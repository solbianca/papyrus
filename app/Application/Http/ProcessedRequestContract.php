<?php


namespace Papyrus\Application\Http;

use Papyrus\Application\Validator\Contracts\ValidationErrorsBagContract;
use Psr\Http\Message\ServerRequestInterface as RequestContract;

interface ProcessedRequestContract
{
    public function getValidationRules(): array;

    public function getSanitizerRules(): array;

    /**
     * Fetch associative array of body and query string parameters.
     *
     * Note: This method is not part of the PSR-7 standard.
     */
    public function getRequestParams(): ?array;

    public function getRequest(): RequestContract;

    public function getProcessed(): array;

    public function process(): ProcessedRequestContract;

    public function processOrThrow(): ProcessedRequestContract;

    public function passed(): bool;

    public function failed(): bool;

    public function getErrorsBag(): ValidationErrorsBagContract;
}