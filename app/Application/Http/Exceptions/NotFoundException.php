<?php


namespace Papyrus\Application\Http\Exceptions;


use Slim\Exception\NotFoundException as Exception;

class NotFoundException extends Exception
{
    public function __construct()
    {
        parent::__construct(request(), response());
    }

}