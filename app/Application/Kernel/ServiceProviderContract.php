<?php

namespace Papyrus\Application\Kernel;

use DI\Container;

interface ServiceProviderContract
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $container
     * @return void
     */
    public function register(Container $container);
}