<?php


namespace Papyrus\Application\Kernel;



use Papyrus\Application\Kernel\DefaultProviders\CallableResolverProvider;
use Papyrus\Application\Kernel\DefaultProviders\ConfigProvider;
use Papyrus\Application\Kernel\DefaultProviders\HandlersProvider;
use Papyrus\Application\Kernel\DefaultProviders\HttpProvider;
use Papyrus\Application\Kernel\DefaultProviders\InvokersProvider;
use Papyrus\Application\Kernel\DefaultProviders\RouterProvider;
use Papyrus\Application\Kernel\DefaultProviders\SettingsProvider;

class Kernel
{
    /**
     * @var Kernel
     */
    private static $instance;

    /**
     * @var Application
     */
    private $application;

    private function __construct()
    {
    }

    public static function init(): Kernel
    {
        if (null === static::$instance) {
            static::$instance = new static();
            static::$instance->initApplication();
        }
        return static::$instance;
    }

    private function initApplication()
    {
        $this->application = new Application();
        $this->registerDefaultProviders();
    }

    private function initSession()
    {
//        session_start();
    }

    public function getApplication()
    {
        return $this->application;
    }

    public static function application()
    {
        if (!static::$instance->getApplication()) {
            throw new \RuntimeException("Application not instantiated");
        }
        return static::$instance->getApplication();
    }

    public function registerDefaultProviders()
    {
        $this->application->register(ConfigProvider::class);
        $this->application->register(RouterProvider::class);
        $this->application->register(HttpProvider::class);
        $this->application->register(HandlersProvider::class);
        $this->application->register(InvokersProvider::class);
        $this->application->register(CallableResolverProvider::class);
        $this->application->register(SettingsProvider::class);
    }

    public function handle()
    {
        $this->initSession();
        $this->application->run();
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}