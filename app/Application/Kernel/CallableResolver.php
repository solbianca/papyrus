<?php


namespace Papyrus\Application\Kernel;


use Psr\Container\ContainerInterface;
use Slim\Interfaces\CallableResolverInterface;

class CallableResolver implements CallableResolverInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    /**
     * {@inheritdoc}
     */
    public function resolve($toResolve)
    {
        return $this->resolveCallable($toResolve);
    }

    /**
     * @param string $class
     * @param string $method
     * @return callable
     *
     * @throws \RuntimeException if the callable does not exist
     */
    protected function resolveCallable($class, $method = 'handle')
    {
        if ($this->container->has($class)) {
            return [$this->container->get($class), $method];
        }

        if (!class_exists($class)) {
            throw new \RuntimeException(sprintf('Callable %s does not exist', $class));
        }

        return [new $class($this->container), $method];
    }
}