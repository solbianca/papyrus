<?php


namespace Papyrus\Application\Kernel\DefaultProviders;


use DI\Container;
use Papyrus\Application\Handlers\ErrorHandler;
use Papyrus\Application\Handlers\ErrorHandlerLogger;
use Papyrus\Application\Handlers\Responders\HtmlResponder;
use Papyrus\Application\Handlers\Responders\JsonApiResponder;
use Papyrus\Application\Handlers\Responders\JsonResponder;
use Papyrus\Application\Handlers\Responders\XmlResponder;
use Papyrus\Application\Kernel\ServiceProviderContract;
use Slim\Handlers\NotAllowed;
use Slim\Handlers\NotFound;

class HandlersProvider implements ServiceProviderContract
{
    public function register(Container $container)
    {
        $config = $container->get(\Papyrus\Application\Config\Config::class);

        $container->set('errorHandler', function () use ($config, $container) {
            $errorHandler = new ErrorHandler();
            $errorHandler->setDisplayErrorDetails($config->get('settings:displayErrorDetails'));
            $errorHandler->setLogger(new ErrorHandlerLogger($container->get('logger')));
            $errorHandler->addResponder('html', new HtmlResponder());
            $errorHandler->addResponder('json', new JsonResponder());
            $errorHandler->addResponder('xml', new XmlResponder());
            $errorHandler->addResponder('jsonapi', new JsonApiResponder());
            return $errorHandler;
        });

        $container->set('phpErrorHandler', function () use ($config, $container) {
            $errorHandler = new ErrorHandler();
            $errorHandler->setDisplayErrorDetails($config->get('settings:displayErrorDetails'));
            $errorHandler->setLogger(new ErrorHandlerLogger($container->get('logger')));
            $errorHandler->addResponder('html', new HtmlResponder());
            $errorHandler->addResponder('json', new JsonResponder());
            $errorHandler->addResponder('xml', new XmlResponder());
            return $errorHandler;
        });

        $container->set('notFoundHandler', function () {
            return new NotFound();
        });

        $container->set('notAllowedHandler', function () {
            return new NotAllowed();
        });
    }
}