<?php


namespace Papyrus\Application\Kernel\DefaultProviders;


use DI\Container;
use Papyrus\Application\Kernel\ServiceProviderContract;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Environment;
use Slim\Http\Headers;
use Slim\Http\Request;
use Slim\Http\Response;

class HttpProvider implements ServiceProviderContract
{
    public function register(Container $container)
    {
        $config = $container->get(\Papyrus\Application\Config\Config::class);

        $container->set('environment', function () {
            return new Environment($_SERVER);
        });

        $container->set(ServerRequestInterface::class, function () use ($container) {
            return Request::createFromEnvironment($container->get('environment'));
        });
        $container->set(Request::class, function () use ($container) {
            return $container->get(ServerRequestInterface::class);
        });
        $container->set('request', function () use ($container) {
            return $container->get(ServerRequestInterface::class);
        });

        $container->set(ResponseInterface::class, function () use ($container, $config) {
            $headers = new Headers(['Content-Type' => 'text/html; charset=UTF-8']);
            $response = new Response(200, $headers);
            return $response->withProtocolVersion($config->get('settings:httpVersion'));
        });
        $container->set(Response::class, function () use ($container) {
            return $container->get(ResponseInterface::class);
        });
        $container->set('response', function () use ($container) {
            return $container->get(ResponseInterface::class);
        });
    }

}