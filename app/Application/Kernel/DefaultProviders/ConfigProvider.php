<?php


namespace Papyrus\Application\Kernel\DefaultProviders;

use DI\Container;
use Papyrus\Application\Config\Config;
use Papyrus\Application\Kernel\ServiceProviderContract;

class ConfigProvider implements ServiceProviderContract
{
    public function register(Container $container)
    {
        $container->set(Config::class, function () {
            return new Config(ROOT . '/configs');
        });
    }
}