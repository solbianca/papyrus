<?php


namespace Papyrus\Application\Kernel\DefaultProviders;


use DI\Container;
use Invoker\Invoker;
use Invoker\ParameterResolver\AssociativeArrayResolver;
use Invoker\ParameterResolver\Container\TypeHintContainerResolver;
use Invoker\ParameterResolver\DefaultValueResolver;
use Invoker\ParameterResolver\ResolverChain;
use Papyrus\Application\Kernel\ControllerInvoker;
use Papyrus\Application\Kernel\ServiceProviderContract;

class InvokersProvider implements ServiceProviderContract
{
    public function register(Container $container)
    {
        $config = $container->get(\Papyrus\Application\Config\Config::class);

        $container->set('foundHandler.invoker', function () use ($config, $container) {
            $resolvers = [
                // Inject parameters by name first
                new AssociativeArrayResolver,
                // Then inject services by type-hints for those that weren't resolved
                new TypeHintContainerResolver($container),
                // Then fall back on parameters default values for optional route parameters
                new DefaultValueResolver(),
            ];
            return new Invoker(new ResolverChain($resolvers), $container);
        });

        $container->set('foundHandler', function () use ($container) {
            return new ControllerInvoker($container->get('foundHandler.invoker'));
        });
    }

}