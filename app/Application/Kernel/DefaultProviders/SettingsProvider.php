<?php


namespace Papyrus\Application\Kernel\DefaultProviders;


use DI\Container;
use Papyrus\Application\Kernel\ServiceProviderContract;

class SettingsProvider implements ServiceProviderContract
{
    public function register(Container $container)
    {
        $config = $container->get(\Papyrus\Application\Config\Config::class);

        $container->set('settings', function () use ($config) {
            return $config->get('settings');
        });
    }

}