<?php


namespace Papyrus\Application\Kernel\DefaultProviders;


use DI\Container;
use FastRoute\RouteParser\Std as StdParser;
use Papyrus\Application\Kernel\ServiceProviderContract;
use Slim\Router;

class RouterProvider implements ServiceProviderContract
{
    public function register(Container $container)
    {
        $config = $container->get(\Papyrus\Application\Config\Config::class);

        $container->set(Router::class, function () use ($container, $config) {
            $router = new Router(new StdParser());
            $router->setContainer($container);
            $router->setCacheFile($config->get('settings:routerCacheFile'));
            return $router;
        });

        $container->set('router', function () use ($container) {
            return $container->get(Router::class);
        });
    }

}