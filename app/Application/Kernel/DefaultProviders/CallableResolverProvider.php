<?php

namespace Papyrus\Application\Kernel\DefaultProviders;

use DI\Container;
use Papyrus\Application\Kernel\CallableResolver;
use Papyrus\Application\Kernel\ServiceProviderContract;

class CallableResolverProvider implements ServiceProviderContract
{
    public function register(Container $container)
    {
        $container->set('callableResolver', function () use ($container) {
            return $container->make(CallableResolver::class);
        });
    }

}