<?php


namespace Papyrus\Application\Kernel;

use DI\ContainerBuilder;
use Exception;
use Papyrus\Providers\ServiceProviderContract;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;
use Slim\MiddlewareAwareTrait;

class Application extends App
{
    use MiddlewareAwareTrait;

    /**
     * Papyrus constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $containerBuilder = new ContainerBuilder;
        $container = $containerBuilder->build();

        parent::__construct($container);
    }

    /**
     * @param string|ServiceProviderContract $providerClass
     */
    public function register($providerClass)
    {
        $provider = null;
        if (is_string($providerClass)) {
            if (!class_exists($providerClass)) {
                throw new \RuntimeException("Class `{$providerClass}` not exist");
            }
            $provider = $this->getContainer()->make($providerClass);
        } elseif ($provider instanceof ServiceProviderContract) {
            $provider = $providerClass;
        } else {
            $contract = ServiceProviderContract::class;
            throw new \RuntimeException("Provider must be a string or object and implement `{$contract}`");
        }

        $provider->register($this->getContainer());
    }

}