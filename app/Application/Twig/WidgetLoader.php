<?php


namespace Papyrus\Application\Twig;


use DI\Container;

class WidgetLoader
{
    /**
     * @var string
     */
    private $namespace = '';

    /**
     * @var string
     */
    private $postfix = '';

    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function setNamespace(string $namespace): void
    {
        $this->namespace = $namespace;
    }

    public function setPostfix(string $widgetsPath): void
    {
        $this->postfix = $widgetsPath;
    }

    public function load(string $classname): WidgetContract
    {
        $resolvedClassName = $this->resolveClassName($classname);

        if (!class_exists($resolvedClassName)) {
            throw new WidgetException("Class `{$resolvedClassName}` not exist");
        }

        if ($this->container->has($resolvedClassName)) {
            return $this->container->get($resolvedClassName);
        }

        return $this->container->make($resolvedClassName);
    }

    private function resolveClassName(string $classname): string
    {
        return $this->namespace . '\\' . $classname . $this->postfix;
    }
}