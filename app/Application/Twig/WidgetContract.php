<?php


namespace Papyrus\Application\Twig;


interface WidgetContract
{
    public function render(): string;
}