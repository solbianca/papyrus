<?php


namespace Papyrus\Application\Helpers;


class DateTimeFormatter
{
    const DEFAULT_TIME_ZONE = 'UTC';

    /**
     * @var \DateTime
     */
    private $datetime;

    public function __construct(string $datetime, string $timezone = '')
    {
        if (config('app:timezone')) {
            $timezone = config('app:timezone');
        }

        if (!$timezone || !$this->isValidTimezone($timezone)) {
            $timezone = static::DEFAULT_TIME_ZONE;
        }

        $this->datetime = new \DateTime($datetime, new \DateTimeZone($timezone));
    }

    function isValidTimezone(string $timezone): bool
    {
        return in_array($timezone, \DateTimeZone::listIdentifiers());
    }

    public static function from(string $datetime): DateTimeFormatter
    {
        return new DateTimeFormatter($datetime);
    }

    public function toW3C(): string
    {
        return $this->datetime->format(\DateTime::W3C);
    }
}