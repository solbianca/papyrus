<?php


namespace Papyrus\Application\Handlers;


use Papyrus\Application\Handlers\Responders\ResponderContract;
use Papyrus\Application\Validator\ValidationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use UnexpectedValueException;

class ErrorHandler
{
    /**
     * @var ErrorHandlerLogger
     */
    private $logger;

    /**
     * @var ResponderContract[]
     */
    private $responders;

    /**
     * Known handled content types
     *
     * @var array
     */
    private $knownContentTypes = [
        'application/json',
        'application/xml',
        'text/xml',
        'text/html',
        'application/vnd.api+json',
    ];

    /**
     * @var bool
     */
    private $displayErrorDetails = false;

    public function setLogger(ErrorHandlerLogger $logger): ErrorHandler
    {
        $this->logger = $logger;
        return $this;
    }

    public function addResponder(string $key, ResponderContract $responder): ErrorHandler
    {
        $this->responders[$key] = $responder;
        return $this;
    }

    /**
     * @param bool $displayErrorDetails
     */
    public function setDisplayErrorDetails(bool $displayErrorDetails): void
    {
        $this->displayErrorDetails = $displayErrorDetails;
    }

    /**
     * Invoke error handler
     *
     * @param ServerRequestInterface $request The most recent ProcessedRequest object
     * @param ResponseInterface $response The most recent Response object
     * @param \Throwable $error The caught Throwable object
     *
     * @return ResponseInterface
     * @throws UnexpectedValueException
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        \Throwable $error
    ): ResponseInterface {
        $this->logger->log($error);

        $contentType = $this->determineContentType($request);

        if (($error instanceof ValidationException) && $contentType === 'application/vnd.api+json') {
            return $this->responders['jsonapi']
                ->setDisplayErrorDetails($this->displayErrorDetails)
                ->respond($request, $response, $error, $contentType);
        }

        switch ($contentType) {
            case 'application/json':
            case 'application/vnd.api+json':
                return $this->responders['json']
                    ->setDisplayErrorDetails($this->displayErrorDetails)
                    ->respond($request, $response, $error, $contentType);
                break;
            case 'text/xml':
            case 'application/xml':
                return $this->responders['xml']
                    ->setDisplayErrorDetails($this->displayErrorDetails)
                    ->respond($request, $response, $error, $contentType);
                break;
            case 'text/html':
                return $this->responders['html']
                    ->setDisplayErrorDetails($this->displayErrorDetails)
                    ->respond($request, $response, $error, $contentType);
                break;
            default:
                throw new UnexpectedValueException('Cannot render unknown content type ' . $contentType);
        }
    }

    /**
     * Determine which content type we know about is wanted using Accept header
     *
     * Note: This method is a bare-bones implementation designed specifically for
     * Slim's error handling requirements. Consider a fully-feature solution such
     * as willdurand/negotiation for any other situation.
     *
     * @param ServerRequestInterface $request
     * @return string
     */
    protected function determineContentType(ServerRequestInterface $request)
    {
        $acceptHeader = $request->getHeaderLine('Accept');
        $selectedContentTypes = array_intersect(explode(',', $acceptHeader), $this->knownContentTypes);

        if (count($selectedContentTypes)) {
            return current($selectedContentTypes);
        }

        // handle +json and +xml specially
        if (preg_match('/\+(json|xml)/', $acceptHeader, $matches)) {
            $mediaType = 'application/' . $matches[1];
            if (in_array($mediaType, $this->knownContentTypes)) {
                return $mediaType;
            }
        }

        return 'text/html';
    }
}