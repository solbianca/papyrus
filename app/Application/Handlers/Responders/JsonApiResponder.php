<?php


namespace Papyrus\Application\Handlers\Responders;


use Papyrus\Application\Validator\Contracts\ValidationErrorsBagContract;
use Papyrus\Application\Validator\ErrorMessage;
use Papyrus\Application\Validator\ValidationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Body;

class JsonApiResponder extends Responder implements ResponderContract
{
    public function respond(
        ServerRequestInterface $request,
        ResponseInterface $response,
        \Throwable $error,
        string $contentType
    ): ResponseInterface {
        $body = new Body(fopen('php://temp', 'r+'));
        $body->write($this->output($error));

        return $response
            ->withStatus(422)
            ->withHeader('Content-Type', $contentType)
            ->withHeader('Accept', $contentType)
            ->withBody($body);
    }


    protected function output(\Throwable $error): string
    {
        if (!($error instanceof ValidationException)) {
            throw new \RuntimeException("Error must be instance of `Papyrus\Application\Validator\ValidationException`");
        }

        /* @var \Papyrus\Application\Validator\ValidationException $error */
        if ($error->getValidationErrors() === null) {
            return $this->encode([]);
        }

        return $this->encode($this->serializeErrors($error->getValidationErrors()));
    }

    private function encode(array $errors): string
    {
        return json_encode($errors, JSON_UNESCAPED_SLASHES);
    }

    private function serializeErrors(ValidationErrorsBagContract $errorsBag): array
    {
        if ($errorsBag->isEmpty()) {
            return [];
        }

        $errors = [];
        foreach ($errorsBag->all() as $errorMessage) {
            $errors[] = $this->serializeError($errorMessage);
        }
        return $errors;
    }

    private function serializeError(ErrorMessage $errorMessage)
    {
        return [
            'code' => $errorMessage->getCode(),
            'title' => $errorMessage->getMessage(),
            'source' => [
                'pointer' => $errorMessage->getPropertyPath(),
            ],
        ];
    }
}