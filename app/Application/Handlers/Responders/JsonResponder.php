<?php


namespace Papyrus\Application\Handlers\Responders;


class JsonResponder extends Responder implements ResponderContract
{
    protected function output(\Throwable $error): string
    {
        return $this->renderJsonErrorMessage($error);
    }

    /**
     * Render JSON error
     *
     * @param \Throwable $error
     *
     * @return string
     */
    private function renderJsonErrorMessage(\Throwable $error)
    {
        $json = [
            'message' => 'Slim Application Error',
        ];

        if ($this->displayErrorDetails) {
            $json['error'] = [];

            do {
                $json['error'][] = [
                    'type' => get_class($error),
                    'code' => $error->getCode(),
                    'message' => $error->getMessage(),
                    'file' => $error->getFile(),
                    'line' => $error->getLine(),
                    'trace' => explode("\n", $error->getTraceAsString()),
                ];
            } while ($error = $error->getPrevious());
        }

        return json_encode($json, JSON_PRETTY_PRINT);
    }
}