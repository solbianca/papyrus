<?php


namespace Papyrus\Application\Handlers\Responders;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface ResponderContract
{
    public function respond(
        ServerRequestInterface $request,
        ResponseInterface $response,
        \Throwable $error,
        string $contentType
    ): ResponseInterface;

    public function setDisplayErrorDetails(bool $displayErrorDetails): ResponderContract;
}