<?php


namespace Papyrus\Application\Handlers\Responders;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Body;

abstract class Responder implements ResponderContract
{
    /**
     * @var bool
     */
    protected $displayErrorDetails = false;

    public function setDisplayErrorDetails(bool $displayErrorDetails): ResponderContract
    {
        $this->displayErrorDetails = $displayErrorDetails;
        return $this;
    }

    public function respond(
        ServerRequestInterface $request,
        ResponseInterface $response,
        \Throwable $error,
        string $contentType
    ): ResponseInterface {
        $body = new Body(fopen('php://temp', 'r+'));
        $body->write($this->output($error));

        return $response
            ->withStatus(500)
            ->withHeader('Content-type', $contentType)
            ->withBody($body);
    }

    abstract protected function output(\Throwable $error): string;
}