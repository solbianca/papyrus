<?php


namespace Papyrus\Commands\Atlas;


use Papyrus\Application\Console\Command;
use Symfony\Component\Process\Process;

class GenerateCommand extends Command
{
    protected static $defaultName = 'atlas:generate';

    protected $description = "Сгенерировать сущности на основе базы данных";

    public function handle()
    {
        $process = new Process(["php", "vendor/bin/atlas-skeleton.php", "configs/database.php", "papyrus"]);
        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > ' . $buffer;
            } else {
                echo 'OUT > ' . $buffer;
            }
        });
    }
}