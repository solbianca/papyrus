<?php

namespace Papyrus\Commands\Tests;

use Atlas\Pdo\Connection;
use Papyrus\Application\Console\Command;

class DbInitCommand extends Command
{
    protected static $defaultName = 'tests:db-init';

    protected $description = "Подготовить тестовую базу данных для тестов";

    public function handle()
    {
        $connection = Connection::new(
            'mysql:host=mysql-test;',
            'root',
            'test'
        );

        $this->info("Создаем тестовую базу данных `papyrus_test`");
        $connection->exec("CREATE DATABASE papyrus_test CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;");


        $this->info('База данных создана');
    }
}