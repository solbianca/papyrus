<?php


namespace Papyrus\Commands\Phinx;


use Papyrus\Application\Console\Command;
use Symfony\Component\Process\Process;

class MigrateCommand extends Command
{
    protected static $defaultName = 'phinx:migrate';

    protected $description = "Применить миграции";

    public function handle()
    {
        $process = new Process(["php", "vendor/bin/phinx", "migrate"]);
        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > ' . $buffer;
            } else {
                echo 'OUT > ' . $buffer;
            }
        });
    }
}