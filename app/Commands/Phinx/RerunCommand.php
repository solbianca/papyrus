<?php


namespace Papyrus\Commands\Phinx;


use Papyrus\Application\Console\Command;
use Symfony\Component\Console\Input\ArrayInput;

class RerunCommand extends Command
{
    protected static $defaultName = 'phinx:rerun';

    protected $description = "Откатить и применить миграции и применить сиды";

    public function handle()
    {
        $this->info("Откатить миграции");
        $command = $this->getApplication()->find('phinx:rollback');
        $arguments = ['command' => 'phinx:rollback'];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $this->getOutput());
        $this->info("Миграции  откатили");

        echo "\n";

        $this->info("Применить миграции");
        $command = $this->getApplication()->find('phinx:migrate');
        $arguments = ['command' => 'phinx:migrate'];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $this->getOutput());
        $this->info("Миграции применили");

        echo "\n";

        $this->info("Применить сиды");
        $command = $this->getApplication()->find('seed:run');
        $arguments = ['command' => 'seed:run'];
        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $this->getOutput());
        $this->info("Сиды применили");
    }
}