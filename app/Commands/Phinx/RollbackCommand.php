<?php

namespace Papyrus\Commands\Phinx;

use Papyrus\Application\Console\Command;
use Symfony\Component\Process\Process;

class RollbackCommand extends Command
{
    protected static $defaultName = 'phinx:rollback';

    protected $description = "Откатить миграции";

    public function handle()
    {
        $process = new Process(["php", "vendor/bin/phinx", "rollback"]);
        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > ' . $buffer;
            } else {
                echo 'OUT > ' . $buffer;
            }
        });
    }
}