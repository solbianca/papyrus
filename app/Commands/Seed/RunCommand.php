<?php


namespace Papyrus\Commands\Seed;


use Papyrus\Application\Console\Command;
use Symfony\Component\Process\Process;

class RunCommand extends Command
{
    protected static $defaultName = 'seed:run';

    protected $description = "Запустить сидеры";

    public function handle()
    {
        $process = new Process(["php", "vendor/bin/phinx", "seed:run"]);
        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > ' . $buffer;
            } else {
                echo 'OUT > ' . $buffer;
            }
        });
    }
}