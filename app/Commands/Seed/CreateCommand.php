<?php


namespace Papyrus\Commands\Seed;


use Papyrus\Application\Console\Command;
use Symfony\Component\Process\Process;

class CreateCommand extends Command
{
    protected static $defaultName = 'seed:create';

    protected $description = "Создать сидер";

    public function handle()
    {
        $process = new Process(["php", "vendor/bin/phinx", "seed:create"]);
        $process->run(function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > ' . $buffer;
            } else {
                echo 'OUT > ' . $buffer;
            }
        });
    }
}