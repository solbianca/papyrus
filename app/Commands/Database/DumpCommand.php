<?php

namespace Papyrus\Commands\Database;

use Papyrus\Application\Console\Command;
use Symfony\Component\Process\Process;

class DumpCommand extends Command
{
    protected static $defaultName = 'db:dump';

    protected $description = "Создать дамп структуры базы данных";

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $database;

    /**
     * @var string
     */
    private $filepath;

    public function __construct()
    {
        parent::__construct();
        $this->user = env('DB_USER');
        $this->password = env('DB_PASSWORD');
        $this->database = env('DB_NAME');
        $this->filepath = tests() . '/_data/dump.sql';
    }

    public function handle()
    {
        $user = '-u' . $this->user;
        $password = '-p' . $this->password;
        $database = $this->database;

        $this->deleteFile();

        $process = new Process(["mysqldump", "-d", $user, $password, $database]);
        $process->run(function ($type, $buffer) use ($process) {
            if (Process::ERR === $type) {
                echo 'ERR > ' . $buffer;
            } else {
                $this->saveInFile($buffer);
            }
        });
        $this->info('Database scheme created');
    }

    private function deleteFile()
    {
        unlink($this->filepath);
    }

    private function saveInFile(string $scheme)
    {
        return file_put_contents($this->filepath, $scheme, FILE_APPEND);
    }
}