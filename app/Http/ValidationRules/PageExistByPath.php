<?php


namespace Papyrus\Http\ValidationRules;


use Symfony\Component\Validator\Constraint;

class PageExistByPath extends Constraint
{
    public $message = 'Страница "{{ path }}" не существует';
}