<?php


namespace Papyrus\Http\ValidationRules;


use Papyrus\Application\Repositories\RepositoryFactory;
use Papyrus\Domain\Repositories\PagesRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Symfony\Component\Validator\Tests\Fixtures\ConstraintAValidator;

class PageExistByPathValidator extends ConstraintAValidator
{
    /**
     * @var PagesRepository
     */
    private $pagesRepository;

    public function __construct()
    {
        $this->pagesRepository = RepositoryFactory::create(PagesRepository::class);
    }

    public function validate($value, Constraint $constraint)
    {
        if ($this->pagesRepository->isExistByPath($value)) {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }

        if ('' === $value) {
            return;
        }
        
        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ path }}', $value)
            ->addViolation();
    }
}