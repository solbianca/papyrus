<?php


namespace Papyrus\Http\Actions;


use Papyrus\Http\Responders\TwigResponder;

class TestPage
{
    /**
     * @var TwigResponder
     */
    private $responder;

    public function __construct(TwigResponder $responder)
    {
        $this->responder = $responder;
    }

    public function handle()
    {
        $this->responder->respond('pages/test');
    }
}