<?php


namespace Papyrus\Http\Actions\Pages;


use Papyrus\Application\Http\Exceptions\NotFoundException;
use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Domain\Repositories\PagesRepository;
use Papyrus\Http\Responders\TwigResponder;

class ShowPageAction
{
    /**
     * @var TwigResponder
     */
    private $twigResponder;

    /**
     * @var PagesRepository
     */
    private $pagesRepository;

    /**
     * @var ArticlesRepository
     */
    private $articlesRepository;

    public function __construct(
        TwigResponder $twigResponder,
        PagesRepository $pagesRepository,
        ArticlesRepository $articlesRepository
    ) {
        $this->twigResponder = $twigResponder;
        $this->pagesRepository = $pagesRepository;
        $this->articlesRepository = $articlesRepository;
    }

    public function handle(int $id)
    {
        $pageRecord = $this->pagesRepository->findById($id);

        if (!$pageRecord) {
            throw new NotFoundException();
        }

        $articleRecordsSet = $this->articlesRepository->findForAdminListing($id);

        return $this->twigResponder->respond('admin/pages/show', [
            'page' => $pageRecord,
            'articles' => $articleRecordsSet,
        ]);
    }
}