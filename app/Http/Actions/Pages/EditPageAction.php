<?php

namespace Papyrus\Http\Actions\Pages;


use Papyrus\Application\Writer\Writer;
use Papyrus\Domain\Repositories\PagesRepository;
use Papyrus\Http\Requests\Pages\EditPageRequest;
use Papyrus\Http\Responders\TwigResponder;
use Papyrus\Persistent\Page\PageRecord;
use Slim\Exception\NotFoundException;

class EditPageAction
{

    /**
     * @var TwigResponder
     */
    private $twigResponder;

    /**
     * @var PagesRepository
     */
    private $pagesRepository;

    /**
     * @var Writer
     */
    private $writer;

    public function __construct(TwigResponder $twigResponder, PagesRepository $pagesRepository, Writer $writer)
    {
        $this->twigResponder = $twigResponder;
        $this->pagesRepository = $pagesRepository;
        $this->writer = $writer;
    }

    public function handle(int $id, EditPageRequest $editPageRequest)
    {
        $pageRecord = $this->pagesRepository->findById($id);

        if (!$pageRecord) {
            throw new NotFoundException(request(), response());
        }

        if ($editPageRequest->getRequest()->getMethod() === 'GET') {
            return $this->twigResponder->respond('admin/pages/edit', ['page' => $pageRecord]);
        }

        $processed = $editPageRequest->process()->getProcessed();
        $pageRecord = $this->updatePage($pageRecord, $processed);

        return $this->twigResponder->respond('admin/pages/edit', ['page' => $pageRecord]);
    }

    private function updatePage(PageRecord $pageRecord, array $pageData): PageRecord
    {
        $pageRecord->title = $pageData['page']['title'];
        $this->writer->update($pageRecord);

        return $pageRecord;
    }
}