<?php


namespace Papyrus\Http\Actions\Articles;


use Papyrus\Application\Http\Exceptions\NotFoundException;
use Papyrus\Application\Hydrator\Hydrator;
use Papyrus\Domain\Articles\ArticlesCreator;
use Papyrus\Domain\Repositories\PagesRepository;
use Papyrus\Http\Requests\Articles\CreateArticleRequest;
use Papyrus\Http\RequestsValues\Articles\ArticleRequestValue;
use Papyrus\Http\Responders\TwigResponder;
use Papyrus\Persistent\Article\ArticleRecord;
use Psr\Http\Message\ResponseInterface;

class CreateArticleAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    /**
     * @var ArticlesCreator
     */
    private $creator;

    /**
     * @var PagesRepository
     */
    private $pageRepository;

    public function __construct(TwigResponder $responder, ArticlesCreator $creator, PagesRepository $pagesRepository)
    {
        $this->responder = $responder;
        $this->creator = $creator;
        $this->pageRepository = $pagesRepository;
    }

    public function handle(int $pageId, CreateArticleRequest $request)
    {
        $pageRecord = $this->pageRepository->findById($pageId);

        if (!$pageRecord) {
            throw new NotFoundException();
        }

        if ($request->getRequest()->isGet()) {
            return $this->responder->respond('admin/articles/create', [
                'article' => $this->makeArticleData(),
                'page' => $pageRecord,
            ]);
        }

        if ($request->process()->failed()) {
            return $this->respondValidationErrors($request);
        }
        $articleRecord = $this->publish($request);

        return redirect('/articles/' . $articleRecord->id);
    }

    private function makeArticleData()
    {
        return [
            'author' => 'Sol Bianca',
        ];
    }

    private function publish(CreateArticleRequest $request): ArticleRecord
    {
        $articleRequestValue = $this->createArticleRequestValue($request);
        $article = $this->creator->create($articleRequestValue);
        return $article;
    }

    private function createArticleRequestValue(CreateArticleRequest $request): ArticleRequestValue
    {
        $processedArticle = $request->getProcessed()['article'];
        return Hydrator::factory()->hydrate(new ArticleRequestValue(), $processedArticle);
    }

    private function respondValidationErrors(CreateArticleRequest $request): ResponseInterface
    {
        return $this->responder->respond('admin/articles/create', [
            'article' => $request->getRequestParams()['article'],
            'errorsBag' => $request->getErrorsBag(),
        ]);
    }
}