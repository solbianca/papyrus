<?php


namespace Papyrus\Http\Actions\Articles;


use Papyrus\Application\Http\Exceptions\NotFoundException;
use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Domain\Repositories\PagesRepository;
use Papyrus\Http\Responders\TwigResponder;
use Papyrus\Persistent\Article\ArticleRecordSet;

class HistoryArticleAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    /**
     * @var ArticlesRepository
     */
    private $articleRepository;

    /**
     * @var PagesRepository
     */
    private $pageRepository;

    public function __construct(
        TwigResponder $responder,
        ArticlesRepository $articlesRepository,
        PagesRepository $pagesRepository
    ) {
        $this->responder = $responder;
        $this->articleRepository = $articlesRepository;
        $this->pageRepository = $pagesRepository;
    }

    public function handle(int $pageId, string $articlePid)
    {
        if (!$pageRecord = $this->pageRepository->findById($pageId)) {
            throw new NotFoundException();
        }

        if (!$articleRecordsSet = $this->fetchAllByPid($articlePid)) {
            throw new NotFoundException();
        }

        return $this->responder->respond('admin/articles/history', [
            'page' => $pageRecord,
            'articles' => $articleRecordsSet,
        ]);
    }

    private function fetchAllByPid(string $pid): ArticleRecordSet
    {
        return $this->articleRepository->findAllPublishedAndActiveByPid($pid);
    }
}