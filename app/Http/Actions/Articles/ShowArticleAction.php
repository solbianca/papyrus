<?php


namespace Papyrus\Http\Actions\Articles;


use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Http\Responders\TwigResponder;

class ShowArticleAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    /**
     * @var ArticlesRepository
     */
    private $repository;

    public function __construct(TwigResponder $responder, ArticlesRepository $repository)
    {
        $this->responder = $responder;
        $this->repository = $repository;
    }

    public function handle(int $pageId, string $articlePid)
    {
        $articleRecord = $this->repository->findLastByPid($articlePid);
        return $this->responder->respond('admin/articles/show', ['article' => $articleRecord]);
    }
}