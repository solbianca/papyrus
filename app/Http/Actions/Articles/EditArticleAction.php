<?php

namespace Papyrus\Http\Actions\Articles;


use Papyrus\Application\Http\Exceptions\NotFoundException;
use Papyrus\Application\Hydrator\Hydrator;
use Papyrus\Domain\Articles\ArticlesUpdater;
use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Http\Requests\Articles\EditArticleRequest;
use Papyrus\Http\RequestsValues\Articles\ArticleRequestValue;
use Papyrus\Http\Responders\TwigResponder;
use Papyrus\Persistent\Article\ArticleRecord;
use Psr\Http\Message\ResponseInterface;

class EditArticleAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    /**
     * @var ArticlesUpdater
     */
    private $updater;

    private $articleRepository;

    public function __construct(TwigResponder $responder, ArticlesUpdater $updater, ArticlesRepository $articlesRepository)
    {
        $this->responder = $responder;
        $this->updater = $updater;
        $this->articleRepository = $articlesRepository;
    }

    public function handle(string $pageId, string $articlePid, EditArticleRequest $request)
    {
        $articleRecord = $this->articleRepository->findLastByPid($articlePid, ['page']);

        if (!$articleRecord) {
            throw new NotFoundException();
        }

        if ($request->getRequest()->isGet()) {
            return $this->responder->respond('admin/articles/edit', [
                'article' => $articleRecord,
            ]);
        }

        if ($request->process()->failed()) {
            return $this->respondValidationErrors($articlePid, $request);
        }

        $articleRecord = $this->update($articlePid, $request);

        return redirect("/admin/pages/{$pageId}/articles/edit/{$articlePid}");
    }

    private function update(string $articlePid, EditArticleRequest $request): ArticleRecord
    {
        $articleRequestValue = $this->createArticleRequestValue($articlePid, $request);
        return $this->updater->update($articleRequestValue);
    }

    private function createArticleRequestValue(string $articlePid, EditArticleRequest $request): ArticleRequestValue
    {
        $processedArticle = $request->getProcessed()['article'];
        $processedArticle['pid'] = $articlePid;
        return Hydrator::factory()->hydrate(new ArticleRequestValue(), $processedArticle);
    }

    private function respondValidationErrors(string $articlePid, EditArticleRequest $request): ResponseInterface
    {
        return $this->responder->respond('admin/articles/edit', [
            'article' => $request->getRequestParams()['article'],
            'errorsBag' => $request->getErrorsBag(),
        ]);
    }
}