<?php


namespace Papyrus\Http\Actions\Articles;


use Papyrus\Application\Http\Exceptions\NotFoundException;
use Papyrus\Domain\Articles\Difference\Difference;
use Papyrus\Domain\Repositories\PagesRepository;
use Papyrus\Http\Requests\Articles\CompareArticlesProcessedRequest;
use Papyrus\Http\Responders\TwigResponder;

class CompareArticlesAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    /**
     * @var PagesRepository
     */
    private $pageRepository;

    /**
     * @var Difference
     */
    private $difference;

    public function __construct(TwigResponder $responder, PagesRepository $pagesRepository, Difference $difference)
    {
        $this->responder = $responder;
        $this->pageRepository = $pagesRepository;
        $this->difference = $difference;
    }

    public function handle(int $pageId, CompareArticlesProcessedRequest $request)
    {
        if (!$pageRecord = $this->pageRepository->findById($pageId)) {
            throw new NotFoundException();
        }

        $processed = $request->process()->getProcessed();
        $difference = $this->difference->handle($processed['articles'][1], $processed['articles'][0]);

        return $this->responder->respond('admin/articles/compare', ['page' => $pageRecord, 'diff' => $difference]);
    }
}