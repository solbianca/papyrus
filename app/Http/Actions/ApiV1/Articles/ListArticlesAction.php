<?php


namespace Papyrus\Http\Actions\ApiV1\Articles;


use Papyrus\Domain\Articles\ListingFetcher;
use Papyrus\Domain\Repositories\PagesRepository;
use Papyrus\Http\Requests\Articles\ListArticlesRequest;
use Papyrus\Http\Responders\JsonApiResponder;
use Papyrus\Http\Serializers\ArticleSerializer;

class ListArticlesAction
{
    /**
     * @var JsonApiResponder
     */
    private $responder;

    /**
     * @var ListingFetcher
     */
    private $listingFetcher;

    /**
     * @var PagesRepository
     */
    private $pagesRepository;

    public function __construct(
        JsonApiResponder $responder,
        ListingFetcher $listingFetcher,
        PagesRepository $pagesRepository
    ) {
        $this->responder = $responder;
        $this->listingFetcher = $listingFetcher;
        $this->pagesRepository = $pagesRepository;
    }

    public function handle(ListArticlesRequest $request)
    {
        $processed = $request->processOrThrow()->getProcessed();

        if (!$pageId = $this->getPageIdByPath($processed['page'])) {
            throw new \RuntimeException('Page not founded');
        }

        $articlesRecordsSet = $this->listingFetcher->fetch($pageId, $processed['author']);

        return $this->responder
            ->setSerializer(new ArticleSerializer())
            ->collection($articlesRecordsSet, ['draft'])
            ->respond();
    }

    private function getPageIdByPath(string $pagesSet): ?int
    {
        /* @var \Papyrus\Persistent\Page\PageRecord */
        $pagesSet = $this->pagesRepository->findBy(['path =', $pagesSet]);
        
        if ($pagesSet->count() !== 1) {
            return null;
        }

        return $pagesSet[0]->id;
    }
}