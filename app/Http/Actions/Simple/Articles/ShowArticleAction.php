<?php


namespace Papyrus\Http\Actions\Simple\Articles;

use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Http\Responders\TwigResponder;

class ShowArticleAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    /**
     * @var ArticlesRepository
     */
    private $repository;

    public function __construct(TwigResponder $responder, ArticlesRepository $repository)
    {
        $this->responder = $responder;
        $this->repository = $repository;
    }

    public function handle(int $id)
    {
        $articleRecord = $this->repository->findById($id);
        return $this->responder->respond('articles/show', ['article' => $articleRecord]);
    }
}