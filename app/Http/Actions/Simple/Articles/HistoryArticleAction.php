<?php


namespace Papyrus\Http\Actions\Simple\Articles;

use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Http\Responders\TwigResponder;
use Papyrus\Persistent\Article\ArticleRecordSet;
use Slim\Exception\NotFoundException;

class HistoryArticleAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    /**
     * @var ArticlesRepository
     */
    private $articleRepository;

    public function __construct(TwigResponder $responder, ArticlesRepository $articlesRepository)
    {
        $this->responder = $responder;
        $this->articleRepository = $articlesRepository;
    }

    public function handle(string $pid)
    {
        if (!$articlesRecordsSet = $this->fetchAllByPid($pid)) {
            throw new NotFoundException();
        }

        return $this->responder->respond('articles/history', ['articles' => $articlesRecordsSet]);
    }

    private function fetchAllByPid(string $pid): ArticleRecordSet
    {
        return $this->articleRepository->findAllPublishedAndActiveByPid($pid);
    }
}