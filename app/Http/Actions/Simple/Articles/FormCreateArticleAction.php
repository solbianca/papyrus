<?php


namespace Papyrus\Http\Actions\Simple\Articles;

use Papyrus\Application\Writer\WriterContract;
use Papyrus\Domain\Repositories\PagesRepository;
use Papyrus\Http\Requests\Articles\FormCreateArticleRequest;
use Papyrus\Http\Responders\TwigResponder;
use Papyrus\Persistent\Page\Page;
use Papyrus\Persistent\Page\PageRecord;
use Psr\Http\Message\ResponseInterface;

class FormCreateArticleAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    /**
     * @var PagesRepository
     */
    private $pageRepository;

    /**
     * @var WriterContract
     */
    private $writer;

    public function __construct(
        TwigResponder $responder,
        PagesRepository $pageRepository,
        WriterContract $writer
    ) {
        $this->responder = $responder;
        $this->pageRepository = $pageRepository;
        $this->writer = $writer;
    }

    public function handle(FormCreateArticleRequest $request)
    {
        if ($request->process()->failed()) {
            return $this->respondValidationErrors($request);
        }

        $processed = $request->getProcessed();

        $page = $this->findPageByPath($processed['path']);

        return $this->responder->respond('articles/create', [
            'article' => ['author' => $processed['author']],
            'page' => $page,
        ]);
    }

    private function findPageByPath(string $path): PageRecord
    {
        $recordsSet = $this->pageRepository->findBy(['path =', $path]);
        if ($recordsSet->isEmpty()) {
            return $this->createPage($path);
        }

        if ($recordsSet->count() != 1) {
            throw new \RuntimeException("Invalid page");
        }
        return $recordsSet[0];
    }

    private function createPage(string $path): PageRecord
    {
        return $this->writer->create(Page::class, ['title' => '', 'path' => $path]);
    }

    private function respondValidationErrors(FormCreateArticleRequest $request): ResponseInterface
    {
        return $this->responder->respond('articles/create', [
            'errorsBag' => $request->getErrorsBag(),
        ]);
    }
}