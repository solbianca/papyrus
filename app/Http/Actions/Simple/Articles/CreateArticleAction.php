<?php


namespace Papyrus\Http\Actions\Simple\Articles;

use Papyrus\Domain\Articles\ArticlesCreator;
use Papyrus\Http\Requests\Articles\CreateArticleRequest;
use Papyrus\Http\Responders\TwigResponder;
use Papyrus\Persistent\Article\ArticleRecord;
use Psr\Http\Message\ResponseInterface;

class CreateArticleAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    /**
     * @var ArticlesCreator
     */
    private $creator;

    public function __construct(TwigResponder $responder, ArticlesCreator $publisher)
    {
        $this->responder = $responder;
        $this->creator = $publisher;
    }

    public function handle(CreateArticleRequest $request)
    {
        if ($request->process()->failed()) {
            return $this->respondValidationErrors($request);
        }

        $articleRecord = $this->publish($request);

        return redirect('/articles/' . $articleRecord->id);
    }

    private function publish(CreateArticleRequest $request): ArticleRecord
    {
        $article = $this->creator->create($request->getRequestValue());
        return $article;
    }

    private function respondValidationErrors(CreateArticleRequest $request): ResponseInterface
    {
        return $this->responder->respond('articles/create', [
            'article' => $request->getRequestParams()['article'],
            'errorsBag' => $request->getErrorsBag(),
        ]);
    }
}