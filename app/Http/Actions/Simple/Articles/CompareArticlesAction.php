<?php


namespace Papyrus\Http\Actions\Simple\Articles;


use Papyrus\Domain\Articles\Difference\Difference;
use Papyrus\Http\Requests\Articles\CompareArticlesProcessedRequest;
use Papyrus\Http\Responders\TwigResponder;

class CompareArticlesAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    /**
     * @var Difference
     */
    private $difference;

    public function __construct(TwigResponder $responder, Difference $difference)
    {
        $this->responder = $responder;
        $this->difference = $difference;
    }

    public function handle(CompareArticlesProcessedRequest $request)
    {
        $processed = $request->process()->getProcessed();
        $difference = $this->difference->handle($processed['articles'][1], $processed['articles'][0]);

        return $this->responder->respond('articles/compare', ['diff' => $difference]);
    }
}