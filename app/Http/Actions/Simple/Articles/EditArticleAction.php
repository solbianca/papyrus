<?php

namespace Papyrus\Http\Actions\Simple\Articles;

use Papyrus\Domain\Articles\ArticlesUpdater;
use Papyrus\Http\Requests\Articles\EditArticleRequest;
use Papyrus\Http\Responders\TwigResponder;
use Papyrus\Persistent\Article\ArticleRecord;
use Psr\Http\Message\ResponseInterface;

class EditArticleAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    private $updater;

    public function __construct(TwigResponder $responder, ArticlesUpdater $publisher)
    {
        $this->responder = $responder;
        $this->updater = $publisher;
    }

    public function handle(string $pid, EditArticleRequest $request)
    {
        if ($request->process()->failed()) {
            return $this->respondValidationErrors($request);
        }

        $articleRecord = $this->publish($request);

        return $this->responder->respond('articles/edit', ['article' => $articleRecord]);
    }

    private function publish(EditArticleRequest $request): ArticleRecord
    {
        return $this->updater->update($request->getRequestValue());
    }

    private function respondValidationErrors(EditArticleRequest $request): ResponseInterface
    {
        return $this->responder->respond('articles/edit', [
            'article' => $request->getRequestParams()['article'],
            'errorsBag' => $request->getErrorsBag(),
        ]);
    }
}