<?php


namespace Papyrus\Http\Actions\Simple\Articles;

use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Http\Requests\Articles\FormEditArticleRequest;
use Papyrus\Http\Responders\TwigResponder;
use Papyrus\Persistent\Article\ArticleRecord;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\NotFoundException;

class FormEditArticleAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    /**
     * @var ArticlesRepository
     */
    private $articleRepository;

    public function __construct(TwigResponder $responder, ArticlesRepository $articlesRepository)
    {
        $this->responder = $responder;
        $this->articleRepository = $articlesRepository;
    }

    public function handle(string $pid, FormEditArticleRequest $request)
    {
        if ($request->process()->failed()) {
            return $this->respondValidationErrors($request);
        }

        $article = $this->fetchLastByPid($pid);
        if (!$article) {
            throw new NotFoundException($request->getRequest(), response());
        }
        $article->author = $request->getProcessed()['author'];

        return $this->responder->respond('articles/edit', [
            'article' => $article->getArrayCopy(),
            'page' => $article->page,
            'host' => $article->page->host,
        ]);
    }

    private function fetchLastByPid(string $pid): ?ArticleRecord
    {
        return $this->articleRepository->findLastPublishedByPid($pid);
    }

    private function respondValidationErrors(FormEditArticleRequest $request): ResponseInterface
    {
        return $this->responder->respond('articles/edit', [
            'errorsBag' => $request->getErrorsBag(),
        ]);
    }
}