<?php


namespace Papyrus\Http\Actions\Simple\Articles;

use Papyrus\Domain\Articles\ListingFetcher;
use Papyrus\Http\Requests\Articles\ListArticlesRequest;
use Papyrus\Http\Responders\TwigResponder;

class ListArticleAction
{
    /**
     * @var TwigResponder
     */
    private $responder;

    /**
     * @var ListingFetcher
     */
    private $listingFetcher;

    public function __construct(TwigResponder $responder, ListingFetcher $listingFetcher)
    {
        $this->responder = $responder;
        $this->listingFetcher = $listingFetcher;
    }

    public function handle(ListArticlesRequest $request)
    {
        $processed = $request->process()->getProcessed();

        $articlesRecordsSet = $this->listingFetcher->fetch(1, $processed['author']);

        return $this->responder->respond('articles/list', ['articles' => $articlesRecordsSet]);
    }
}