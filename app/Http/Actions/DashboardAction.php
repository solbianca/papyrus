<?php


namespace Papyrus\Http\Actions;


use Papyrus\Http\Responders\TwigResponder;

class DashboardAction
{
    /**
     * @var TwigResponder
     */
    private $twigResponder;

    public function __construct(TwigResponder $twigResponder)
    {
        $this->twigResponder = $twigResponder;
    }

    public function handle()
    {
        return $this->twigResponder->respond('admin/dashboard');
    }
}