<?php


namespace Papyrus\Http\Responders;


use Papyrus\Http\Responders\Contracts\JsonApiResponderInterface;
use Psr\Http\Message\ResponseInterface;
use Tobscure\JsonApi\Collection;
use Tobscure\JsonApi\Document;
use Tobscure\JsonApi\Resource;
use Tobscure\JsonApi\SerializerInterface;

class JsonApiResponder extends Responder implements JsonApiResponderInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;


    private $document;

    public function __construct(ResponseInterface $response)
    {
        parent::__construct($response);
        $this->initDocument();
    }

    public function setSerializer(SerializerInterface $serializer): JsonApiResponderInterface
    {
        $this->serializer = $serializer;
        return $this;
    }

    public function getDocument(): Document
    {
        return $this->document;
    }

    public function initDocument(): JsonApiResponderInterface
    {
        $this->document = new Document();
        return $this;
    }

    public function resource($data, array $relationship = null): JsonApiResponderInterface
    {
        if (!$this->serializer) {
            throw  new \RuntimeException("Serializer not set");
        }

        $resource = new Resource($data, $this->serializer);
        if ($relationship) {
            $this->document->setData($resource->with($relationship));
        } else {
            $this->document->setData($resource);
        }
        $this->document->setData($resource);

        return $this;
    }

    public function collection($data, array $relationship = null): JsonApiResponderInterface
    {
        if (!$this->serializer) {
            throw  new \RuntimeException("Serializer not set");
        }

        $collection = new Collection($data, $this->serializer);

        if ($relationship) {
            $this->document->setData($collection->with($relationship));
        } else {
            $this->document->setData($collection);
        }

        return $this;
    }

    public function respond(int $status = 200, array $headers = []): ResponseInterface
    {
        $this->setStatus($status);

        $defaultHeaders = ['Content-Type' => 'application/vnd.api+json', 'Accept' => 'application/vnd.api+json'];
        foreach ($defaultHeaders as $key => $value) {
            $headers[$key] = $value;
        }
        $this->setHeaders($headers);

        $this->setContent(json_encode($this->document->toArray(), JSON_UNESCAPED_SLASHES));

        return $this->send();
    }
}