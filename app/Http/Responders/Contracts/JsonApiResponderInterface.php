<?php


namespace Papyrus\Http\Responders\Contracts;


use Psr\Http\Message\ResponseInterface;
use Tobscure\JsonApi\Document;
use Tobscure\JsonApi\SerializerInterface;

interface JsonApiResponderInterface
{

    public function setSerializer(SerializerInterface $serializer): JsonApiResponderInterface;

    public function getDocument(): Document;

    public function initDocument(): JsonApiResponderInterface;

    /**
     * @param object $data
     * @param array|null $relationship
     * @return JsonApiResponderInterface
     */
    public function resource($data, array $relationship = null): JsonApiResponderInterface;

    /**
     * @param object $data
     * @param array|null $relationship
     * @return JsonApiResponderInterface
     */
    public function collection($data, array $relationship = null): JsonApiResponderInterface;

    public function respond(int $status = 200, array $headers = []): ResponseInterface;
}