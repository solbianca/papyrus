<?php


namespace Papyrus\Http\Responders\Contracts;


use Psr\Http\Message\ResponseInterface;

interface ResponderInterface
{
    public function setContent(string $content): ResponderInterface;

    public function setHeaders(array $headers): ResponderInterface;

    public function appendHeaders(array $headers): ResponderInterface;

    public function setStatus(int $code, string $reasonPhrase = ''): ResponderInterface;

    public function send(): ResponseInterface;
}