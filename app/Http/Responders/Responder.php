<?php

namespace Papyrus\Http\Responders;

use Papyrus\Http\Responders\Contracts\ResponderInterface;
use Psr\Http\Message\ResponseInterface;

class Responder implements ResponderInterface
{
    /**
     * @var ResponseInterface
     */
    protected $response;

    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    public function setContent(string $content): ResponderInterface
    {
        $body = $this->response->getBody();
        $body->write($content);
        return $this;
    }

    public function setStatus(int $code, string $reasonPhrase = ''): ResponderInterface
    {
        $this->response = $this->response->withStatus($code, $reasonPhrase);
        return $this;
    }

    public function setHeaders(array $headers): ResponderInterface
    {
        $headersToRemove = array_keys($this->response->getHeaders());
        foreach ($headersToRemove as $header) {
            $this->response = $this->response->withoutHeader($header);
        }

        foreach ($headers as $key => $value) {
            $this->response = $this->response->withHeader($key, $value);
        }
        return $this;
    }

    public function appendHeaders(array $headers): ResponderInterface
    {
        foreach ($headers as $key => $value) {
            $this->response = $this->response->withAddedHeader($key, $value);
        }
        return $this;
    }

    public function send(): ResponseInterface
    {
        return $this->response;
    }
}