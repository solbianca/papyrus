<?php


namespace Papyrus\Http\Responders;


use Psr\Http\Message\ResponseInterface;
use Slim\Views\Twig;

class TwigResponder
{
    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var string
     */
    private $fileExtension = 'twig';

    public function __construct(ResponseInterface $response, Twig $twig)
    {
        $this->response = $response;
        $this->twig = $twig;
    }

    public function respond(string $template, array $params = []): ResponseInterface
    {
        $response = $this->prepareResponse();

        $template = $template . '.' . $this->fileExtension;
        return $this->twig->render($response, $template, $params);
    }

    private function prepareResponse()
    {
        $response = $this->response->withStatus(200);

        if (!$response->hasHeader('content-type')) {
            $response = $response->withAddedHeader('content-type', 'text/html; charset=UTF-8');
        }

        return $response;
    }

    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }
}