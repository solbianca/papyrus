<?php


namespace Papyrus\Http\Responders;


use Papyrus\Application\Validator\ErrorMessage;
use Papyrus\Application\Validator\ValidationErrorsBag;

class JsonApiErrorResponder extends Responder
{
    public function respond(ValidationErrorsBag $errorsBag)
    {
        $this->setStatus(422);

        $headers = ['Content-Type' => 'application/vnd.api+json', 'Accept' => 'application/vnd.api+json'];
        $this->setHeaders($headers);

        $this->setContent(json_encode($this->serializeErrors($errorsBag), JSON_UNESCAPED_SLASHES));

        return $this->send();
    }

    private function serializeErrors(ValidationErrorsBag $errorsBag): array
    {
        if ($errorsBag->isEmpty()) {
            return [];
        }

        $errors = [];
        foreach ($errorsBag->all() as $errorMessage) {
            $errors[] = $this->serializeError($errorMessage);
        }
        return $errors;
    }

    private function serializeError(ErrorMessage $errorMessage)
    {
        return [
            'code' => $errorMessage->getCode(),
            'title' => $errorMessage->getMessage(),
            'source' => [
                'pointer' => $errorMessage->getPropertyPath(),
            ],
        ];
    }
}