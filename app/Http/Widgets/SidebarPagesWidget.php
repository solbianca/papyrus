<?php


namespace Papyrus\Http\Widgets;


use Papyrus\Application\Twig\WidgetContract;
use Papyrus\Domain\Repositories\PagesRepository;
use Slim\Views\Twig;

class SidebarPagesWidget implements WidgetContract
{
    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var PagesRepository
     */
    private $pagesRepository;

    public function __construct(Twig $twig, PagesRepository $pagesRepository)
    {
        $this->twig = $twig;
        $this->pagesRepository = $pagesRepository;
    }

    public function render(): string
    {
        $pagesRecordSet = $this->pagesRepository->select()->fetchRecordSet();
        return $this->twig->fetch('admin/widgets/sidebar-pages.twig', ['pages' => $pagesRecordSet]);
    }
}