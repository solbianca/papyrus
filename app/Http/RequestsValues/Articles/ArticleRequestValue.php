<?php

namespace Papyrus\Http\RequestsValues\Articles;


class ArticleRequestValue
{
    /**
     * @var string|null
     */
    private $pid = null;

    /**
     * @var string
     */
    private $author;

    /**
     * @var int
     */
    private $page_id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $body;

    /**
     * @var string
     */
    private $status;

    /**
     * @var int
     */
    private $weight;

    public function toArray(): array
    {
        return [
            'pid' => $this->pid,
            'author' => $this->author,
            'page_id' => $this->page_id,
            'title' => $this->title,
            'body' => $this->body,
            'status' => $this->status,
            'weight' => $this->weight,
        ];
    }

    public function getPid(): ?string
    {
        return $this->pid;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

    public function getPageId(): int
    {
        return $this->page_id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function isNewRecord(): bool
    {
        return isset($this->pid);
    }
}