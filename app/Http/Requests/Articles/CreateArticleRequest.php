<?php


namespace Papyrus\Http\Requests\Articles;


use Papyrus\Application\Http\ProcessedRequest;
use Papyrus\Application\Hydrator\Hydrator;
use Papyrus\Application\Sanitizer\Rules\Cast;
use Papyrus\Application\Sanitizer\Rules\Collection as SanitizeCollection;
use Papyrus\Domain\Articles\Dictionaries\ArticleStatusDictionary;
use Papyrus\Domain\Articles\Dictionaries\ArticleTypeDictionary;
use Papyrus\Http\RequestsValues\Articles\ArticleRequestValue;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class CreateArticleRequest extends ProcessedRequest
{

    public function getSanitizerRules(): array
    {
        return [
            'article' => [
                new SanitizeCollection([
                    'page_id' => [new Cast('int')],
                    'weight' => [new Cast('int')],
                ]),
            ],
        ];
    }

    public function getValidationRules(): array
    {
        return [
            'article' => [
                new Type('array'),
                new NotBlank(['message' => 'Форма не может быть пустой']),
                new Collection([
                    'author' => [
                        new NotBlank(['message' => 'Необходимо указать автора статьи']),
                        new Type(['type' => 'string', 'message' => 'Автор должен быть строкой']),
                    ],
                    'page_id' => [
                        new NotBlank(['message' => 'Необходимо указать id страницы для которой создается статья']),
                        new Type(['type' => 'integer', 'message' => 'Идентификатор статьи должен быть целым числом']),
                    ],
                    'title' => [
                        new NotBlank(['message' => 'Заголовок не должно быть пустым']),
                        new Length(['min' => 3, 'max' => '256']),
                    ],
                    'body' => [
                        new NotBlank(['message' => 'Статья не должна быть пустой']),
                        new Length(['min' => 3, 'max' => 10000]),
                    ],
                    'weight' => [
                        new NotBlank(['message' => 'Вес не может быть пустым значением']),
                        new Type(['type' => 'integer', 'message' => 'Вес статьи должно быть целым числом',]),
                    ],
                    'status' => [
                        new NotBlank(['message' => 'Необходимо явно указать статус статьи']),
                        new Choice([
                            'choices' => [ArticleStatusDictionary::PUBLISHED, ArticleStatusDictionary::HIDE],
                            'message' => 'Неправильный статус статьи',
                        ]),
                    ],
                ]),
            ],
        ];
    }

    public function getRequestValue(): ArticleRequestValue
    {
        $processedArticle = $this->getProcessed()['article'];
        return Hydrator::factory()->hydrate(new ArticleRequestValue(), $processedArticle);
    }
}