<?php


namespace Papyrus\Http\Requests\Articles;


use Papyrus\Application\Http\ProcessedRequest;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class FormCreateArticleRequest extends ProcessedRequest
{
    public function getValidationRules(): array
    {
        return [
            'author' => [
                new NotBlank(['message' => 'Необходимо Указать автора статьи']),
            ],
            'path' => [
                new NotBlank(['message' => 'Необходимо указать страницу для которой создается статья']),
                new Type(['type' => 'string', 'message' => 'Вес статьи должно быть целым числом']),
            ],
        ];
    }
}