<?php


namespace Papyrus\Http\Requests\Articles;


use Papyrus\Application\Http\ProcessedRequest;
use Symfony\Component\Validator\Constraints\NotBlank;

class FormEditArticleRequest extends ProcessedRequest
{
    public function getValidationRules(): array
    {
        return [
            'author' => [
                new NotBlank(['message' => 'Необходимо указать автора статьи']),
            ],
        ];
    }
}