<?php


namespace Papyrus\Http\Requests\Articles;


use Papyrus\Application\Http\ProcessedRequest;
use Papyrus\Http\ValidationRules\PageExistByPath;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class ListArticlesRequest extends ProcessedRequest
{
    public function getValidationRules(): array
    {
        return [
            'page' => [new NotBlank(), new Type(['type' => 'string']), new PageExistByPath()],
            'author' => [new NotBlank(), new Type(['type' => 'string'])],
        ];
    }
}