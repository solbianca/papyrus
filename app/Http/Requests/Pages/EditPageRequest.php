<?php

namespace Papyrus\Http\Requests\Pages;

use Papyrus\Application\Http\ProcessedRequest;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;

class EditPageRequest extends ProcessedRequest
{
    public function getValidationRules(): array
    {
        return [
            'page' => new Collection([
                'title' => [new NotBlank()],
            ]),
        ];
    }
}