<?php


namespace Papyrus\Http\Serializers;


use Papyrus\Application\Helpers\DateTimeFormatter;
use Papyrus\Persistent\Article\ArticleRecord;
use Tobscure\JsonApi\AbstractSerializer;

class ArticleSerializer extends AbstractSerializer
{
    protected $type = 'article';

    /**
     * @param ArticleRecord $articleRecord
     * @param array|null $fields
     * @return array
     */
    public function getAttributes($articleRecord, array $fields = null)
    {
        return [
            'page_id' => $articleRecord->page_id,
            'author' => $articleRecord->author,
            'title' => $articleRecord->title,
            'body' => $articleRecord->body,
            'version' => $articleRecord->version,
            'weight' => $articleRecord->weight,
            'status' => $articleRecord->status,
            'created_at' => $this->toW3c($articleRecord->created_at),
            'updated_at' => $this->toW3c($articleRecord->updated_at),
        ];
    }

    /**
     * @param ArticleRecord $model
     * @return string
     */
    public function getId($model)
    {
        return $model->pid;
    }

    private function toW3c(string $datetime): string
    {
        return DateTimeFormatter::from($datetime)->toW3C();
    }
}