<?php


namespace Papyrus\Domain\Repositories;

use Papyrus\Application\Repositories\Repository;
use Papyrus\Domain\Articles\Dictionaries\ArticleStatusDictionary;
use Papyrus\Persistent\Article\Article;
use Papyrus\Persistent\Article\ArticleRecord;
use Papyrus\Persistent\Article\ArticleRecordSet;

class ArticlesRepository extends Repository
{
    protected $mapper = Article::class;

    public function findLastPublishedByPid(string $pid): ?ArticleRecord
    {
        $mapperSelect = $this->select()
            ->where('pid = ', $pid);

        $mapperSelect->where('status =', ArticleStatusDictionary::PUBLISHED)
            ->orderBy('version desc');

        return $mapperSelect->fetchRecord();
    }

    public function findLastByPid(string $pid, array $with = []): ?ArticleRecord
    {
        $mapperSelect = $this->select()
            ->where('pid = ', $pid);
        
        if ($with) {
            $mapperSelect->with($with);
        }

        $mapperSelect->orderBy('version desc');

        return $mapperSelect->fetchRecord();
    }

    public function findForListing(int $pageId): ArticleRecordSet
    {
        $articlesRecordsSet = $this->select()
            ->resetFrom()
            ->from('articles as a')
            ->where('a.page_id = ', $pageId)
            ->andWhere('a.status =', ArticleStatusDictionary::PUBLISHED)
            ->andWhere("a.version = (select max(version) from articles where pid = a.pid and status = 'published')")
            ->orderBy('a.weight desc')
            ->fetchRecordSet();

        return $articlesRecordsSet;
    }

    public function findForAdminListing(int $pageId): ArticleRecordSet
    {
        $articlesRecordsSet = $this->select()
            ->resetFrom()
            ->from('articles as a')
            ->where('a.page_id = ', $pageId)
            ->andWhere('a.version = (select max(version) from articles where pid = a.pid)')
            ->orderBy('a.weight desc')
            ->fetchRecordSet();

        return $articlesRecordsSet;
    }

    public function findAllPublishedAndActiveByPid(string $pid): ArticleRecordSet
    {
        $articlesRecordsSet = $this->select()
            ->where('pid = ', $pid)
            ->andWhere('status =', ArticleStatusDictionary::PUBLISHED)
            ->orderBy('version desc')
            ->fetchRecordSet();

        return $articlesRecordsSet;
    }
}