<?php


namespace Papyrus\Domain\Repositories;


use Papyrus\Application\Repositories\Repository;
use Papyrus\Persistent\Page\Page;

class PagesRepository extends Repository
{
    protected $mapper = Page::class;

    public function isExistByPath(string $path): bool
    {
        $count = $this->select()->where('path =', $path)->fetchCount('id');

        return ($count === 1) ? true : false;
    }
}