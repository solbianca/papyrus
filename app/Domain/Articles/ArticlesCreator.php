<?php


namespace Papyrus\Domain\Articles;


use Papyrus\Application\Writer\Writer;
use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Http\RequestsValues\Articles\ArticleRequestValue;
use Papyrus\Persistent\Article\Article;
use Papyrus\Persistent\Article\ArticleRecord;

class ArticlesCreator
{
    /**
     * @var ArticlesRepository
     */
    private $articleRepository;

    /**
     * @var Writer
     */
    private $writer;

    /**
     * @var PidGenerator
     */
    private $pidGenerator;

    public function __construct(ArticlesRepository $articleRepository, Writer $writer, PidGenerator $pidGenerator)
    {
        $this->articleRepository = $articleRepository;
        $this->writer = $writer;
        $this->pidGenerator = $pidGenerator;
    }

    public function create(ArticleRequestValue $articleRequestValue)
    {
        return $this->createArticle($articleRequestValue);
    }

    private function createArticle(ArticleRequestValue $articleRequestValue): ArticleRecord
    {
        $articleData = $articleRequestValue->toArray();
        $articleData['pid'] = $this->generatePid();
        $articleData['version'] = 1;

        return $this->writer->create(Article::class, $articleData);
    }

    private function generatePid(): string
    {
        return $this->pidGenerator->generate();
    }
}