<?php


namespace Papyrus\Domain\Articles\Difference;


use Papyrus\Persistent\Article\ArticleRecord;

class DifferencePayload
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $body;

    /**
     * @var ArticleRecord
     */
    private $old;

    /**
     * @var ArticleRecord
     */
    private $new;

    public function __construct(string $title, string $body, ArticleRecord $old, ArticleRecord $new)
    {
        $this->title = $title;
        $this->body = $body;
        $this->old = $old;
        $this->new = $new;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function getOld(): ArticleRecord
    {
        return $this->old;
    }

    public function getNew(): ArticleRecord
    {
        return $this->new;
    }
}