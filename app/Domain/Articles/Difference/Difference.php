<?php


namespace Papyrus\Domain\Articles\Difference;


use Caxy\HtmlDiff\HtmlDiff;
use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Persistent\Article\ArticleRecord;

class Difference
{
    /**
     * @var ArticlesRepository
     */
    private $articleRepository;

    public function __construct(ArticlesRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    public function handle(int $oldArticleId, int $newArticleId): DifferencePayload
    {
        $this->checkOrThrow($oldArticleId, $newArticleId);

        $oldArticle = $this->fetchArticle($oldArticleId);
        $newArticle = $this->fetchArticle($newArticleId);

        $title = $this->compare($oldArticle->title, $newArticle->title);
        $body = $this->compare($oldArticle->body, $newArticle->body);

        return $this->createPayload($title, $body, $oldArticle, $newArticle);
    }

    private function checkOrThrow(int $oldArticleId, int $newArticleId)
    {
        if (!$this->articleRepository->isExistById($oldArticleId)) {
            throw new DifferenceException("Статья с id `{$oldArticleId}` не существует");
        }

        if (!$this->articleRepository->isExistById($newArticleId)) {
            throw new DifferenceException("Статья с id `{$newArticleId}` не существует");
        }
    }

    private function fetchArticle(int $id): ArticleRecord
    {
        return $this->articleRepository->findById($id);
    }

    private function compare(string $old, string $new): string
    {
        $htmlDiff = new HtmlDiff($old, $new);
        return $htmlDiff->build();
    }

    private function createPayload(
        string $title,
        string $body,
        ArticleRecord $old,
        ArticleRecord $new
    ): DifferencePayload {
        return new DifferencePayload($title, $body, $old, $new);
    }
}