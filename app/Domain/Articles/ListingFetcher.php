<?php


namespace Papyrus\Domain\Articles;


use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Persistent\Article\ArticleRecordSet;

class ListingFetcher
{
    /**
     * @var ArticlesRepository
     */
    private $articlesRepository;

    public function __construct(ArticlesRepository $articlesRepository)
    {
        $this->articlesRepository = $articlesRepository;
    }

    public function fetch(int $pageId, string $author = ''): ArticleRecordSet
    {
        $articleSet = $this->fetchArticlesForPage($pageId);

        if ($articleSet->isEmpty()) {
            return $articleSet;
        }

        if (!$author) {
            return $articleSet;
        }
        
        return $articleSet;
    }

    private function fetchArticlesForPage(int $pageId): ArticleRecordSet
    {
        return $this->articlesRepository->findForListing($pageId);
    }
}