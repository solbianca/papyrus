<?php


namespace Papyrus\Domain\Articles;


use Papyrus\Application\Writer\Writer;
use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Http\RequestsValues\Articles\ArticleRequestValue;
use Papyrus\Persistent\Article\Article;
use Papyrus\Persistent\Article\ArticleRecord;

class ArticlesUpdater
{
    /**
     * @var ArticlesRepository
     */
    private $articleRepository;

    /**
     * @var Writer
     */
    private $writer;

    public function __construct(ArticlesRepository $articleRepository, Writer $writer)
    {
        $this->articleRepository = $articleRepository;
        $this->writer = $writer;
    }

    public function update(ArticleRequestValue $articleRequestValue): ArticleRecord
    {
        return $this->updateArticle($articleRequestValue);
    }

    private function updateArticle(ArticleRequestValue $articleRequestValue): ArticleRecord
    {
        $articleRecord = $this->fetchArticleByPid($articleRequestValue->getPid());
        $articleData = $articleRequestValue->toArray();
        $articleData['version'] = $articleRecord->version + 1;
        $articleData['page_id'] = $articleRecord->page_id;
        return $this->writer->create(Article::class, $articleData);
    }

    private function fetchArticleByPid(string $pid): ArticleRecord
    {
        $articleRecord = $this->articleRepository->findLastPublishedByPid($pid);
        if (!$articleRecord) {
            throw new \RuntimeException("Invalid pid");
        }
        return $articleRecord;
    }
}