<?php


namespace Papyrus\Domain\Articles;


class PidGenerator
{
    /**
     * @var string
     */
    private $salt = 'RftpPS75jc9u8Tp0HZ94DWQNwqVTVUnww58pfycqZ_Y4gl1_ByUqSGaPst4NYapZ';

    /**
     * @var int
     */
    private $length = 16;

    public function generate(): string
    {
        $hex = md5($this->salt . uniqid("", true) . (string)microtime());

        $pack = pack('H*', $hex);
        $tmp = base64_encode($pack);

        $uid = preg_replace("#(*UTF8)[^A-Za-z0-9]#", "", $tmp);

        $length = max(4, min(128, $this->length));

        return substr($uid, 0, $length);
    }
}