<?php


namespace Papyrus\Domain\Articles\Dictionaries;


class ArticleStatusDictionary
{
    const PUBLISHED = 'published';
    const HIDE = 'hide';
}