<?php


namespace Papyrus\Domain\Auth;


class SessionManager
{
    public function __construct()
    {
    }

    public function setUserData(string $token, array $userData)
    {
        $_SESSION[$token] = $userData;
    }

    public function getUserData(string $token): ?array
    {
        return $_SESSION[$token] ?? null;
    }
}