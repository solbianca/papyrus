<?php


namespace Papyrus\Domain\Auth;


use Papyrus\Persistent\User\UserRecord;

class Identity implements IdentityContract
{
    /**
     * @var UserRecord
     */
    private $userRecord;

    public function __construct(UserRecord $userRecord = null)
    {
        $this->userRecord = $userRecord;
    }

    public function isGuest(): bool
    {
        return $this->userRecord ? false : true;
    }

    public function getId(): ?int
    {
        return $this->userRecord->id ?? null;
    }

    public function getUser(): ?UserRecord
    {
        return $this->userRecord;
    }
}