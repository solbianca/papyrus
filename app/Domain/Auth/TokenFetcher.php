<?php


namespace Papyrus\Domain\Auth;


use Psr\Http\Message\ServerRequestInterface;

class TokenFetcher implements TokenFetcherContract
{
    /**
     * @var string
     */
    private $target;

    /**
     * @var string
     */
    private $key;

    public function __construct(string $target, string $key)
    {
        $this->target = $target;
        $this->key = $key;
    }

    public function fetch(ServerRequestInterface $request): string
    {
        switch ($this->target) {
            case 'environment':
                return $this->fetchFromEnvironment($request);
            case 'header':
                return $this->fetchFromHeader($request);
            case 'cookie':
                return $this->fetchFromCookies($request);
            default:
                return '';
        }
    }

    private function fetchFromEnvironment(ServerRequestInterface $request): string
    {
        $params = $request->getServerParams();
        return $params[strtoupper($this->key)] ?? '';
    }

    private function fetchFromHeader(ServerRequestInterface $request): string
    {
        return $request->getHeaderLine($this->key);
    }

    private function fetchFromCookies(ServerRequestInterface $request): string
    {
        $cookies = $request->getCookieParams();
        return $cookies[$this->key] ?? '';
    }
}