<?php


namespace Papyrus\Domain\Auth;


use Papyrus\Domain\Repositories\UsersRepository;

class Authenticator
{
    /**
     * @var SessionManager
     */
    private $sessionManager;

    /**
     * @var UsersRepository
     */
    private $userRepository;

    public function __construct(SessionManager $sessionManager, UsersRepository $userRepository)
    {
        $this->sessionManager = $sessionManager;
        $this->userRepository = $userRepository;
    }

    public function authenticate(string $token): ?Identity
    {
        $userData = $this->sessionManager->getUserData($token);
        if (!isset($userData['id'])) {
            return null;
        }

        /* @var \Papyrus\Persistent\User\UserRecord $userRecord */
        $userRecord = $this->userRepository->findById($userData['id']);

        if (!$userRecord) {
            return null;
        }

        return new Identity($userRecord);
    }
}