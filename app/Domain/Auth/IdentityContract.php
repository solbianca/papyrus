<?php


namespace Papyrus\Domain\Auth;


use Papyrus\Persistent\User\UserRecord;

interface IdentityContract
{
    public function getId(): ?int;

    public function isGuest(): bool;

    public function getUser(): ?UserRecord;
}