<?php


namespace Papyrus\Domain\Auth;


use Psr\Http\Message\ServerRequestInterface;

interface TokenFetcherContract
{
    public function fetch(ServerRequestInterface $request): string;
}