<?php

namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Unit extends \Codeception\Module
{
    /**
     * Очистить таблицу от данных
     *
     * @param string $tableName
     * @throws \Codeception\Exception\ModuleConfigException
     * @throws \Codeception\Exception\ModuleException
     */
    public function cleanupTable(string $tableName)
    {
        /** @var \Codeception\Module\Db $dbModule */
        $dbModule = $this->getModule('Db');
        $dbh = $dbModule->_getDriver()->getDbh();
        $dbh->exec("TRUNCATE `{$tableName}`");
    }

    /**
     * Заполнить таблицу данными
     *
     * @param string $table
     * @param array $data
     * @throws \Codeception\Exception\ModuleConfigException
     * @throws \Codeception\Exception\ModuleException
     */
    public function populateTable(string $table, array $data)
    {
        /** @var \Codeception\Module\Db $dbModule */
        $dbModule = $this->getModule('Db');
        foreach ($data as $item) {
            $dbModule->haveInDatabase($table, $item);
        }
    }

    /**
     * Получить PDO соединение модуля `Db`
     *
     * @return \PDO
     * @throws \Codeception\Exception\ModuleException
     */
    public function getPdoConnection(): \PDO
    {
        /** @var \Codeception\Module\Db $dbModule */
        $dbModule = $this->getModule('Db');
        return $dbModule->_getDbh();
    }
}
