<?php

namespace Tests\unit\Application\Http;

use Papyrus\Application\Http\ProcessedRequest;
use Papyrus\Application\Sanitizer\Rules\RuleContract;
use Papyrus\Application\Sanitizer\Rules\Trim;
use Papyrus\Application\Sanitizer\Sanitizer;
use Papyrus\Application\Sanitizer\SanitizerContract;
use Papyrus\Application\Validator\Contracts\ValidatorContract;
use Papyrus\Application\Validator\SymfonyValidatorAdapter;
use Papyrus\Application\Validator\ValidationException;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\ValidatorBuilder;
use Tests\utilites\Factories\RequestFactory;

class ProcessedRequestTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    private function getAttributes()
    {
        return [
            'name' => ' John Doe ',
            'ids' => [1, 2, 3],
            'foo' => 'buz',
        ];
    }

    private function createRequest()
    {
        return RequestFactory::create('GET', 'http://example.com?foo=bar', ['content-type' => 'application/json'],
            [], [], json_encode($this->getAttributes()));
    }

    private function createRequestWithoutRules(): ProcessedRequest
    {
        $symfonyValidator = (new ValidatorBuilder())->getValidator();
        $validator = new SymfonyValidatorAdapter($symfonyValidator);
        $sanitizer = new Sanitizer();

        return new class($this->createRequest(), $validator, $sanitizer) extends ProcessedRequest
        {
        };
    }

    private function createRequestWithRules(): ProcessedRequest
    {
        $symfonyValidator = (new ValidatorBuilder())->getValidator();
        $validator = new SymfonyValidatorAdapter($symfonyValidator);
        $sanitizer = new Sanitizer();

        return new class($this->createRequest(), $validator, $sanitizer) extends ProcessedRequest
        {
            public function getValidationRules(): array
            {
                return [
                    'name' => new NotBlank(),
                    'ids' => [
                        new NotBlank(),
                        new Type(['type' => 'array']),
                    ],
                    'foo' => new NotBlank(),
                ];
            }

            public function getSanitizerRules(): array
            {
                return [
                    'name' => [new Trim()],
                ];
            }
        };
    }

    private function createRequestForProcessOrThrow(): ProcessedRequest
    {
        $symfonyValidator = (new ValidatorBuilder())->getValidator();
        $validator = new SymfonyValidatorAdapter($symfonyValidator);
        $sanitizer = new Sanitizer();

        return new class($this->createRequest(), $validator, $sanitizer) extends ProcessedRequest
        {
            public function getValidationRules(): array
            {
                return [
                    'name' => new Type(['type' => 'array']),
                    'ids' => [
                        new NotBlank(),
                        new Type(['type' => 'array']),
                    ],
                    'foo' => new NotBlank(),
                ];
            }
        };
    }

    public function testProcessWithRules()
    {
        $processedRequest = $this->createRequestWithRules();

        $processed = $processedRequest->process()->getProcessed();

        $this->tester->assertEquals(['foo' => 'buz', 'name' => 'John Doe', 'ids' => [1, 2, 3]], $processed);

        $errorsBag = $processedRequest->getErrorsBag();

        $this->tester->assertTrue($errorsBag->isEmpty());
        $this->tester->assertEquals(0, $errorsBag->count());
        $this->tester->assertTrue($processedRequest->passed());
        $this->tester->assertFalse($processedRequest->failed());
    }

    public function testProcessWithoutRules()
    {
        $processedRequest = $this->createRequestWithoutRules();

        $processed = $processedRequest->process()->getProcessed();

        $this->tester->assertEquals(['foo' => 'buz', 'name' => ' John Doe ', 'ids' => [1, 2, 3]], $processed);

        $errorsBag = $processedRequest->getErrorsBag();

        $this->tester->assertTrue($errorsBag->isEmpty());
        $this->tester->assertEquals(0, $errorsBag->count());
        $this->tester->assertTrue($processedRequest->passed());
        $this->tester->assertFalse($processedRequest->failed());
    }

    public function testProcessOrThrowWithRules()
    {
        $processedRequest = $this->createRequestWithRules();

        $processed = $processedRequest->processOrThrow()->getProcessed();

        $this->tester->assertEquals(['foo' => 'buz', 'name' => 'John Doe', 'ids' => [1, 2, 3]], $processed);

        $errorsBag = $processedRequest->getErrorsBag();

        $this->tester->assertTrue($errorsBag->isEmpty());
        $this->tester->assertEquals(0, $errorsBag->count());
        $this->tester->assertTrue($processedRequest->passed());
        $this->tester->assertFalse($processedRequest->failed());
    }

    public function testProcessOrThrow()
    {
        $processedRequest = $this->createRequestForProcessOrThrow();

        $this->tester->expectThrowable(ValidationException::class, function () use ($processedRequest) {
            $processed = $processedRequest->processOrThrow();
        });
    }

    public function testGetValidationRules()
    {
        $this->tester->assertEquals([], $this->createRequestWithoutRules()->getValidationRules());

        $rules = $this->createRequestWithRules()->getValidationRules();
        $this->tester->assertInstanceOf(NotBlank::class, $rules['name']);
        $this->tester->assertInstanceOf(NotBlank::class, $rules['ids'][0]);
        $this->tester->assertInstanceOf(Type::class, $rules['ids'][1]);
    }

    public function testGetSanitizerRules()
    {
        $this->tester->assertEquals([], $this->createRequestWithoutRules()->getValidationRules());

        $rules = $this->createRequestWithRules()->getSanitizerRules();

        $this->tester->assertCount(1, $rules);
        $this->tester->assertInstanceOf(RuleContract::class, $rules['name'][0]);
    }

    public function testGetRequestParams()
    {
        $processedRequest = $this->createRequestWithoutRules();
        $params = $processedRequest->getRequestParams();
        $this->tester->assertCount(3, $params);
        $this->tester->assertNotEquals('bar', $params['foo']);
        $this->tester->assertEquals('buz', $params['foo']);
    }

    public function testGetRequest()
    {
        $this->tester->assertInstanceOf(ServerRequestInterface::class,
            $this->createRequestWithoutRules()->getRequest());
    }

    public function testGetValidator()
    {
        $processedRequest = $this->createRequestWithoutRules();

        $this->tester->assertInstanceOf(ValidatorContract::class, $processedRequest->getValidator());
    }

    public function testGetSanitizer()
    {
        $processedRequest = $this->createRequestWithoutRules();

        $this->tester->assertInstanceOf(SanitizerContract::class, $processedRequest->getSanitizer());
    }

    public function testPassed()
    {
        $processedRequest = $this->createRequestWithoutRules();
        $this->tester->expectThrowable(\RuntimeException::class, function () use ($processedRequest) {
            $processedRequest->passed();
        });
    }

    public function testFailed()
    {
        $processedRequest = $this->createRequestWithoutRules();
        $this->tester->expectThrowable(\RuntimeException::class, function () use ($processedRequest) {
            $processedRequest->failed();
        });
    }

    public function testGetErrorsBag()
    {
        $processedRequest = $this->createRequestWithoutRules();
        $this->tester->expectThrowable(\RuntimeException::class, function () use ($processedRequest) {
            $processedRequest->getErrorsBag();
        });
    }
}