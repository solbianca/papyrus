<?php namespace Application;

use Papyrus\Application\Config\Config;
use Papyrus\Application\Config\ConfigException;

class ConfigTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    private function createConfig(string $delimiter = null)
    {
        if ($delimiter) {
            return new Config(__DIR__ . '/../../../_data/configs', $delimiter);
        }
        return new Config(__DIR__ . '/../../../_data/configs');
    }

    public function testGetExistedConfig()
    {
        $config = $this->createConfig();

        $this->tester->assertEquals('Test', $config->get('test:app'));
        $this->tester->assertEquals('buzz', $config->get('test:foo:bar'));
    }

    public function testGetDefaultValue()
    {
        $config = $this->createConfig();

        $this->tester->assertEquals(null, $config->get('test:nope'));
        $this->tester->assertEquals('yep', $config->get('test:nope', 'yep'));
    }

    public function testGetExistedConfigWithDifferentDelimiter()
    {
        $config = $this->createConfig('.');

        $this->tester->assertEquals('Test', $config->get('test.app'));
        $this->tester->assertEquals('buzz', $config->get('test.foo.bar'));
    }

    public function testGetNotExistedConfig()
    {
        $config = $this->createConfig();

        $this->tester->expectThrowable(ConfigException::class, function () use ($config) {
            $config->get('nope:app');
        });
    }

    public function testGetWithInvalidKey()
    {
        $config = $this->createConfig();
        $this->tester->expectThrowable(ConfigException::class, function () use ($config) {
            $config->get('');
        });
    }

    public function testGetFile()
    {
        $config = $this->createConfig();

        $this->tester->assertEquals(['app' => 'Test', 'foo' => ['bar' => 'buzz']], $config->get('test'));
    }
}