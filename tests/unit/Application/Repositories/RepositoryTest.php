<?php


namespace Tests\unit\Application\Repositories;


use Tests\utilites\Fakes\FakeRepository;
use Tests\utilites\Populators\PagesPopulator;

class RepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var FakeRepository
     */
    private $repository;

    /**
     * @var PagesPopulator
     */
    private $populator;

    protected function _before()
    {
        $this->populator = new PagesPopulator($this->tester);
        $this->repository = $this->createRepository();
        $this->populator->cleanup();
        $this->populator->populate();
    }

    protected function _after()
    {
        $this->populator->cleanup();
    }

    private function createRepository(): FakeRepository
    {
        return new FakeRepository(container()->get('atlas'));
    }

    public function testSuccessFindById()
    {
        /* @var \Papyrus\Persistent\Page\PageRecord $record */
        $record = $this->repository->findById(1);

        $this->tester->assertNotNull($record);
        $this->tester->assertEquals(1, $record->id);
        $this->tester->assertEquals('/test', $record->path);
    }

    public function testFailFindById()
    {
        /* @var \Papyrus\Persistent\Page\PageRecord $record */
        $record = $this->repository->findById(187463);

        $this->tester->assertNull($record);
    }

    public function testSuccessFindByConditionWithOneParameters()
    {
        $recordSet = $this->repository->findBy('id = 1');

        $this->tester->assertEquals(1, $recordSet->count());

        /* @var \Papyrus\Persistent\Page\PageRecord $record */
        $record = $recordSet[0];
        $this->tester->assertEquals(1, $record->id);
        $this->tester->assertEquals('/test', $record->path);

        $recordSet = $this->repository->findBy('id in (1,2,3)');

        $this->tester->assertEquals(3, $recordSet->count());
    }

    public function testSuccessFindByConditionWithTwoParameters()
    {
        $recordSet = $this->repository->findBy('id = ', 1);

        $this->tester->assertEquals(1, $recordSet->count());

        /* @var \Papyrus\Persistent\Page\PageRecord $record */
        $record = $recordSet[0];
        $this->tester->assertEquals(1, $record->id);
        $this->tester->assertEquals('/test', $record->path);

        $recordSet = $this->repository->findBy('id in ', [1, 2, 3]);

        $this->tester->assertEquals(3, $recordSet->count());
    }

    public function testSuccessFindByConditionOneElementOneParameter()
    {
        $recordSet = $this->repository->findBy(['id = 1']);

        $this->tester->assertEquals(1, $recordSet->count());

        /* @var \Papyrus\Persistent\Page\PageRecord $record */
        $record = $recordSet[0];
        $this->tester->assertEquals(1, $record->id);
        $this->tester->assertEquals('/test', $record->path);
    }

    public function testSuccessFindByConditionOfOneElementTwoParameters()
    {
        $recordSet = $this->repository->findBy(['id =', 1]);

        $this->tester->assertEquals(1, $recordSet->count());

        /* @var \Papyrus\Persistent\Page\PageRecord $record */
        $record = $recordSet[0];
        $this->tester->assertEquals(1, $record->id);
        $this->tester->assertEquals('/test', $record->path);
    }

    public function testSuccessFindByConditionOfOneElementThreeParameters()
    {
        $recordSet = $this->repository->findBy(['and', 'id =', 1]);

        $this->tester->assertEquals(1, $recordSet->count());

        /* @var \Papyrus\Persistent\Page\PageRecord $record */
        $record = $recordSet[0];
        $this->tester->assertEquals(1, $record->id);
        $this->tester->assertEquals('/test', $record->path);
    }

    public function testSuccessFindByConditionOfTwoElement()
    {
        $recordSet = $this->repository->findBy(['id =', 1], ['or', 'id =', 2]);
        $this->tester->assertEquals(2, $recordSet->count());

        $recordSet = $this->repository->findBy(['status =', 'active'], ['and', 'id =', 1]);
        $this->tester->assertEquals(1, $recordSet->count());
    }

    public function conditionsProvider()
    {
        return [
            [
                [],
            ],
            [
                ['id ='],
            ],
            [
                'and',
                'id =',
                1,
                'MORE!!1',
            ],
            [
                [
                    'and',
                    'id =',
                    1,
                    'MORE!!1',
                ],
            ],
        ];
    }

    /**
     * @dataProvider conditionsProvider
     */
    public function testInvalidFindByCondition($condition)
    {
        $this->tester->expectThrowable(\RuntimeException::class, function () use ($condition) {
            $this->repository->findBy($condition);
        });
    }

    public function testIsExistById()
    {
        $this->tester->assertTrue($this->repository->isExistById(1));
        $this->tester->assertFalse($this->repository->isExistById(87534534));
    }
}
