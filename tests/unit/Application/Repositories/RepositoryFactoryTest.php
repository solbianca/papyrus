<?php


namespace Tests\unit\Application\Repositories;


use Papyrus\Application\Repositories\RepositoryFactory;
use Papyrus\Domain\Repositories\ArticlesRepository;

class RepositoryFactoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var RepositoryFactory
     */
    private $factory;

    protected function _before()
    {
        $this->factory = new RepositoryFactory();
    }

    protected function _after()
    {
    }

    public function testCreate()
    {
        $repository = $this->factory->create(ArticlesRepository::class);
        $this->tester->assertEquals(ArticlesRepository::class, get_class($repository));
    }

    public function testCreateClassNotExist()
    {
        $this->tester->expectThrowable(\RuntimeException::class, function () {
            $this->factory->create('\Foo\Bar');
        });
    }

    public function testCreateClassExistButNotExtendRepository()
    {
        $this->tester->expectThrowable(\RuntimeException::class, function () {
            $this->factory->create(\stdClass::class);
        });
    }
}