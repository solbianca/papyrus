<?php

namespace Tests\unit\Application;

use Papyrus\Application\Kernel\CallableResolver;
use Tests\utilites\Fakes\Container;
use Tests\utilites\Fakes\Resolved;

class CallableResolverTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    private $resolver;

    protected function _before()
    {
        $container = new Container();
        $this->resolver = new CallableResolver($container);
    }

    protected function _after()
    {
    }

    /**
     * @param string|null $key
     * @param mixed|null $value
     * @return CallableResolver
     */
    private function createResolver($key = null, $value = null)
    {
        $container = new Container();
        if ($key) {
            $container->set($key, $value);
        }
        return new CallableResolver($container);
    }

    public function testResolveFromContainer()
    {
        $resolver = $this->createResolver('foo', true);

        $this->tester->assertEquals([true, 'handle'], $resolver->resolve('foo'));
    }

    public function testResolveNewClass()
    {
        $resolver = $this->createResolver();
        $resolved = $resolver->resolve(Resolved::class);


        $this->tester->assertCount(2, $resolved);
        $this->tester->assertInstanceOf(Resolved::class, $resolved[0]);
        $this->tester->assertEquals('handle', $resolved[1]);
    }

    public function testResolveClassNotExist()
    {
        $resolver = $this->createResolver();

        $this->tester->expectThrowable(\RuntimeException::class, function () use ($resolver) {
            $resolver->resolve('Foo\Bar');
        });
    }
}

