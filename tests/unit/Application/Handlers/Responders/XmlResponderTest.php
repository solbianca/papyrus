<?php


namespace Papyrus\Application\Handlers\Responders;


use Slim\Http\Response;
use Tests\utilites\Factories\RequestFactory;

class XmlResponderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    private function createResponder(bool $displayErrorDetails)
    {
        $responder = new XmlResponder();
        $responder->setDisplayErrorDetails($displayErrorDetails);
        return $responder;
    }

    public function testRenderWithoutDisplayError()
    {
        $responder = $this->createResponder(false);
        $exception = new \Exception('Exception!');

        $response = $responder->respond(RequestFactory::create('GET', ''), new Response(), $exception, 'text/xml');

        $this->tester->assertEquals(500, $response->getStatusCode());
        $this->tester->assertEquals('text/xml', $response->getHeaderLine('content-type'));

        $content = ((string)$response->getBody());
        $this->tester->assertEquals("<error>\n<message>Slim Application Error</message>\n</error>", $content);
    }

    public function testRenderWithDisplayError()
    {
        $responder = $this->createResponder(true);
        $exception = new \Exception('Exception!', 0, new \Exception("More!"));

        $response = $responder->respond(RequestFactory::create('GET', ''), new Response(), $exception, 'text/xml');

        $reflection = new \ReflectionClass(XmlResponderTest::class);
        $fileName = $reflection->getFileName();
        $filePath = realpath($fileName);

        $this->tester->assertEquals(500, $response->getStatusCode());
        $this->tester->assertEquals('text/xml', $response->getHeaderLine('content-type'));

        $content = ((string)$response->getBody());
        $this->tester->assertTrue((bool)strpos($content, '<message>Slim Application Error</message>'));
        $this->tester->assertTrue((bool)strpos($content, '<type>Exception</type>'));
        $this->tester->assertTrue((bool)strpos($content, '<code>0</code>'));
        $this->tester->assertTrue((bool)strpos($content, '<message><![CDATA[Exception!]]></message>'));
        $this->tester->assertTrue((bool)strpos($content, "<file>{$filePath}</file>"));
        $this->tester->assertTrue((bool)strpos($content, '<line>49</line>'));
        $this->tester->assertTrue((bool)strpos($content, '<trace><![CDATA[#0 [internal function]:'));
    }
}