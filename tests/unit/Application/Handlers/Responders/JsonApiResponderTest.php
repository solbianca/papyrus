<?php


namespace Papyrus\Application\Handlers\Responders;


use Papyrus\Application\Validator\ErrorMessage;
use Papyrus\Application\Validator\ValidationErrorsBag;
use Papyrus\Application\Validator\ValidationException;
use Slim\Http\Response;
use Tests\utilites\Factories\RequestFactory;

class JsonApiResponderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    private function createResponder(bool $displayErrorDetails)
    {
        $responder = new JsonApiResponder();
        $responder->setDisplayErrorDetails($displayErrorDetails);
        return $responder;
    }

    private function createErrorMessage($message = 'error', $code = 42, $invalidValue = 'abc', $path = 'foo.bar')
    {
        return new ErrorMessage($message, $code, $invalidValue, $path);
    }

    public function testRenderWithWrongExceptionType()
    {
        $responder = $this->createResponder(false);
        $exception = new \Exception();

        $this->tester->expectThrowable(\RuntimeException::class, function () use ($responder, $exception) {
            $responder->respond(RequestFactory::create('GET', ''), new Response(), $exception,
                'application/vnd.api+json');
        });
    }

    public function testRenderWithoutErrorBad()
    {
        $responder = $this->createResponder(false);
        $exception = new ValidationException();

        $response = $responder->respond(RequestFactory::create('GET', ''), new Response(), $exception,
            'application/vnd.api+json');

        $this->tester->assertEquals(422, $response->getStatusCode());
        $this->tester->assertEquals('application/vnd.api+json', $response->getHeaderLine('content-type'));

        $content = json_decode((string)$response->getBody());
        $this->tester->assertEquals([], $content);
    }

    public function testRender()
    {
        $responder = $this->createResponder(false);
        $exception = new ValidationException();
        $errorsBag = new ValidationErrorsBag();
        $errorsBag->add($this->createErrorMessage());
        $exception->setValidationErrors($errorsBag);

        $response = $responder->respond(RequestFactory::create('GET', ''), new Response(), $exception,
            'application/vnd.api+json');

        $this->tester->assertEquals(422, $response->getStatusCode());
        $this->tester->assertEquals('application/vnd.api+json', $response->getHeaderLine('content-type'));

        $content = json_decode((string)$response->getBody());
        $this->tester->assertNotEmpty($content);
        $this->tester->assertTrue(is_array($content));
        $this->tester->assertCount(1, $content);
        $this->tester->assertEquals(42, $content[0]->code);
        $this->tester->assertEquals('error', $content[0]->title);
        $this->tester->assertEquals('foo.bar', $content[0]->source->pointer);
    }
}