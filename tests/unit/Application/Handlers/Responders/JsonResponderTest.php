<?php


namespace Papyrus\Application\Handlers\Responders;


use Slim\Http\Response;
use Tests\utilites\Factories\RequestFactory;

class JsonResponderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    private function createResponder(bool $displayErrorDetails)
    {
        $responder = new JsonResponder();
        $responder->setDisplayErrorDetails($displayErrorDetails);
        return $responder;
    }

    public function testRenderWithoutDisplayError()
    {
        $responder = $this->createResponder(false);
        $exception = new \Exception('Exception!');

        $response = $responder->respond(RequestFactory::create('GET', ''), new Response(), $exception,
            'application/json');

        $this->tester->assertEquals(500, $response->getStatusCode());
        $this->tester->assertEquals('application/json', $response->getHeaderLine('content-type'));

        $content = json_decode((string)$response->getBody());
        $this->tester->assertEquals('Slim Application Error', $content->message);
    }

    public function testRenderWithDisplayError()
    {
        $responder = $this->createResponder(true);
        $exception = new \Exception('Exception!', 0, new \Exception("More!"));

        $response = $responder->respond(RequestFactory::create('GET', ''), new Response(), $exception,
            'application/json');

        $reflection = new \ReflectionClass(JsonResponderTest::class);
        $fileName = $reflection->getFileName();
        $filePath = realpath($fileName);

        $this->tester->assertEquals(500, $response->getStatusCode());
        $this->tester->assertEquals('application/json', $response->getHeaderLine('content-type'));

        $content = json_decode((string)$response->getBody());
        $this->tester->assertEquals('Slim Application Error', $content->message);
        $this->tester->assertTrue(is_array($content->error));
        $this->tester->assertCount(2, $content->error);

        $this->tester->assertEquals('Exception', $content->error[0]->type);
        $this->tester->assertEquals(0, $content->error[0]->code);
        $this->tester->assertEquals('Exception!', $content->error[0]->message);
        $this->tester->assertEquals($filePath, $content->error[0]->file);
        $this->tester->assertTrue(is_integer($content->error[0]->line));
        $this->tester->assertNotEmpty($content->error[0]->trace);

        $this->tester->assertEquals('Exception', $content->error[1]->type);
        $this->tester->assertEquals(0, $content->error[1]->code);
        $this->tester->assertEquals('More!', $content->error[1]->message);
        $this->tester->assertEquals($filePath, $content->error[1]->file);
        $this->tester->assertTrue(is_integer($content->error[1]->line));
        $this->tester->assertNotEmpty($content->error[1]->trace);

    }
}