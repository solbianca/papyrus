<?php


namespace Papyrus\Application\Handlers\Responders;


use Slim\Http\Response;
use Tests\utilites\Factories\RequestFactory;

class ResponderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    private function createResponder()
    {
        $responder = new class() extends Responder
        {
            protected function output(\Throwable $error): string
            {
                return $error->getMessage();
            }
        };
        return $responder;
    }

    public function testRespond()
    {
        $responder = $this->createResponder();
        $exception = new \Exception('Exception!');

        $response = $responder->respond(RequestFactory::create('GET', ''), new Response(), $exception, 'text/html');

        $this->tester->assertEquals(500, $response->getStatusCode());
        $this->tester->assertEquals('text/html', $response->getHeaderLine('content-type'));
        $this->tester->assertEquals('Exception!', (string)$response->getBody());
    }
}