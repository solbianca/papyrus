<?php


namespace Papyrus\Application\Handlers\Responders;


use Slim\Http\Response;
use Tests\utilites\Factories\RequestFactory;

class HtmlResponderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    private function createResponder(bool $displayErrorDetails)
    {
        $responder = new HtmlResponder();
        $responder->setDisplayErrorDetails($displayErrorDetails);
        return $responder;
    }

    public function testRenderWithoutDisplayError()
    {
        $responder = $this->createResponder(false);
        $exception = new \Exception('Exception!');

        $response = $responder->respond(RequestFactory::create('GET', ''), new Response(), $exception, 'text/html');

        $this->tester->assertEquals(500, $response->getStatusCode());
        $this->tester->assertEquals('text/html', $response->getHeaderLine('content-type'));

        $content = ((string)$response->getBody());
        $this->tester->assertTrue((bool)strpos($content, '<h1>Slim Application Error</h1>'));
        $this->tester->assertFalse((bool)strpos($content, '<h2>Details</h2>'));
    }

    public function testRenderWithDisplayError()
    {
        $responder = $this->createResponder(true);
        $exception = new \Exception('Exception!', 100, new \Exception('More!'));

        $response = $responder->respond(RequestFactory::create('GET', ''), new Response(), $exception, 'text/html');

        $this->tester->assertEquals(500, $response->getStatusCode());
        $this->tester->assertEquals('text/html', $response->getHeaderLine('content-type'));

        $content = ((string)$response->getBody());

        $this->tester->assertTrue((bool)strpos($content, '<h1>Slim Application Error</h1>'));
        $this->tester->assertTrue((bool)strpos($content, '<h2>Details</h2>'));
        $this->tester->assertTrue((bool)strpos($content, '<div><strong>Code:</strong> 100</div>'));
    }
}