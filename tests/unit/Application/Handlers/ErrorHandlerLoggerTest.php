<?php

namespace Tests\unit\Application\Handlers;

use Papyrus\Application\Handlers\ErrorHandlerLogger;
use Tests\utilites\Fakes\FakeLogger;

class ErrorHandlerLoggerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var  ErrorHandlerLogger
     */
    private $errorHandlerLogger;

    /**
     * @var FakeLogger
     */
    private $fakeLogger;

    protected function _before()
    {
        $this->fakeLogger = new FakeLogger();
        $this->errorHandlerLogger = new ErrorHandlerLogger($this->fakeLogger);
    }

    protected function _after()
    {
    }

    public function testLog()
    {
        $exception = new \Exception("Exception!", 42, new \Exception("More Exceptions!"));
        $this->errorHandlerLogger->log($exception);
        $log = $this->fakeLogger->getLog();

        $this->tester->assertNotEmpty($log);
        $this->tester->assertEquals('error', $log['level']);
        $this->tester->assertEquals([], $log['context']);

        $this->tester->assertNotEmpty($log['message']);
        $this->tester->assertTrue(is_string($log['message']));
    }
}