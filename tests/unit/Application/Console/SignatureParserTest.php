<?php

namespace Tests\unit\Application\Console;

use Papyrus\Application\Console\SignatureParser;

class SignatureSignatureParserTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testBasicParameterParsing()
    {
        $results = SignatureParser::parse('command:name');
        $this->assertEquals('command:name', $results[0]);
        $results = SignatureParser::parse('command:name {argument} {--option}');
        $this->assertEquals('command:name', $results[0]);
        $this->assertEquals('argument', $results[1][0]->getName());
        $this->assertEquals('option', $results[2][0]->getName());
        $this->assertFalse($results[2][0]->acceptValue());
        $results = SignatureParser::parse('command:name {argument*} {--option=}');
        $this->assertEquals('command:name', $results[0]);
        $this->assertEquals('argument', $results[1][0]->getName());
        $this->assertTrue($results[1][0]->isArray());
        $this->assertTrue($results[1][0]->isRequired());
        $this->assertEquals('option', $results[2][0]->getName());
        $this->assertTrue($results[2][0]->acceptValue());
        $results = SignatureParser::parse('command:name {argument?*} {--option=*}');
        $this->assertEquals('command:name', $results[0]);
        $this->assertEquals('argument', $results[1][0]->getName());
        $this->assertTrue($results[1][0]->isArray());
        $this->assertFalse($results[1][0]->isRequired());
        $this->assertEquals('option', $results[2][0]->getName());
        $this->assertTrue($results[2][0]->acceptValue());
        $this->assertTrue($results[2][0]->isArray());
        $results = SignatureParser::parse('command:name {argument?* : The argument description.}    {--option=* : The option description.}');
        $this->assertEquals('command:name', $results[0]);
        $this->assertEquals('argument', $results[1][0]->getName());
        $this->assertEquals('The argument description.', $results[1][0]->getDescription());
        $this->assertTrue($results[1][0]->isArray());
        $this->assertFalse($results[1][0]->isRequired());
        $this->assertEquals('option', $results[2][0]->getName());
        $this->assertEquals('The option description.', $results[2][0]->getDescription());
        $this->assertTrue($results[2][0]->acceptValue());
        $this->assertTrue($results[2][0]->isArray());
        $results = SignatureParser::parse('command:name
            {argument?* : The argument description.}
            {--option=* : The option description.}');
        $this->assertEquals('command:name', $results[0]);
        $this->assertEquals('argument', $results[1][0]->getName());
        $this->assertEquals('The argument description.', $results[1][0]->getDescription());
        $this->assertTrue($results[1][0]->isArray());
        $this->assertFalse($results[1][0]->isRequired());
        $this->assertEquals('option', $results[2][0]->getName());
        $this->assertEquals('The option description.', $results[2][0]->getDescription());
        $this->assertTrue($results[2][0]->acceptValue());
        $this->assertTrue($results[2][0]->isArray());
    }

    public function testShortcutNameParsing()
    {
        $results = SignatureParser::parse('command:name {--o|option}');
        $this->assertEquals('o', $results[2][0]->getShortcut());
        $this->assertEquals('option', $results[2][0]->getName());
        $this->assertFalse($results[2][0]->acceptValue());
        $results = SignatureParser::parse('command:name {--o|option=}');
        $this->assertEquals('o', $results[2][0]->getShortcut());
        $this->assertEquals('option', $results[2][0]->getName());
        $this->assertTrue($results[2][0]->acceptValue());
        $results = SignatureParser::parse('command:name {--o|option=*}');
        $this->assertEquals('command:name', $results[0]);
        $this->assertEquals('o', $results[2][0]->getShortcut());
        $this->assertEquals('option', $results[2][0]->getName());
        $this->assertTrue($results[2][0]->acceptValue());
        $this->assertTrue($results[2][0]->isArray());
        $results = SignatureParser::parse('command:name {--o|option=* : The option description.}');
        $this->assertEquals('command:name', $results[0]);
        $this->assertEquals('o', $results[2][0]->getShortcut());
        $this->assertEquals('option', $results[2][0]->getName());
        $this->assertEquals('The option description.', $results[2][0]->getDescription());
        $this->assertTrue($results[2][0]->acceptValue());
        $this->assertTrue($results[2][0]->isArray());
        $results = SignatureParser::parse('command:name
            {--o|option=* : The option description.}');
        $this->assertEquals('command:name', $results[0]);
        $this->assertEquals('o', $results[2][0]->getShortcut());
        $this->assertEquals('option', $results[2][0]->getName());
        $this->assertEquals('The option description.', $results[2][0]->getDescription());
        $this->assertTrue($results[2][0]->acceptValue());
        $this->assertTrue($results[2][0]->isArray());
    }

    public function testDefaultValueParsing()
    {
        $results = SignatureParser::parse('command:name {argument=defaultArgumentValue} {--option=defaultOptionValue}');
        $this->assertFalse($results[1][0]->isRequired());
        $this->assertEquals('defaultArgumentValue', $results[1][0]->getDefault());
        $this->assertTrue($results[2][0]->acceptValue());
        $this->assertEquals('defaultOptionValue', $results[2][0]->getDefault());
        $results = SignatureParser::parse('command:name {argument=*defaultArgumentValue1,defaultArgumentValue2} {--option=*defaultOptionValue1,defaultOptionValue2}');
        $this->assertTrue($results[1][0]->isArray());
        $this->assertFalse($results[1][0]->isRequired());
        $this->assertEquals(['defaultArgumentValue1', 'defaultArgumentValue2'], $results[1][0]->getDefault());
        $this->assertTrue($results[2][0]->acceptValue());
        $this->assertTrue($results[2][0]->isArray());
        $this->assertEquals(['defaultOptionValue1', 'defaultOptionValue2'], $results[2][0]->getDefault());
    }

    public function testArgumentDefaultValue()
    {
        $results = SignatureParser::parse('command:name {argument= : The argument description.}');
        $this->assertNull($results[1][0]->getDefault());
        $results = SignatureParser::parse('command:name {argument=default : The argument description.}');
        $this->assertSame('default', $results[1][0]->getDefault());
    }

    public function testOptionDefaultValue()
    {
        $results = SignatureParser::parse('command:name {--option= : The option description.}');
        $this->assertNull($results[2][0]->getDefault());
        $results = SignatureParser::parse('command:name {--option=default : The option description.}');
        $this->assertSame('default', $results[2][0]->getDefault());
    }
}