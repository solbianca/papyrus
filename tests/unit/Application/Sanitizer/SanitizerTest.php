<?php

namespace Tests\unit\Application\Sanitizer;

use Papyrus\Application\Sanitizer\Rules\All;
use Papyrus\Application\Sanitizer\Rules\Capitalize;
use Papyrus\Application\Sanitizer\Rules\Cast;
use Papyrus\Application\Sanitizer\Rules\Collection;
use Papyrus\Application\Sanitizer\Rules\Trim;
use Papyrus\Application\Sanitizer\Sanitizer;
use Papyrus\Application\Sanitizer\SanitizerContract;

class SanitizerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var SanitizerContract
     */
    private $sanitizer;

    protected function _before()
    {
        $this->sanitizer = new Sanitizer();
    }

    protected function _after()
    {
    }

    public function testSanitize()
    {
        $sanitized = $this->sanitizer->sanitize(
            [
                'name' => ' john  ',
                'age' => '42',
                'flag' => 1,
                'custom' => 'Hello',
                'skipped' => 'skipped attribute',
            ],
            [
                'name' => [new Trim(), new Capitalize()],
                'age' => [new Cast('int')],
                'flag' => [new Cast('bool')],
                'custom' => [
                    function (string $value) {
                        return $value . ' World!';
                    },
                ],
            ]);
        $this->tester->assertEquals('John', $sanitized['name']);
        $this->tester->assertTrue(is_integer($sanitized['age']));
        $this->tester->assertTrue(is_bool($sanitized['flag']));
        $this->tester->assertEquals('Hello World!', $sanitized['custom']);
        $this->tester->assertEquals('skipped attribute', $sanitized['skipped']);
    }

    public function testSanitizeRuleNotExist()
    {
        $this->tester->expectThrowable(\RuntimeException::class, function () {
            $this->sanitizer->sanitize(['foo' => 'bar'], ['foo' => ['nope']]);
        });
    }

    public function testSanitizeNested()
    {
        $sanitized = $this->sanitizer->sanitize([
            'foo' => [
                'bar' => [
                    '1',
                ],
                'buz' => [
                    ' 1 ',
                ],
            ],
        ], [
            'foo' => [
                new Collection([
                    'bar' =>
                        [
                            new All([
                                new Cast('integer'),
                            ]),
                        ],
                    'buz' => [
                        new All([
                            new Trim(),
                        ]),
                    ],
                ]),
            ],
        ]);

        $this->tester->assertEquals(['foo' => ['bar' => [1], 'buz' => ['1']]], $sanitized);
    }
}