<?php

namespace Tests\unit\Application\Sanitizer;

use Papyrus\Application\Sanitizer\Rules\Cast;

class CastRuleTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testApplyInteger()
    {
        $rule = new Cast('int');
        $this->tester->assertTrue(is_integer($rule->apply('1')));
        $this->tester->assertFalse(is_string($rule->apply('1')));
        $rule = new Cast('integer');
        $this->tester->assertTrue(is_integer($rule->apply('1')));
        $this->tester->assertFalse(is_string($rule->apply('1')));


        $rule = new Cast('int');
        $this->tester->assertEquals(1, $rule->apply('1'));
        $this->tester->assertEquals(1, $rule->apply(true));
        $this->tester->assertEquals(1, $rule->apply('1ab'));
        $this->tester->assertEquals(0, $rule->apply('abc'));
        $this->tester->assertEquals(0, $rule->apply([]));
        $this->tester->assertEquals(0, $rule->apply(null));

    }

    public function testApplyFloat()
    {
        $rule = new Cast('real');
        $this->tester->assertTrue(is_float($rule->apply('1.1')));
        $this->tester->assertFalse(is_string($rule->apply('1.1')));
        $rule = new Cast('float');
        $this->tester->assertTrue(is_float($rule->apply('1.1')));
        $this->tester->assertFalse(is_string($rule->apply('1.1')));
        $rule = new Cast('double');
        $this->tester->assertTrue(is_float($rule->apply('1.1')));
        $this->tester->assertFalse(is_string($rule->apply('1.1')));

        $rule = new Cast('float');
        $this->tester->assertEquals(1.1, $rule->apply('1.1'));
        $this->tester->assertEquals(1.0, $rule->apply(1));
        $this->tester->assertEquals(1.0, $rule->apply(true));
        $this->tester->assertEquals(0.0, $rule->apply([]));
        $this->tester->assertEquals(0.0, $rule->apply(null));
    }

    public function testApplyString()
    {
        $rule = new Cast('string');
        $this->tester->assertTrue(is_string($rule->apply(1)));
        $this->tester->assertFalse(is_integer($rule->apply(1)));
        $rule = new Cast('str');
        $this->tester->assertTrue(is_string($rule->apply(1)));
        $this->tester->assertFalse(is_integer($rule->apply(1)));

        $rule = new Cast('string');
        $this->tester->assertEquals('1', $rule->apply(1, ['string']));
        $this->tester->assertEquals('1.1', $rule->apply(1.1, ['string']));
        $this->tester->assertEquals('1', $rule->apply(true, ['string']));
        $this->tester->assertEquals('', $rule->apply(null, ['string']));
    }

    public function testApplyBoolean()
    {
        $rule = new Cast('bool');
        $this->tester->assertTrue(is_bool($rule->apply(1)));
        $this->tester->assertFalse(is_integer($rule->apply(1)));
        $rule = new Cast('boolean');
        $this->tester->assertTrue(is_bool($rule->apply(1)));
        $this->tester->assertFalse(is_integer($rule->apply(1)));

        $rule = new Cast('bool');
        $this->tester->assertEquals(true, $rule->apply(42));
        $this->tester->assertEquals(false, $rule->apply(0));
        $this->tester->assertEquals(false, $rule->apply(null));
        $this->tester->assertEquals(true, $rule->apply('42'));
        $this->tester->assertEquals(false, $rule->apply(''));
        $this->tester->assertEquals(false, $rule->apply('0'));
        $this->tester->assertEquals(false, $rule->apply([]));
        $this->tester->assertEquals(true, $rule->apply(1.12));
        $this->tester->assertEquals(false, $rule->apply(0.00));
    }

    public function testApplyWrongType()
    {
        $this->tester->expectThrowable(\InvalidArgumentException::class, function () {
            $rule = new Cast('nope');
            $rule->apply(1);
        });
    }
}