<?php


namespace Tests\unit\Application\Sanitizer;


use Papyrus\Application\Sanitizer\Rules\Cast;
use Papyrus\Application\Sanitizer\Rules\Collection;
use Papyrus\Application\Sanitizer\Rules\Trim;

class CollectionRuleTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {

    }

    protected function _after()
    {
    }

    public function testApply()
    {
        $rule = new Collection(['foo' => [new Trim(), new Cast('integer')]]);

        $result = $rule->apply(['foo' => ' 1 ', 'bar' => '2']);

        $this->tester->assertEquals(['foo' => 1, 'bar' => '2'], $result);

        $result = $rule->apply(' 1 ');
        $this->tester->assertEquals(' 1 ', $result);
    }
}