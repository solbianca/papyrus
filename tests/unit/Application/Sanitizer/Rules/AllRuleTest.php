<?php


namespace Tests\unit\Application\Sanitizer;


use Papyrus\Application\Sanitizer\Rules\All;
use Papyrus\Application\Sanitizer\Rules\Cast;
use Papyrus\Application\Sanitizer\Rules\Trim;

class AllRuleTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {

    }

    protected function _after()
    {
    }

    public function testApply()
    {
        $rule = new All([new Trim(), new Cast('integer')]);

        $result = $rule->apply([' 1 ', '2', ' 3', '4 ']);

        $this->tester->assertEquals([1, 2, 3, 4], $result);

        $result = $rule->apply(' 1 ');
        $this->tester->assertEquals(' 1 ', $result);
    }
}