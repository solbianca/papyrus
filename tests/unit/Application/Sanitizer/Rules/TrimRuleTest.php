<?php

namespace Tests\unit\Application\Sanitizer;

use Papyrus\Application\Sanitizer\Rules\Trim;

class TrimRuleTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testApply()
    {
        $rule = new Trim();
        $this->tester->assertEquals('foo', $rule->apply(' foo '));
        $this->tester->assertNotEquals(' foo ', $rule->apply(' foo '));
    }
}