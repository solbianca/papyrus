<?php

namespace Tests\unit\Application\Sanitizer;

use Papyrus\Application\Sanitizer\Rules\Capitalize;
use Papyrus\Contracts\Application\Sanitizer\RuleContract;

class CapitalizeRuleTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testApply()
    {
        $rule = new Capitalize();
        $this->tester->assertEquals('Foo', $rule->apply('foo'));
        $this->tester->assertNotEquals('foo', $rule->apply('foo'));
    }
}