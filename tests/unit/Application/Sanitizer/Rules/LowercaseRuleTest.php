<?php

namespace Tests\unit\Application\Sanitizer;

use Papyrus\Application\Sanitizer\Rules\Lowercase;

class LowercaseRuleTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testApply()
    {
        $rule = new Lowercase();
        $this->tester->assertEquals('foo', $rule->apply('FOO'));
        $this->tester->assertEquals('foo', $rule->apply('FoO'));
        $this->tester->assertNotEquals('FOO', $rule->apply('FOO'));
    }
}