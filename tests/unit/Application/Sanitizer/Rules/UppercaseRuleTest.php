<?php

namespace Tests\unit\Application\Sanitizer;

use Papyrus\Application\Sanitizer\Rules\Uppercase;
use Papyrus\Contracts\Application\Sanitizer\RuleContract;

class UppercaseRuleTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testApply()
    {
        $rule = new Uppercase();
        $this->tester->assertEquals('FOO', $rule->apply('foo'));
        $this->tester->assertEquals('FOO', $rule->apply('fOo'));
        $this->tester->assertNotEquals('foo', $rule->apply('foo'));
    }
}