<?php

namespace Tests\unit\Application\Twig;


use DI\Container;
use Papyrus\Application\Twig\WidgetException;
use Papyrus\Application\Twig\WidgetLoader;
use Tests\utilites\Fakes\FakeWidget;

class WidgetLoaderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var WidgetLoader
     */
    private $widgetLoader;

    protected function _before()
    {
        $this->widgetLoader = new WidgetLoader(container());
        $this->widgetLoader->setPostfix('Widget');
        $this->widgetLoader->setNamespace('Tests\utilites\Fakes');
    }

    protected function _after()
    {
    }

    public function testLoad()
    {
        $this->tester->assertInstanceOf(FakeWidget::class, $this->widgetLoader->load('Fake'));
    }

    public function testLoadFromContainer()
    {
        $container = new Container();
        $container->set(FakeWidget::class, new FakeWidget());
        $widgetLoader = new WidgetLoader($container);
        $widgetLoader->setPostfix('Widget');
        $widgetLoader->setNamespace('Tests\utilites\Fakes');

        $this->tester->assertInstanceOf(FakeWidget::class, $widgetLoader->load('Fake'));
    }

    public function testLoadClassNotExist()
    {
        $this->tester->expectThrowable(WidgetException::class, function () {
            $this->widgetLoader->load('nope');
        });
    }
}