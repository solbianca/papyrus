<?php

namespace Tests\unit\Application;

use Atlas\Orm\Atlas;
use Papyrus\Application\Writer\Writer;
use Papyrus\Persistent\Article\Article;
use Papyrus\Persistent\Article\ArticleRecord;

class WriterTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var Atlas
     */
    private $atlas;

    /**
     * @var Writer
     */
    private $writer;

    protected function _before()
    {
        $this->tester->cleanupTable('articles');

        $this->atlas = container()->get('atlas');
        $this->writer = new Writer($this->atlas);
    }

    protected function _after()
    {
        $this->tester->cleanupTable('articles');
    }

    public function testNewtRecord()
    {
        /* @var \Papyrus\Persistent\Article\ArticleRecord $record */
        $record = $this->writer->newRecord(Article::class, ['title' => 'Articles Title']);

        $this->tester->assertInstanceOf(ArticleRecord::class, $record);
        $this->tester->assertEquals('Articles Title', $record->title);
        $this->tester->assertEquals(null, $record->body);
    }

    public function testInsertRecord()
    {
        /* @var \Papyrus\Persistent\Article\ArticleRecord $record */
        $record = $this->writer->newRecord(Article::class, $this->getArticleFields());

        $this->tester->assertEquals(null, $record->id);
        $this->tester->dontSeeInDatabase('articles', ['pid' => 'qwerty']);

        $record = $this->writer->insertRecord($record);
        $this->tester->assertNotNull($record->id);
        $this->tester->assertTrue(is_numeric($record->id));
        $this->tester->seeInDatabase('articles', ['id' => $record->id, 'pid' => $record->pid]);
    }

    public function testCreate()
    {
        $this->tester->dontSeeInDatabase('articles', ['pid' => 'qwerty']);

        $record = $this->writer->create(Article::class, $this->getArticleFields());

        $this->tester->assertInstanceOf(ArticleRecord::class, $record);
        $this->tester->assertNotNull($record->id);
        $this->tester->assertTrue(is_numeric($record->id));
        $this->tester->seeInDatabase('articles', ['id' => $record->id, 'pid' => $record->pid]);
    }

    public function testUpdate()
    {
        $record = $this->writer->create(Article::class, $this->getArticleFields());
        $title = $this->tester->grabColumnFromDatabase('articles', 'title', ['id' => $record->id]);
        $this->tester->assertEquals('Articles Title', $title[0]);

        $record->title = 'New Title';
        $this->writer->update($record);
        $title = $this->tester->grabColumnFromDatabase('articles', 'title', ['id' => $record->id]);
        $this->tester->assertEquals('New Title', $title[0]);
    }

    private function getArticleFields()
    {
        return [
            'pid' => 'qwerty',
            'author' => 'John Doe',
            'page_id' => 1,
            'title' => 'Articles Title',
            'body' => 'Articles Body',
        ];
    }
}

