<?php


namespace Papyrus\Application\Validator;


class ErrorMessageTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;


    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testCreateMessage()
    {
        $message = new ErrorMessage('error', 42, 'abc', 'foo.bar');

        $this->tester->assertEquals('error', $message->getMessage());
        $this->tester->assertEquals(42, $message->getCode());
        $this->tester->assertEquals('abc', $message->getInvalidValue());
        $this->tester->assertEquals('foo.bar', $message->getPropertyPath());
    }

    public function testCreateEmptyMessage()
    {
        $message = new ErrorMessage(null, null, null, 'foo.bar');

        $this->tester->assertEquals(null, $message->getMessage());
        $this->tester->assertEquals(null, $message->getCode());
        $this->tester->assertEquals(null, $message->getInvalidValue());
        $this->tester->assertEquals('foo.bar', $message->getPropertyPath());
    }
}