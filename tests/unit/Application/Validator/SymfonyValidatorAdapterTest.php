<?php


namespace Papyrus\Application\Validator;


use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validation;

class SymfonyValidatorAdapterTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var SymfonyValidatorAdapter
     */
    private $validator;

    protected function _before()
    {
        $this->validator = new SymfonyValidatorAdapter(Validation::createValidator());
    }

    protected function _after()
    {
    }

    private function getAttributes()
    {
        return [
            'name' => 'John Doe',
            'ids' => [1, 2, 3],
        ];
    }

    private function getRules()
    {
        return [
            'name' => new NotBlank(),
            'ids' => [
                new NotBlank(),
                new Type(['type' => 'array']),
                new All([
                    new NotBlank(),
                    new Type(['type' => 'integer']),
                ]),
            ],
        ];
    }

    public function testValidate()
    {
        $validationResult = $this->validator->validate($this->getAttributes(), $this->getRules());

        $this->tester->assertTrue($validationResult->passed());
        $this->tester->assertTrue($validationResult->getErrorsBag()->isEmpty());
    }

    public function testValidateWithError()
    {
        $validationResult = $this->validator->validate([], $this->getRules());

        $this->tester->assertFalse($validationResult->passed());
        $this->tester->assertFalse($validationResult->getErrorsBag()->isEmpty());
    }

    public function testValidateOrThrow()
    {
        $validationResult = $this->validator->validateOrThrow($this->getAttributes(), $this->getRules());

        $this->tester->assertTrue($validationResult->passed());
        $this->tester->assertTrue($validationResult->getErrorsBag()->isEmpty());
    }

    public function testValidateOrThrowWithError()
    {
        $this->tester->expectThrowable(ValidationException::class, function () {
            $this->validator->validateOrThrow([], $this->getRules());
        });
    }
}