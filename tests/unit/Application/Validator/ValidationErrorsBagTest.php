<?php


namespace Papyrus\Application\Validator;

class ValidationErrorsBagTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var ValidationErrorsBag
     */
    private $bag;

    protected function _before()
    {
        $this->bag = new ValidationErrorsBag();
    }

    protected function _after()
    {
    }

    public function testAll()
    {
        $this->bag->add($this->createErrorMessage());

        $messages = $this->bag->all();

        $this->tester->assertCount(1, $messages);

        $this->bag->add($this->createErrorMessage());

        $messages = $this->bag->all();

        $this->tester->assertCount(2, $messages);
    }

    public function testIsEmpty()
    {
        $this->tester->assertTrue($this->bag->isEmpty());

        $this->bag->add($this->createErrorMessage());

        $this->tester->assertFalse($this->bag->isEmpty());
    }

    private function createErrorMessage($message = 'error', $code = 42, $invalidValue = 'abc', $path = 'foo.bar')
    {
        return new ErrorMessage($message, $code, $invalidValue, $path);
    }

    public function testCount()
    {
        $this->tester->assertEquals(0, $this->bag->count());

        $this->bag->add($this->createErrorMessage());

        $this->tester->assertEquals(1, $this->bag->count());
    }
}