<?php


namespace Papyrus\Application\Validator;


class ValidationExceptionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testErrorBag()
    {
        $exception = new ValidationException();

        $this->tester->assertEquals(null, $exception->getValidationErrors());
        $exception->setValidationErrors(new ValidationErrorsBag());
        $this->tester->assertInstanceOf(ValidationErrorsBag::class, $exception->getValidationErrors());
    }
}