<?php

namespace Tests\unit\Application\Helpers;


use Papyrus\Application\Helpers\DateTimeFormatter;

class DateTimeFormatterTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;


    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testFrom()
    {
        $this->tester->assertInstanceOf(DateTimeFormatter::class, DateTimeFormatter::from('2010-10-10 12:00:00'));
    }

    public function testToW3c()
    {
        $datetimeW3C = (new DateTimeFormatter('2010-10-10 12:00:00'))->toW3C();
        $this->tester->assertEquals('2010-10-10T12:00:00+04:00', $datetimeW3C);
    }
}