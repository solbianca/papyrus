<?php


namespace Tests\unit\Http\Responders;


use Papyrus\Application\Validator\ErrorMessage;
use Papyrus\Application\Validator\ValidationErrorsBag;
use Papyrus\Http\Responders\JsonApiErrorResponder;
use Slim\Http\Response;

class JsonApiErrorResponderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var JsonApiErrorResponder
     */
    private $responder;

    protected function _before()
    {
        $this->responder = new JsonApiErrorResponder(new Response());
    }

    protected function _after()
    {
    }

    private function createErrorBag()
    {
        $errorBag = new ValidationErrorsBag();
        $message = new ErrorMessage('Error!', 42, null, 'foo.bar');
        $errorBag->add($message);
        return $errorBag;
    }

    public function testRespond()
    {
        $response = $this->responder->respond($this->createErrorBag());

        $this->tester->assertEquals(422, $response->getStatusCode());
        $this->tester->assertEquals([
            'Content-Type' => ['application/vnd.api+json'],
            'Accept' => ['application/vnd.api+json'],
        ], $response->getHeaders());

        $body = (string)$response->getBody();
        $this->tester->assertEquals('[{"code":42,"title":"Error!","source":{"pointer":"foo.bar"}}]', $body);
    }

    public function testRespondWithoutErrors()
    {
        $response = $this->responder->respond(new ValidationErrorsBag());

        $this->tester->assertEquals(422, $response->getStatusCode());
        $this->tester->assertEquals([
            'Content-Type' => ['application/vnd.api+json'],
            'Accept' => ['application/vnd.api+json'],
        ], $response->getHeaders());

        $body = (string)$response->getBody();
        $this->tester->assertEquals('[]', $body);
    }
}