<?php

namespace Tests\unit\Http\Responders;

use Papyrus\Http\Responders\Responder;
use Slim\Http\Response;

class ResponderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var Responder
     */
    private $responder;

    protected function _before()
    {
        $this->responder = new Responder(new Response());
    }

    protected function _after()
    {
    }

    /**
     * @Skip
     */
    public function testSetContent()
    {
        $this->responder->setContent('test');
        $response = $this->responder->send();

        $this->tester->assertEquals('test', (string)$response->getBody());
    }

    public function testSetStatus()
    {
        $this->responder->setStatus(500);
        $response = $this->responder->send();

        $this->tester->assertEquals(500, $response->getStatusCode());
    }

    public function testSetHeaders()
    {
        $this->responder->setHeaders(['Content-Type' => 'application/json']);
        $response = $this->responder->send();

        $this->tester->assertEquals(['Content-Type' => ['application/json']], $response->getHeaders());

        $this->responder->setHeaders(['Accept' => 'application/json']);
        $response = $this->responder->send();

        $this->tester->assertEquals(['Accept' => ['application/json']], $response->getHeaders());
    }

    public function testAppendHeaders()
    {
        $this->responder->appendHeaders(['Content-Type' => 'application/json']);
        $response = $this->responder->send();

        $this->tester->assertEquals(['Content-Type' => ['application/json']], $response->getHeaders());

        $this->responder->appendHeaders(['Accept' => 'application/json']);
        $response = $this->responder->send();

        $this->tester->assertEquals([
            'Content-Type' => ['application/json'],
            'Accept' => ['application/json'],
        ], $response->getHeaders());
    }
}