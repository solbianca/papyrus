<?php

namespace Tests\unit\Http\Responders;

use Papyrus\Http\Responders\TwigResponder;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Response;

class TwigResponderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var TwigResponder
     */
    private $responder;

    protected function _before()
    {
        $twig = new \Slim\Views\Twig(tests() . '/_data/templates', [
            'cache' => false,
            'debug' => true,
        ]);
        $this->responder = new TwigResponder(new Response(), $twig);
    }

    protected function _after()
    {
    }

    public function testRespond()
    {
        $response = $this->responder->respond('test', ['text' => 'test']);

        $this->tester->assertEquals(200, $response->getStatusCode());
        $this->tester->assertEquals('<h1>test</h1>', (string)$response->getBody());
    }

    public function testGetResponse()
    {
        $response = $this->responder->getResponse();

        $this->tester->assertInstanceOf(ResponseInterface::class, $response);
    }
}