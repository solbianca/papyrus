<?php

namespace Tests\unit\Http\Responders;

use Papyrus\Http\Responders\JsonApiResponder;
use Slim\Http\Response;
use Tests\utilites\Fakes\DataObject;
use Tests\utilites\Fakes\FooSerializer;
use Tobscure\JsonApi\Document;

class JsonApiResponderTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var JsonApiResponder
     */
    private $responder;

    protected function _before()
    {
        $this->responder = new JsonApiResponder(new Response());
    }

    protected function _after()
    {
    }

    public function testGetDocument()
    {
        $this->tester->assertInstanceOf(Document::class, $this->responder->getDocument());
    }

    public function testInitDocument()
    {
        $document1 = $this->responder->getDocument();
        $this->responder->initDocument();
        $document2 = $this->responder->getDocument();

        $this->tester->assertNotEquals(spl_object_id($document1), spl_object_id($document2));
    }

    public function testResource()
    {
        $this->responder->setSerializer(new FooSerializer());
        $this->responder->resource(DataObject::foo(), ['bar']);

        $this->tester->assertEquals([
            'data' => [
                'type' => 'foo',
                'id' => 1,
                'attributes' => [
                    'title' => 'Title!',
                    'body' => 'Body!',
                ],
                'relationships' => [
                    'bar' => [
                        'data' => [
                            'type' => 'bar',
                            'id' => 1,
                        ],
                    ],
                ],
            ],
            'included' => [
                [
                    'type' => 'bar',
                    'id' => 1,
                    'attributes' => [
                        'title' => 'Title!',
                        'body' => 'Body!',
                    ],
                ],
            ],
        ], $this->responder->getDocument()->toArray());
    }

    public function testResourceWithoutSerializer()
    {
        $this->tester->expectThrowable(\RuntimeException::class, function () {
            $this->responder->resource(DataObject::foo(), ['bar']);
        });
    }

    public function testResourceWithoutRelationship()
    {
        $this->responder->setSerializer(new FooSerializer());
        $this->responder->resource(DataObject::foo());

        $this->tester->assertEquals([
            'data' => [
                'type' => 'foo',
                'id' => 1,
                'attributes' => [
                    'title' => 'Title!',
                    'body' => 'Body!',
                ],
            ],
        ], $this->responder->getDocument()->toArray());
    }

    public function testCollection()
    {
        $this->responder->setSerializer(new FooSerializer());
        $this->responder->collection([DataObject::foo()], ['bar']);
        $this->tester->assertEquals([
            'data' => [
                [
                    'type' => 'foo',
                    'id' => 1,
                    'attributes' => [
                        'title' => 'Title!',
                        'body' => 'Body!',
                    ],
                    'relationships' => [
                        'bar' => [
                            'data' => [
                                'type' => 'bar',
                                'id' => 1,
                            ],
                        ],
                    ],
                ],
            ],
            'included' => [
                [
                    'type' => 'bar',
                    'id' => 1,
                    'attributes' => [
                        'title' => 'Title!',
                        'body' => 'Body!',
                    ],
                ],
            ],
        ], $this->responder->getDocument()->toArray());
    }

    public function testCollectionWithoutSerializer()
    {
        $this->tester->expectThrowable(\RuntimeException::class, function () {
            $this->responder->collection([DataObject::foo()], ['bar']);
        });
    }

    public function testCollectionWithoutRelationship()
    {
        $this->responder->setSerializer(new FooSerializer());
        $this->responder->collection([DataObject::foo()]);

        $this->tester->assertEquals([
            'data' => [
                [
                    'type' => 'foo',
                    'id' => 1,
                    'attributes' => [
                        'title' => 'Title!',
                        'body' => 'Body!',
                    ],
                ],
            ],
        ], $this->responder->getDocument()->toArray());
    }

    public function testRespond()
    {
        $this->responder->setSerializer(new FooSerializer());
        $this->responder->resource(DataObject::foo());
        $response = $this->responder->respond();

        $this->tester->assertEquals(200, $response->getStatusCode());
        $this->tester->assertEquals([
            'Content-Type' => ['application/vnd.api+json'],
            'Accept' => ['application/vnd.api+json'],
        ], $response->getHeaders());

        $response = $this->responder->respond(500, ['Accept' => ['text/html'], 'X-Content' => ['text/html']]);

        $this->tester->assertEquals(500, $response->getStatusCode());
        $this->tester->assertEquals([
            'Content-Type' => ['application/vnd.api+json'],
            'Accept' => ['application/vnd.api+json'],
            'X-Content' => ['text/html'],
        ], $response->getHeaders());

        $this->tester->assertEquals('{"data":{"type":"foo","id":"1","attributes":{"title":"Title!","body":"Body!"}}}{"data":{"type":"foo","id":"1","attributes":{"title":"Title!","body":"Body!"}}}',
            (string)$response->getBody());
    }
}