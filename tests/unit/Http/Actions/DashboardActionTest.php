<?php

namespace Tests\unit\Http\Actions;

use Papyrus\Http\Actions\DashboardAction;
use Tests\utilites\Factories\TwigResponderFactory;

class DashboardActionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var DashboardAction
     */
    private $action;

    protected function _before()
    {
        $this->action = new DashboardAction(TwigResponderFactory::crate());
    }

    protected function _after()
    {
    }

    public function testHandle()
    {
        $response = $this->action->handle();

        $this->tester->assertEquals(200, $response->getStatusCode());
        $this->tester->assertEquals(['text/html; charset=UTF-8'], $response->getHeader('content-type'));
        $this->tester->assertNotEmpty((string)$response->getBody());
    }
}