<?php


namespace Tests\unit\Http\Actions\Simple\Articles;


use Papyrus\Application\Writer\Writer;
use Papyrus\Domain\Articles\ArticlesCreator;
use Papyrus\Domain\Articles\PidGenerator;
use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Http\Actions\Simple\Articles\CreateArticleAction;
use Tests\utilites\Factories\TwigResponderFactory;

class CreateArticleActionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var CreateArticleAction
     */
    private $action;

    protected function _before()
    {
        $this->action = new CreateArticleAction(
            TwigResponderFactory::crate(),
            new ArticlesCreator(new ArticlesRepository(atlas()), new Writer(atlas()), new PidGenerator())
        );
    }

    protected function _after()
    {
    }

    public function testHandle()
    {

    }
}