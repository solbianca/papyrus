<?php

namespace Tests\unit\Http\Actions\Simple\Articles;

use Papyrus\Application\Writer\Writer;
use Papyrus\Domain\Repositories\PagesRepository;
use Papyrus\Http\Actions\Simple\Articles\FormCreateArticleAction;
use Papyrus\Http\Requests\Articles\FormCreateArticleRequest;
use PHPHtmlParser\Dom;
use Tests\utilites\Factories\ProcessedRequestFactory;
use Tests\utilites\Factories\RequestFactory;
use Tests\utilites\Factories\TwigResponderFactory;
use Tests\utilites\Populators\ArticlesPopulator;
use Tests\utilites\Populators\PagesPopulator;

class FormCreateArticleActionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var FormCreateArticleAction
     */
    private $action;

    /**
     * @var Dom
     */
    private $dom;

    /**
     * @var PagesPopulator
     */
    private $pagesPopulator;

    /**
     * @var ArticlesPopulator
     */
    private $articlesPopulator;

    protected function _before()
    {
        $this->pagesPopulator = new PagesPopulator($this->tester);
        $this->articlesPopulator = new ArticlesPopulator($this->tester);

        $this->pagesPopulator->cleanup();
        $this->articlesPopulator->cleanup();

        $this->action = new FormCreateArticleAction(
            TwigResponderFactory::crate(),
            new PagesRepository(atlas()),
            new Writer(atlas())
        );

        $this->dom = new Dom();
    }

    protected function _after()
    {
        $this->pagesPopulator->cleanup();
        $this->articlesPopulator->cleanup();
    }

    public function testHandleSuccessPageNotExist()
    {
        $request = RequestFactory::create('get', '/test?author=Joe&path=/foo-bar');
        $processedRequest = ProcessedRequestFactory::create(FormCreateArticleRequest::class, $request);

        $this->tester->dontSeeInDatabase('pages', ['path' => '/foo-bar']);

        $response = $this->action->handle($processedRequest);

        $content = (string)$response->getBody();

        $this->tester->seeInDatabase('pages', ['path' => '/foo-bar']);
        $this->tester->assertEquals(200, $response->getStatusCode());

        /**
         * @var Dom\Collection|Dom\HtmlNode[] $errors
         */
        $errors = $this->dom->load($content)->find('.validation-errors ul li');
        $this->tester->assertEquals(0, $errors->count());
    }

    public function testHandleSuccessPageExist()
    {
        $this->pagesPopulator->populate();
        $request = RequestFactory::create('get', '/test?author=Joe&path=/test');
        $processedRequest = ProcessedRequestFactory::create(FormCreateArticleRequest::class, $request);

        $this->tester->seeInDatabase('pages', ['path' => '/test']);

        $response = $this->action->handle($processedRequest);

        $content = (string)$response->getBody();

        $this->tester->seeInDatabase('pages', ['path' => '/test']);
        $this->tester->assertEquals(200, $response->getStatusCode());

        /**
         * @var Dom\Collection|Dom\HtmlNode[] $errors
         */
        $errors = $this->dom->load($content)->find('.validation-errors ul li');
        $this->tester->assertEquals(0, $errors->count());
    }

    public function testHandleValidationErrors()
    {
        $processedRequest = ProcessedRequestFactory::create(FormCreateArticleRequest::class);

        $response = $this->action->handle($processedRequest);

        $content = (string)$response->getBody();

        $this->tester->assertEquals(200, $response->getStatusCode());

        /**
         * @var Dom\Collection|Dom\HtmlNode[] $errors
         */
        $errors = $this->dom->load($content)->find('.validation-errors ul li');
        $this->tester->assertNotEquals(0, $errors->count());
    }
}