<?php


namespace Tests\unit\Http\Actions\Simple\Articles;


use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Http\Actions\Simple\Articles\ShowArticleAction;
use Tests\utilites\Factories\TwigResponderFactory;
use Tests\utilites\Populators\ArticlesPopulator;

class ShowArticleActionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var ShowArticleAction
     */
    private $action;

    /**
     * @var ArticlesPopulator
     */
    private $articlesPopulator;

    protected function _before()
    {
        $this->articlesPopulator = new ArticlesPopulator($this->tester);
        $this->articlesPopulator->populate();

        $this->action = new ShowArticleAction(TwigResponderFactory::crate(), new ArticlesRepository(atlas()));
    }

    protected function _after()
    {
        $this->articlesPopulator->cleanup();
    }

    public function testHandle()
    {
        $response = $this->action->handle(1);

        $content = (string)$response->getBody();

        $this->tester->assertEquals(200, $response->getStatusCode());
        $this->tester->assertNotEmpty($content);
    }
}