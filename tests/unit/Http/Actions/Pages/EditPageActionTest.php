<?php


namespace Tests\unit\Http\Actions\Pages;


use Papyrus\Application\Sanitizer\Sanitizer;
use Papyrus\Application\Validator\SymfonyValidatorAdapter;
use Papyrus\Application\Writer\Writer;
use Papyrus\Domain\Repositories\PagesRepository;
use Papyrus\Http\Actions\Pages\EditPageAction;
use Papyrus\Http\Requests\Pages\EditPageRequest;
use Papyrus\Http\Responders\TwigResponder;
use Symfony\Component\Validator\ValidatorBuilder;
use Tests\utilites\Factories\RequestFactory;
use Tests\utilites\Populators\PagesPopulator;

class EditPageActionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var EditPageAction
     */
    private $action;

    /**
     * @var PagesPopulator
     */
    private $pagesPopulator;

    protected function _before()
    {
        $this->pagesPopulator = new PagesPopulator($this->tester);
        $this->action = new EditPageAction(
            new TwigResponder(response(), container()->get('twig')),
            new PagesRepository(atlas()),
            new Writer(atlas())
        );

        $this->pagesPopulator->cleanup();
        $this->pagesPopulator->populate();
    }

    protected function _after()
    {
        $this->pagesPopulator->cleanup();
    }

    private function createRequest(string $method)
    {
        $symfonyValidator = (new ValidatorBuilder())->getValidator();
        $validator = new SymfonyValidatorAdapter($symfonyValidator);
        $sanitizer = new Sanitizer();

        return new EditPageRequest(RequestFactory::create($method, '/test'), $validator, $sanitizer);
    }

//    public function testHandle()
//    {
//
//    }

    public function testPageNotExist()
    {
//        $this->tester->expectThrowable(NotFoundException::class, function () {
//            $this->action->handle(21312, $this->createRequest('GET'));
//        });
    }
}