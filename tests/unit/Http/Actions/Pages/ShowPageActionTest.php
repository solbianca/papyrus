<?php

namespace Tests\unit\Http\Actions\Pages;

use Papyrus\Application\Http\Exceptions\NotFoundException;
use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Domain\Repositories\PagesRepository;
use Papyrus\Http\Actions\Pages\ShowPageAction;
use Papyrus\Http\Responders\TwigResponder;
use Tests\utilites\Populators\ArticlesPopulator;
use Tests\utilites\Populators\PagesPopulator;

class ShowPageActionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var ShowPageAction
     */
    private $action;

    /**
     * @var PagesPopulator
     */
    private $pagesPopulator;

    /**
     * @var ArticlesPopulator
     */
    private $articlesPopulator;

    protected function _before()
    {
        $this->pagesPopulator = new PagesPopulator($this->tester);
        $this->articlesPopulator = new ArticlesPopulator($this->tester);
        $this->action = new ShowPageAction(
            new TwigResponder(response(), container()->get('twig')),
            new PagesRepository(container()->get('atlas')),
            new ArticlesRepository(container()->get('atlas'))
        );

        $this->articlesPopulator->cleanup();
        $this->pagesPopulator->cleanup();
        $this->articlesPopulator->populate();
        $this->pagesPopulator->populate();
    }

    protected function _after()
    {
        $this->articlesPopulator->cleanup();
        $this->pagesPopulator->cleanup();
    }

    public function testHandle()
    {
        $response = $this->action->handle(1);

        $this->tester->assertEquals(200, $response->getStatusCode());
        $this->tester->assertEquals(['text/html; charset=UTF-8'], $response->getHeader('content-type'));
        $this->tester->assertNotEmpty((string)$response->getBody());
    }

    public function testHandlePageNotExist()
    {
        $this->tester->expectThrowable(NotFoundException::class, function () {
            $this->action->handle(56787);
        });
    }
}