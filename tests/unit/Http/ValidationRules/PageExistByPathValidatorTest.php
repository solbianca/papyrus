<?php


namespace Tests\unit\Http\ValidationRules;


use Papyrus\Domain\Repositories\PagesRepository;
use Papyrus\Http\ValidationRules\PageExistByPathValidator;
use Papyrus\Http\ValidationRules\PageExistByPath;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Tests\utilites\Fakes\Validation\ExecutionContext;
use Tests\utilites\Populators\PagesPopulator;

class PageExistByPathValidatorTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var PagesPopulator
     */
    private $pagesPopulator;

    /**
     * @var PageExistByPath
     */
    private $constraint;

    /**
     * @var PageExistByPathValidator
     */
    private $validator;

    /**
     * @var ExecutionContext
     */
    private $context;

    protected function _before()
    {
        $this->constraint = new PageExistByPath();

        $this->validator = new PageExistByPathValidator();
        $this->context = new ExecutionContext();
        $this->validator->initialize($this->context);

        $this->pagesPopulator = new PagesPopulator($this->tester);
        $this->pagesPopulator->populate();
    }

    protected function _after()
    {
        $this->pagesPopulator->cleanup();
    }

    public function testValidationSuccessfully()
    {
        $this->tester->assertNull($this->validator->validate('/test', $this->constraint));
        $violations = $this->context->getViolations();
        $this->assertEmpty($violations);
    }

    public function testValidateInvalidPath()
    {
        $this->tester->assertNull($this->validator->validate('/nope/nope/nope/', $this->constraint));
        $violations = $this->context->getViolations();
        $this->tester->assertCount(1, $violations);
        $this->tester->assertEquals('Страница "{{ path }}" не существует', $violations[0]['message']);
        $this->tester->assertEquals('/nope/nope/nope/', $violations[0]['params']['{{ path }}']);
    }

    public function testValidateWithEmptyValue()
    {
        $this->tester->assertNull($this->validator->validate('', $this->constraint));
        $violations = $this->context->getViolations();
        $this->assertEmpty($violations);
    }

    public function testValidateWithNotStringValue()
    {
        $this->tester->expectThrowable(UnexpectedValueException::class, function () {
            $this->validator->validate(42, $this->constraint);
        });
    }
}