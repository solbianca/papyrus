<?php

namespace Tests\unit\Http\Serializers;

use Papyrus\Http\Serializers\ArticleSerializer;
use Tests\utilites\Populators\ArticlesPopulator;

class ArticleSerializerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var ArticlesPopulator
     */
    private $articlePopulator;

    protected function _before()
    {
        $this->articlePopulator = new ArticlesPopulator($this->tester);
    }

    protected function _after()
    {
    }

    public function testGetId()
    {
        $serializer = new ArticleSerializer();
        $article = (object)$this->articlePopulator->makeArticlesData()[0];

        $this->tester->assertEquals('pid_1', $serializer->getId($article));
    }

    public function testGetType()
    {
        $serializer = new ArticleSerializer();
        $article = (object)$this->articlePopulator->makeArticlesData()[0];

        $this->tester->assertEquals('article', $serializer->getType($article));
    }

    public function testGetAttributes()
    {
        $serializer = new ArticleSerializer();
        $article = (object)$this->articlePopulator->makeArticlesData()[0];

        $this->tester->assertEquals([
            'author' => 'John Doe',
            'page_id' => 1,
            'version' => 1,
            'weight' => 100,
            'status' => 'published',
            'title' => 'Articles Title',
            'body' => 'Articles Body',
            'created_at' => '2010-10-10T12:00:00+04:00',
            'updated_at' => '2010-10-10T12:00:00+04:00',
        ], $serializer->getAttributes($article));
    }
}