<?php

define('ROOT', dirname(dirname(__DIR__)));

require_once(__DIR__ . '/../../vendor/autoload.php');

(new Dotenv\Dotenv(ROOT, '.env.testing'))->load();

$app = \Papyrus\Application\Kernel\Kernel::init()->getApplication();

// Set up dependencies
require __DIR__ . '/../../bootstrap/services.php';
// Register middleware
require __DIR__ . '/../../bootstrap/middleware.php';