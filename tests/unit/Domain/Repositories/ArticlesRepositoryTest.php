<?php

namespace Tests\unit\Application\Domain\Repositories;

use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Persistent\Article\ArticleRecord;
use Tests\utilites\Populators\ArticlesPopulator;
use Tests\utilites\Populators\PagesPopulator;

class ArticlesRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var ArticlesRepository
     */
    private $articlesRepository;


    /**
     * @var ArticlesPopulator
     */
    private $articlesPopulator;

    /**
     * @var PagesPopulator
     */
    private $pagesPopulator;

    protected function _before()
    {
        $this->articlesPopulator = new ArticlesPopulator($this->tester);
        $this->pagesPopulator = new PagesPopulator($this->tester);

        $this->cleanup();
        $this->populate();

        $this->articlesRepository = new ArticlesRepository(container()->get('atlas'));
    }

    protected function _after()
    {
        $this->cleanup();
    }

    private function populate()
    {
        $this->articlesPopulator->populate();
        $this->pagesPopulator->populate();
    }

    private function cleanup()
    {
        $this->articlesPopulator->cleanup();
        $this->pagesPopulator->cleanup();
    }

    public function testFindLastPublishedByPid()
    {
        $articleRecord = $this->articlesRepository->findLastPublishedByPid('pid_1');

        $this->tester->assertInstanceOf(ArticleRecord::class, $articleRecord);
        $this->tester->assertEquals(3, $articleRecord->id);
        $this->tester->assertNull($articleRecord->page);
    }

    public function testFindLastByPid()
    {
        $articleRecord = $this->articlesRepository->findLastByPid('pid_1');

        $this->tester->assertInstanceOf(ArticleRecord::class, $articleRecord);
        $this->tester->assertEquals(5, $articleRecord->id);
        $this->tester->assertNull($articleRecord->page);
    }

    public function testFindLastByPidWithRelationships()
    {
        $articleRecord = $this->articlesRepository->findLastPublishedByPid('pid_1');

        $this->tester->assertInstanceOf(ArticleRecord::class, $articleRecord);
        $this->tester->assertEquals(3, $articleRecord->id);
    }

    public function testFindLastByPidNotFound()
    {
        $articleRecord = $this->articlesRepository->findLastPublishedByPid('nope');

        $this->tester->assertNull($articleRecord);
    }

    public function testFindForListing()
    {
        $articlesSet = $this->articlesRepository->findForListing(1);

        $this->tester->assertEquals(2, $articlesSet->count());
        $this->tester->assertEquals('pid_1', $articlesSet[0]->pid);
        $this->tester->assertEquals(200, $articlesSet[0]->weight);
        $this->tester->assertEquals('pid_2', $articlesSet[1]->pid);
        $this->tester->assertEquals(100, $articlesSet[1]->weight);
    }

    public function testFindForAdminListing()
    {
        $articlesSet = $this->articlesRepository->findForAdminListing(1);

        $this->tester->assertEquals(2, $articlesSet->count());
        $this->tester->assertEquals('pid_1', $articlesSet[0]->pid);
        $this->tester->assertEquals(200, $articlesSet[0]->weight);
        $this->tester->assertEquals('pid_2', $articlesSet[1]->pid);
        $this->tester->assertEquals(100, $articlesSet[1]->weight);
    }

    /**
     * @skip
     */
    public function testFindAllPublishedAndActiveByPid()
    {
        $articlesSet = $this->articlesRepository->findAllPublishedAndActiveByPid('pid_1');

        $this->tester->assertEquals(2, $articlesSet->count());
        $this->tester->assertEquals(3, $articlesSet[0]->id);
        $this->tester->assertEquals('pid_1', $articlesSet[0]->pid);
        $this->tester->assertEquals(2, $articlesSet[0]->version);
        $this->tester->assertEquals(1, $articlesSet[1]->id);
        $this->tester->assertEquals('pid_1', $articlesSet[1]->pid);
        $this->tester->assertEquals(1, $articlesSet[1]->version);
    }
}