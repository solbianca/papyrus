<?php

namespace Tests\unit\Application\Domain\Repositories;

use Papyrus\Domain\Repositories\PagesRepository;
use Papyrus\Persistent\Page\PageRecord;
use Tests\utilites\Populators\ArticlesPopulator;
use Tests\utilites\Populators\HostsPopulator;
use Tests\utilites\Populators\PagesPopulator;

class PageRepositoryTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var PagesRepository;
     */
    private $pagesRepository;

    /**
     * @var PagesPopulator
     */
    private $pagesPopulator;

    protected function _before()
    {
        $this->pagesPopulator = new PagesPopulator($this->tester);

        $this->cleanup();
        $this->populate();

        $this->pagesRepository = new PagesRepository(container()->get('atlas'));


    }

    protected function _after()
    {
        $this->cleanup();
    }

    private function populate()
    {
        $this->pagesPopulator->populate();
    }

    private function cleanup()
    {
        $this->pagesPopulator->cleanup();
    }

    public function testFindById()
    {
        /* @var \Papyrus\Persistent\Page\PageRecord $pageRecord */
        $pageRecord = $this->pagesRepository->findById(1);

        $this->tester->assertInstanceOf(PageRecord::class, $pageRecord);
        $this->tester->assertEquals(1, $pageRecord->id);
        $this->tester->assertEquals('Page Title', $pageRecord->title);
        $this->tester->assertEquals('/test', $pageRecord->path);
        $this->tester->assertEquals('active', $pageRecord->status);
        $this->tester->assertEquals('2010-10-10 12:00:00', $pageRecord->created_at);
        $this->tester->assertEquals('2010-10-10 12:00:00', $pageRecord->updated_at);
    }

    public function testIsExistByPath()
    {
        $this->tester->assertTrue($this->pagesRepository->isExistByPath('/test'));
    }
}