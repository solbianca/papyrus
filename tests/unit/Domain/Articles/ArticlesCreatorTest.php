<?php


namespace Tests\unit\Domain\Articles;


use Papyrus\Application\Hydrator\Hydrator;
use Papyrus\Application\Writer\Writer;
use Papyrus\Domain\Articles\ArticlesCreator;
use Papyrus\Domain\Articles\Dictionaries\ArticleStatusDictionary;
use Papyrus\Domain\Articles\PidGenerator;
use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Http\RequestsValues\Articles\ArticleRequestValue;
use Papyrus\Persistent\Article\ArticleRecord;
use Tests\utilites\Populators\ArticlesPopulator;

class ArticlesCreatorTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var ArticlesPopulator
     */
    private $articlesPopulator;

    /**
     * @var ArticlesCreator
     */
    private $creator;

    protected function _before()
    {
        $this->articlesPopulator = new ArticlesPopulator($this->tester);

        $articlesRepository = new ArticlesRepository(atlas());
        $writer = new Writer(atlas());
        $this->creator = new ArticlesCreator($articlesRepository, $writer, new PidGenerator());
    }

    protected function _after()
    {
        $this->articlesPopulator->cleanup();
    }

    private function createRequestValue(string $status = ArticleStatusDictionary::PUBLISHED): ArticleRequestValue
    {
        return (new Hydrator())->hydrate(ArticleRequestValue::class, [
            'author' => 'Sol',
            'page_id' => 42,
            'title' => 'Test Title',
            'body' => 'Test Body',
            'status' => $status
        ]);
    }

    public function testCreateArticle()
    {
        $this->tester->dontSeeInDatabase('articles', [
            'page_id' => 42,
            'author' => 'Sol',
        ]);

        $articleRecord = $this->creator->create($this->createRequestValue());
        $this->tester->seeInDatabase('articles', [
            'page_id' => 42,
            'author' => 'Sol',
            'version' => 1,
            'weight' => 100,
        ]);

        $this->tester->seeInDatabase('articles', ['id' => $articleRecord->id]);
    }

    /**
     * @skip
     */
    public function testCreateArticleAndIncrementVersion()
    {

    }
}