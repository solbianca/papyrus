<?php


namespace Tests\unit\Domain\Articles;


use Papyrus\Application\Writer\Writer;
use Papyrus\Domain\Articles\ArticlesUpdater;
use Papyrus\Domain\Repositories\ArticlesRepository;
use Tests\utilites\Populators\ArticlesPopulator;

class ArticlesUpdaterTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var ArticlesPopulator
     */
    private $articlesPopulator;

    /**
     * @var ArticlesUpdater
     */
    private $updater;

    protected function _before()
    {
        $this->articlesPopulator = new ArticlesPopulator($this->tester);

        $articlesRepository = new ArticlesRepository(atlas());
        $writer = new Writer(atlas());
        $this->updater = new ArticlesUpdater($articlesRepository, $writer);
    }

    protected function _after()
    {
        $this->articlesPopulator->cleanup();
    }

}