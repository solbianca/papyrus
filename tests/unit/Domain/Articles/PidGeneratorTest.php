<?php

namespace Tests\unit\Application\Domain\Articles;

class PidGeneratorTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var \Papyrus\Domain\Articles\PidGenerator
     */
    private $pidGenerator;

    protected function _before()
    {
        $this->pidGenerator = new \Papyrus\Domain\Articles\PidGenerator();
    }

    protected function _after()
    {
    }

    public function testGenerate()
    {
        $pid = $this->pidGenerator->generate();

        $this->tester->assertTrue(is_string($pid));
        $this->tester->assertEquals(16, strlen($pid));
    }
}