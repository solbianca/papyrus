<?php


namespace Tests\unit\Domain\Articles;


use Papyrus\Domain\Articles\ListingFetcher;
use Papyrus\Domain\Repositories\ArticlesRepository;
use Tests\utilites\Populators\ArticlesPopulator;

class ListingFetcherTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var ArticlesPopulator
     */
    private $articlesPopulator;

    /**
     * @var ListingFetcher
     */
    private $listingFetcher;

    protected function _before()
    {
        $this->articlesPopulator = new ArticlesPopulator($this->tester);
        $this->articlesPopulator->populate();

        $this->listingFetcher = new ListingFetcher(new ArticlesRepository(atlas()));
    }

    protected function _after()
    {
        $this->articlesPopulator->cleanup();
    }

    public function testFetchWithAuthor()
    {
        $articlesSet = $this->listingFetcher->fetch(1, 'John Doe');

        $this->tester->assertEquals(2, $articlesSet->count());

        /* @var \Papyrus\Persistent\Article\ArticleRecord $articleRecord */
        $articleRecord = $articlesSet[0];

        $this->tester->assertEquals('pid_1', $articleRecord->pid);
        $this->tester->assertEquals(1, $articleRecord->page_id);
        $this->tester->assertEquals('published', $articleRecord->status);
        $this->tester->assertEquals('John Doe', $articleRecord->author);

        /* @var \Papyrus\Persistent\Article\ArticleRecord $articleRecord */
        $articleRecord = $articlesSet[1];
        $this->tester->assertEquals('pid_2', $articleRecord->pid);
        $this->tester->assertEquals(1, $articleRecord->page_id);
        $this->tester->assertEquals('published', $articleRecord->status);
        $this->tester->assertEquals('John Doe', $articleRecord->author);

        $draftRecord = $articleRecord->draft;
        $this->tester->assertNull($draftRecord);
    }

    public function testFetchWithoutAuthor()
    {
        $articlesSet = $this->listingFetcher->fetch(1);

        $this->tester->assertEquals(2, $articlesSet->count());

        /* @var \Papyrus\Persistent\Article\ArticleRecord $articleRecord */
        $articleRecord = $articlesSet[0];

        $this->tester->assertEquals('pid_1', $articleRecord->pid);
        $this->tester->assertEquals(1, $articleRecord->page_id);
        $this->tester->assertEquals('published', $articleRecord->status);
        $this->tester->assertEquals('John Doe', $articleRecord->author);

        /* @var \Papyrus\Persistent\Article\ArticleRecord $articleRecord */
        $articleRecord = $articlesSet[1];
        $this->tester->assertEquals('pid_2', $articleRecord->pid);
        $this->tester->assertEquals(1, $articleRecord->page_id);
        $this->tester->assertEquals('published', $articleRecord->status);
        $this->tester->assertEquals('John Doe', $articleRecord->author);

        $draftRecord = $articleRecord->draft;
        $this->tester->assertNull($draftRecord);
    }

    public function testFetchWithoutArticles()
    {
        $articlesSet = $this->listingFetcher->fetch(346323);

        $this->tester->assertEquals(0, $articlesSet->count());
    }
}