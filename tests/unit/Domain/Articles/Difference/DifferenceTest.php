<?php


namespace Tests\unit\Domain\Articles\Difference;


use Papyrus\Domain\Articles\Difference\Difference;
use Papyrus\Domain\Articles\Difference\DifferenceException;
use Papyrus\Domain\Articles\Difference\DifferencePayload;
use Papyrus\Domain\Repositories\ArticlesRepository;
use Papyrus\Persistent\Article\ArticleRecord;
use Tests\utilites\Populators\ArticlesPopulator;

class DifferenceTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var Difference
     */
    private $difference;

    /**
     * @var ArticlesPopulator
     */
    private $articlesPopulator;

    protected function _before()
    {
        $this->articlesPopulator = new ArticlesPopulator($this->tester);
        $this->articlesPopulator->populate();

        $this->difference = new Difference(new ArticlesRepository(atlas()));
    }

    protected function _after()
    {
        $this->articlesPopulator->cleanup();
    }

    public function testHandleSuccessfully()
    {
        $differencePayload = $this->difference->handle(1, 5);

        $this->tester->assertInstanceOf(DifferencePayload::class, $differencePayload);
        $this->tester->assertInstanceOf(ArticleRecord::class, $differencePayload->getOld());
        $this->tester->assertInstanceOf(ArticleRecord::class, $differencePayload->getNew());

        $this->tester->assertNotEmpty($differencePayload->getTitle());
        $this->tester->assertNotEmpty($differencePayload->getBody());
    }

    public function testHandleOldArticleNotExist()
    {
        $this->tester->expectThrowable(DifferenceException::class, function () {
            $this->difference->handle(546544643, 5);
        });
    }

    public function testHandleNewArticleNotExist()
    {
        $this->tester->expectThrowable(DifferenceException::class, function () {
            $this->difference->handle(1, 8347508324);
        });
    }
}