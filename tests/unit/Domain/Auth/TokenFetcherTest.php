<?php

namespace Tests\unit\Domain\Auth;

use Papyrus\Domain\Auth\TokenFetcher;
use Tests\utilites\Factories\RequestFactory;

class TokenFetcherTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    private $token = 'qwerty';

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    private function createFetcher(string $target, string $key): TokenFetcher
    {
        return new TokenFetcher($target, $key);
    }

    private function createRequest(array $headers = [], array $cookies = [], array $serverParams = [])
    {
        return RequestFactory::create('get', '/', $headers, $cookies, $serverParams);
    }

    public function testFetchFromEnvironment()
    {
        $fetcher = $this->createFetcher('environment', 'HTTP_AUTHORIZATION');
        $token = $fetcher->fetch($this->createRequest([], [], ['HTTP_AUTHORIZATION' => $this->token]));

        $this->tester->assertEquals($this->token, $token);

        $fetcher = $this->createFetcher('environment', 'http_authorization');
        $token = $fetcher->fetch($this->createRequest([], [], ['HTTP_AUTHORIZATION' => $this->token]));

        $this->tester->assertEquals($this->token, $token);

        $fetcher = $this->createFetcher('environment', 'http_authorization');
        $token = $fetcher->fetch($this->createRequest([], [], []));

        $this->tester->assertEquals('', $token);

        $fetcher = $this->createFetcher('environment', 'HTTP_X_AUTHORIZATION');
        $token = $fetcher->fetch($this->createRequest([], [], ['HTTP_AUTHORIZATION' => $this->token]));

        $this->tester->assertEquals('', $token);

        $fetcher = $this->createFetcher('environment', 'HTTP_AUTHORIZATION');
        $token = $fetcher->fetch($this->createRequest([], [], ['HTTP_X_AUTHORIZATION' => $this->token]));

        $this->tester->assertEquals('', $token);
    }

    public function testFetchFromHeader()
    {
        $fetcher = $this->createFetcher('header', 'X-Auth');
        $token = $fetcher->fetch($this->createRequest(['X-Auth' => $this->token], [], []));

        $this->tester->assertEquals($this->token, $token);

        $fetcher = $this->createFetcher('header', 'X-Auth');
        $token = $fetcher->fetch($this->createRequest([], [], []));

        $this->tester->assertEquals('', $token);

        $fetcher = $this->createFetcher('header', 'X-Auth');
        $token = $fetcher->fetch($this->createRequest(['Auth' => $this->token], [], []));

        $this->tester->assertEquals('', $token);

        $fetcher = $this->createFetcher('header', 'Auth');
        $token = $fetcher->fetch($this->createRequest(['X-Auth' => $this->token], [], []));

        $this->tester->assertEquals('', $token);
    }

    public function testFetchFromCookie()
    {
        $fetcher = $this->createFetcher('cookie', 'X-Auth');
        $token = $fetcher->fetch($this->createRequest([], ['X-Auth' => $this->token], []));

        $this->tester->assertEquals($this->token, $token);

        $fetcher = $this->createFetcher('cookie', 'X-Auth');
        $token = $fetcher->fetch($this->createRequest([], [], []));

        $this->tester->assertEquals('', $token);

        $fetcher = $this->createFetcher('cookie', 'X-Auth');
        $token = $fetcher->fetch($this->createRequest([], ['Auth' => $this->token], []));

        $this->tester->assertEquals('', $token);

        $fetcher = $this->createFetcher('cookie', 'Auth');
        $token = $fetcher->fetch($this->createRequest([], ['X-Auth' => $this->token], []));

        $this->tester->assertEquals('', $token);
    }

    public function testFetchFromInvalidTarget()
    {
        $fetcher = $this->createFetcher('foo', 'bar');
        $token = $fetcher->fetch($this->createRequest());

        $this->tester->assertEquals('', $token);
    }
}