<?php


namespace Tests\unit\Domain\Auth;


use Papyrus\Domain\Auth\Authenticator;
use Papyrus\Domain\Auth\SessionManager;
use Papyrus\Domain\Repositories\UsersRepository;
use Tests\utilites\Populators\UsersPopulator;

class AuthenticatorTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var Authenticator
     */
    private $authenticator;

    /**
     * @var UsersPopulator
     */
    private $userPopulator;

    protected function _before()
    {
        $this->userPopulator = new UsersPopulator($this->tester);
        $this->userPopulator->cleanup();
        $this->userPopulator->populate();

        $this->authenticator = new Authenticator(new SessionManager(), new UsersRepository(container()->get('atlas')));
    }

    protected function _after()
    {
        $this->userPopulator->cleanup();
    }

    public function testAuthenticate()
    {

    }
}