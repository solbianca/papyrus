<?php


namespace Tests\unit\Domain\Auth;

use Atlas\Mapper\Related;
use Papyrus\Domain\Auth\Identity;
use Papyrus\Persistent\User\UserRecord;
use Papyrus\Persistent\User\UserRow;

class IdentityTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var Identity
     */
    private $emptyIdentity;

    /**
     * @var Identity
     */
    private $userIdentity;

    protected function _before()
    {
        $this->emptyIdentity = $this->createEmptyIdentity();
        $this->userIdentity = $this->createUserIdentity();
    }

    protected function _after()
    {
    }

    private function createEmptyIdentity()
    {
        return new Identity();
    }

    private function createUserIdentity()
    {
        $row = new UserRow(['id' => 42]);
        $related = new Related();
        $userRecord = new UserRecord($row, $related);
        return new Identity($userRecord);
    }

    public function testGetId()
    {
        $this->tester->assertNull($this->emptyIdentity->getId());
        $this->tester->assertEquals(42, $this->userIdentity->getId());
    }

    public function testIsGuest()
    {
        $this->tester->assertTrue($this->emptyIdentity->isGuest());
        $this->tester->assertFalse($this->userIdentity->isGuest());
    }

    public function testGetUser()
    {
        $this->tester->assertNull($this->emptyIdentity->getUser());

        $this->tester->assertInstanceOf(UserRecord::class, $this->userIdentity->getUser());
        $this->tester->assertEquals(42, $this->userIdentity->getUser()->id);
    }
}