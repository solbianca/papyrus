<?php


namespace Tests\unit\Domain\Auth;


use Papyrus\Domain\Auth\SessionManager;

class SessionManagerTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var SessionManager;
     */
    private $sessionManager;

    protected function _before()
    {
        $this->sessionManager = new SessionManager();
    }

    protected function _after()
    {
    }

    public function testSetAndGet()
    {
        $this->tester->assertNull($this->sessionManager->getUserData('qwerty'));

        $this->sessionManager->setUserData('qwerty', ['id' => 42]);

        $this->tester->assertEquals(['id' => 42], $this->sessionManager->getUserData('qwerty'));
    }
}