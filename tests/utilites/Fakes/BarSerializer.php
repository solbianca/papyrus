<?php


namespace Tests\utilites\Fakes;


use Tobscure\JsonApi\AbstractSerializer;

class BarSerializer extends AbstractSerializer
{
    protected $type = 'bar';

    /**
     * @param object $data
     * @param array|null $fields
     * @return array
     */
    public function getAttributes($data, array $fields = null)
    {
        return [
            'title' => $data->title,
            'body' => $data->body,
        ];
    }
}