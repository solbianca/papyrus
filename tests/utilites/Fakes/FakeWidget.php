<?php


namespace Tests\utilites\Fakes;


use Papyrus\Application\Twig\WidgetContract;

class FakeWidget implements WidgetContract
{
    public function render(): string
    {
        return 'test';
    }

}