<?php


namespace Tests\utilites\Fakes;


use Psr\Container\ContainerInterface;

class Container implements ContainerInterface
{
    /**
     * @var array
     */
    private $container = [];

    public function get($id)
    {
        if ($this->has($id)) {
            return $this->container[$id];
        }
        throw new \InvalidArgumentException();
    }

    public function has($id)
    {
        return key_exists($id, $this->container);
    }

    public function set($id, $value)
    {
        $this->container[$id] = $value;
    }
}