<?php


namespace Tests\utilites\Fakes;


class DataObject
{
    public static function foo()
    {
        $foo = new \stdClass();
        $foo->id = 1;
        $foo->title = 'Title!';
        $foo->body = 'Body!';
        $foo->bar = static::bar();
        return $foo;
    }

    public static function bar()
    {
        $bar = new \stdClass();
        $bar->id = 1;
        $bar->title = 'Title!';
        $bar->body = 'Body!';
        return $bar;
    }
}