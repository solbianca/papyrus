<?php


namespace Tests\utilites\Fakes;


use Papyrus\Application\Repositories\Repository;
use Papyrus\Persistent\Page\Page;

class FakeRepository extends Repository
{
    protected $mapper = Page::class;
}
