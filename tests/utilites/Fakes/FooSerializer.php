<?php


namespace Tests\utilites\Fakes;


use Tobscure\JsonApi\AbstractSerializer;
use Tobscure\JsonApi\Relationship;
use Tobscure\JsonApi\Resource;

class FooSerializer extends AbstractSerializer
{
    protected $type = 'foo';

    /**
     * @param object $data
     * @param array|null $fields
     * @return array
     */
    public function getAttributes($data, array $fields = null)
    {
        return [
            'title' => $data->title,
            'body' => $data->body,
        ];
    }

    public function bar($foo)
    {
        $bar = new Resource($foo->bar, new BarSerializer());

        return new Relationship($bar);
    }
}