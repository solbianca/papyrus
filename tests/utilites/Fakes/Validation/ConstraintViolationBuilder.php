<?php


namespace Tests\utilites\Fakes\Validation;


use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

class ConstraintViolationBuilder implements ConstraintViolationBuilderInterface
{
    /**
     * @var ExecutionContext
     */
    private $context;

    /**
     * @var string
     */
    private $message;

    /**
     * @var array
     */
    private $parameters;

    public function __construct(string $message, array $parameters)
    {
        $this->message = $message;
        $this->parameters = $parameters;
    }

    public function setContext(ExecutionContext $context)
    {
        $this->context = $context;
    }

    public function atPath($path)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function setParameter($key, $value)
    {
        $this->parameters[$key] = $value;
        return $this;
    }

    public function setParameters(array $parameters)
    {
    }

    public function setTranslationDomain($translationDomain)
    {
    }

    public function setInvalidValue($invalidValue)
    {
    }

    public function setPlural($number)
    {
    }

    public function setCode($code)
    {
    }

    public function setCause($cause)
    {
    }

    public function addViolation()
    {
        $this->context->addViolation($this->message, $this->parameters);

        return ['message' => $this->message, 'parameters' => $this->parameters];
    }

}