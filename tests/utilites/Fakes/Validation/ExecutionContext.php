<?php


namespace Tests\utilites\Fakes\Validation;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\MetadataInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;
use Tests\utilites\Fakes\Validation\ConstraintViolationBuilder;

class ExecutionContext implements ExecutionContextInterface
{
    /**
     * @var array
     */
    private $violations;

    public function addViolation($message, array $params = [])
    {
        $this->violations[] = [
            'message' => $message,
            'params' => $params
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildViolation($message, array $parameters = [])
    {
        $builder = new ConstraintViolationBuilder($message, $parameters);
        $builder->setContext($this);
        return $builder;
    }

    public function getValidator()
    {
    }

    public function getObject()
    {
    }

    public function setNode($value, $object, MetadataInterface $metadata = null, $propertyPath)
    {
    }

    public function setGroup($group)
    {
    }

    public function setConstraint(Constraint $constraint)
    {
    }

    public function markGroupAsValidated($cacheKey, $groupHash)
    {
    }

    public function isGroupValidated($cacheKey, $groupHash)
    {
    }

    public function markConstraintAsValidated($cacheKey, $constraintHash)
    {
    }

    public function isConstraintValidated($cacheKey, $constraintHash)
    {
    }

    public function markObjectAsInitialized($cacheKey)
    {
    }

    public function isObjectInitialized($cacheKey)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getViolations()
    {
        return $this->violations;
    }

    public function getRoot()
    {
    }

    public function getValue()
    {
    }

    public function getMetadata()
    {
    }

    public function getGroup()
    {
    }

    public function getClassName()
    {
    }

    public function getPropertyName()
    {
    }

    public function getPropertyPath($subPath = '')
    {
    }

}