<?php


namespace Tests\utilites\Factories;

use Slim\Http\Response;

class ResponseFactory
{
    public static function create(int $statusCode = 200): Response
    {
        return new Response($statusCode);
    }
}