<?php


namespace Tests\utilites\Factories;


use Papyrus\Application\Http\ProcessedRequestContract;
use Papyrus\Application\Sanitizer\Sanitizer;
use Papyrus\Application\Validator\SymfonyValidatorAdapter;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Validator\ValidatorBuilder;

class ProcessedRequestFactory
{
    public static function create(
        string $processedRequest,
        ServerRequestInterface $request = null
    ): ProcessedRequestContract {
        if (!is_a($processedRequest, ProcessedRequestContract::class, true)) {
            throw new \Exception("Parameter must implement `ProcessedRequestContract`");
        }

        if (!$request) {
            $request = RequestFactory::create('GET', '/test');
        }

        $symfonyValidator = (new ValidatorBuilder())->getValidator();
        $validator = new SymfonyValidatorAdapter($symfonyValidator);
        $sanitizer = new Sanitizer();

        return new $processedRequest($request, $validator, $sanitizer);
    }
}