<?php


namespace Tests\utilites\Factories;

use Papyrus\Http\Responders\TwigResponder;

class TwigResponderFactory
{
    public static function crate(): TwigResponder
    {
        return new TwigResponder(ResponseFactory::create(), container()->get('twig'));
    }
}