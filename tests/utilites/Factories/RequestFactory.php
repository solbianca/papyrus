<?php

namespace Tests\utilites\Factories;


use Slim\Http\Headers;
use Slim\Http\Request;
use Slim\Http\RequestBody;
use Slim\Http\Uri;

class RequestFactory
{
    public static function create(
        string $method,
        string $uri,
        array $headers = [],
        array $cookies = [],
        array $serverParams = [],
        string $bodyContent = '',
        array $uploadFiles = []
    ): Request {
        $uri = Uri::createFromString($uri);
        $requestHeaders = new Headers();

        foreach ($headers as $key => $value) {
            $requestHeaders->add($key, $value);
        }

        $body = new RequestBody();
        $body->write($bodyContent);

        return new Request($method, $uri, $requestHeaders, $cookies, $serverParams, $body, $uploadFiles);
    }
}