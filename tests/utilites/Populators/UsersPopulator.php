<?php


namespace Tests\utilites\Populators;


class UsersPopulator extends Populator
{
    public function populate()
    {
        $this->tester->populateTable('users', $this->makeUsersData());
    }

    public function cleanup()
    {
        $this->tester->cleanupTable('users');
    }

    public function makeUsersData()
    {
        return [
            $this->makeUserData(1),
        ];
    }

    public function makeUserData(int $id)
    {
        return [
            'id' => $id,
            'username' => "johndoe_{$id}",
            'email' => "john_{$id}@example.com",
            'password' => "secret_{$id}",
            'status' => 'active',
            'created_at' => '2010-10-10 12:00:00',
            'updated_at' => '2010-10-10 12:00:00',
        ];
    }
}