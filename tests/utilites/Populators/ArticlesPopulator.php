<?php


namespace Tests\utilites\Populators;


class ArticlesPopulator extends Populator
{
    public function populate()
    {
        $this->populateArticles();
    }

    public function cleanup()
    {
        $this->tester->cleanupTable('articles');
    }

    private function populateArticles()
    {
        $this->tester->populateTable('articles', $this->makeArticlesData());
    }

    public function makeArticlesData()
    {
        return [
            [
                'id' => 1,
                'pid' => 'pid_1',
                'author' => 'John Doe',
                'page_id' => 1,
                'version' => 1,
                'weight' => 100,
                'status' => 'published',
                'type' => 'article',
                'title' => 'Articles Title',
                'body' => 'Articles Body',
                'created_at' => '2010-10-10 12:00:00',
                'updated_at' => '2010-10-10 12:00:00',
            ],
            [
                'id' => 2,
                'pid' => 'pid_2',
                'author' => 'John Doe',
                'page_id' => 1,
                'version' => 1,
                'weight' => 100,
                'status' => 'published',
                'type' => 'article',
                'title' => 'Articles Title',
                'body' => 'Articles Body',
                'created_at' => '2010-10-10 12:00:00',
                'updated_at' => '2010-10-10 12:00:00',
            ],
            [
                'id' => 3,
                'pid' => 'pid_1',
                'author' => 'John Doe',
                'page_id' => 1,
                'version' => 2,
                'weight' => 200,
                'status' => 'published',
                'type' => 'article',
                'title' => 'Articles Title',
                'body' => 'Articles Body',
                'created_at' => '2010-10-10 12:00:00',
                'updated_at' => '2010-10-10 12:00:00',
            ],
            [
                'id' => 4,
                'pid' => 'pid_1',
                'author' => 'John Doe',
                'page_id' => 1,
                'version' => 0,
                'weight' => 200,
                'status' => 'published',
                'type' => 'draft',
                'title' => 'Articles Title Draft',
                'body' => 'Articles Body Draft   ',
                'created_at' => '2010-10-10 12:00:00',
                'updated_at' => '2010-10-10 12:00:00',
            ],
            [
                'id' => 5,
                'pid' => 'pid_1',
                'author' => 'John Doe',
                'page_id' => 1,
                'version' => 3,
                'weight' => 200,
                'status' => 'hide',
                'type' => 'article',
                'title' => 'Hidden Articles Title',
                'body' => 'Hidden Articles Body',
                'created_at' => '2010-10-10 12:00:00',
                'updated_at' => '2010-10-10 12:00:00',
            ],
        ];
    }
}