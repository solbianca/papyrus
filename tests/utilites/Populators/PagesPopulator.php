<?php


namespace Tests\utilites\Populators;


class PagesPopulator extends Populator
{
    public function populate()
    {
        $this->populatePages();
    }

    public function cleanup()
    {
        $this->tester->cleanupTable('pages');
    }

    private function populatePages()
    {
        $this->tester->populateTable('pages', $this->makePagesData());
    }

    public function makePagesData()
    {
        return [
            [
                'id' => 1,
                'title' => 'Page Title',
                'path' => '/test',
                'status' => 'active',
                'created_at' => '2010-10-10 12:00:00',
                'updated_at' => '2010-10-10 12:00:00',
            ],
            [
                'id' => 2,
                'title' => 'Foo Title',
                'path' => '/foo',
                'status' => 'active',
                'created_at' => '2010-10-10 12:00:00',
                'updated_at' => '2010-10-10 12:00:00',
            ],
            [
                'id' => 3,
                'title' => 'Foo Bar Title',
                'path' => '/foo/bar',
                'status' => 'active',
                'created_at' => '2010-10-10 12:00:00',
                'updated_at' => '2010-10-10 12:00:00',
            ],
        ];
    }
}