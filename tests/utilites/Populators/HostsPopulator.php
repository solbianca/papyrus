<?php


namespace Tests\utilites\Populators;


class HostsPopulator extends Populator
{
    public function populate()
    {
        $this->populateHosts();
    }

    public function cleanup()
    {
        $this->tester->cleanupTable('hosts');
    }

    public function populateHosts()
    {
        $this->tester->populateTable('hosts', $this->makeHostsData());
    }

    public function makeHostsData()
    {
        return [
            [
                'id' => 1,
                'hostname' => 'example.com',
                'status' => 'active',
                'created_at' => '2010-10-10 12:00:00',
                'updated_at' => '2010-10-10 12:00:00',
            ],
            [
                'id' => 2,
                'hostname' => 'foo.com',
                'status' => 'active',
                'created_at' => '2010-10-10 12:00:00',
                'updated_at' => '2010-10-10 12:00:00',
            ],
            [
                'id' => 3,
                'hostname' => 'bar.com',
                'status' => 'active',
                'created_at' => '2010-10-10 12:00:00',
                'updated_at' => '2010-10-10 12:00:00',
            ],
        ];
    }
}