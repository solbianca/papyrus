<?php


namespace Tests\utilites\Populators;


abstract class Populator
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function __construct($tester)
    {
        $this->tester = $tester;
    }

    abstract public function populate();

    abstract public function cleanup();
}